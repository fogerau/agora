﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[ALIM_CA] 
-- Add the parameters for the stored procedure here
AS
     BEGIN
         -- SET NOCOUNT ON added to prevent extra result sets from
         -- interfering with SELECT statements.

         SET NOCOUNT ON;

         -- Insert statements for procedure here

         TRUNCATE TABLE [STAGING_AGORA].[dbo].[CA];

         INSERT INTO [STAGING_AGORA].[dbo].[CA]
         ([Type],
          TypeValue,
          [GUID],
          Media,
          Titre,
          Pole,
          Univers,
          [Desc],
          TotalNet,
          Rang,
          TotalRang,
          CodeSociete
         ) 

                --Titre/Support
                SELECT 'Titre/Support' Type,
                       1 TypeValue,
                       ca.GUID 'GUID',
                       NULL Media,
                       t.bd_titresupportid Titre,
                       NULL Pole,
                       NULL Univers,
                       t.bd_titresupportidname 'Desc',
                       SUM(ca.TotalNet) Montant,
                       ROW_NUMBER() OVER(PARTITION BY t.bd_titresupportidname ORDER BY SUM(ca.TotalNet) DESC) AS rang,
                (
                    SELECT COUNT(DISTINCT ca2.guid)
                    FROM [STAGING_AGORA].[dbo].[PRE_CA] ca2
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].account fa ON fa.accountid = ca2.GUID
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titreca t2 ON ca2.CodeAnalytique = t2.bd_codeanalytique
                    WHERE t2.bd_titresupportid = t.bd_titresupportid
                          AND ca2.GUID IS NOT NULL
                          AND ca2.GUID <> 'NULL'
                ) total,
                       CodeSociete CodeSociete
                FROM [STAGING_AGORA].[dbo].[PRE_CA] ca
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].account fa ON fa.accountid = ca.GUID
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titreca t ON ca.CodeAnalytique = t.bd_codeanalytique
                WHERE ca.GUID IS NOT NULL
                      AND ca.GUID <> 'NULL'
                      AND t.bd_titresupportidname IS NOT NULL
                GROUP BY ca.GUID,
                         t.bd_titresupportid,
                         t.bd_titresupportidname,
                         CodeSociete
                --order by t.titresupport,sum(ca.TotalNet) desc

                UNION

                --Total
                SELECT 'Total' Type,
                       5 TypeValue,
                       ca.GUID,
                       NULL Media,
                       NULL Titre,
                       NULL Pole,
                       NULL Univers,
                       'Total' 'Desc',
                       SUM(ca.TotalNet) Montant,
                       ROW_NUMBER() OVER(ORDER BY SUM(ca.TotalNet) DESC) AS rang,
                (
                    SELECT COUNT(DISTINCT ca2.guid)
                    FROM [STAGING_AGORA].[dbo].[PRE_CA] ca2
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].account fa ON fa.accountid = ca2.GUID
                    WHERE ca2.GUID IS NOT NULL
                          AND ca2.GUID <> 'NULL'
                ) total,
                       CodeSociete CodeSociete
                FROM [STAGING_AGORA].[dbo].[PRE_CA] ca
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].account fa ON fa.accountid = ca.GUID
                WHERE ca.GUID IS NOT NULL
                      AND ca.GUID <> 'NULL'
                GROUP BY ca.GUID,
                         ca.CodeSociete
                --order by sum(ca.TotalNet) desc

                UNION

                --Média
                SELECT 'Média' Type,
                       2 TypeValue,
                       ca.GUID,
                       ts.bd_media Media,
                       NULL Titre,
                       NULL Pole,
                       NULL Univers,
                       sm.value 'Desc',
                       SUM(ca.TotalNet) Montant,
                       ROW_NUMBER() OVER(PARTITION BY sm.value ORDER BY SUM(ca.TotalNet) DESC) AS rang,
                (
                    SELECT COUNT(DISTINCT ca2.guid)
                    FROM [STAGING_AGORA].[dbo].[PRE_CA] ca2
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].account fa ON fa.accountid = ca2.GUID
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titreca t2 ON ca2.CodeAnalytique = t2.bd_codeanalytique
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titresupport ts2 ON ts2.bd_titresupportid = t2.bd_titresupportid
                    WHERE ts2.bd_media = ts.bd_media
                          AND ca2.GUID IS NOT NULL
                          AND ca2.GUID <> 'NULL'
                ) total,
                       CodeSociete CodeSociete
                FROM [STAGING_AGORA].[dbo].[PRE_CA] ca
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].account fa ON fa.accountid = ca.GUID
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titreca t ON ca.CodeAnalytique = t.bd_codeanalytique
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titresupport ts ON ts.bd_titresupportid = t.bd_titresupportid
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].StringMapBase sm ON(sm.AttributeValue = ts.bd_media
                                                                               AND sm.attributename = 'bd_media'
                                                                               AND sm.objecttypecode = '10026')
                WHERE ca.GUID IS NOT NULL
                      AND ca.GUID <> 'NULL'
                      AND sm.value IS NOT NULL
                GROUP BY sm.value,
                         ts.bd_media,
                         ca.GUID,
                         CodeSociete
                --order by t.Media, sum(ca.TotalNet) desc

                UNION

                --Pole
                SELECT 'Pôle' Type,
                       3 TypeValue,
                       ca.GUID,
                       NULL Media,
                       NULL Titre,
                       pole.businessunitid Pole,
                       NULL Univers,
                       pole.name 'Desc',
                       SUM(ca.TotalNet) Montant,
                       ROW_NUMBER() OVER(PARTITION BY pole.name ORDER BY SUM(ca.TotalNet) DESC) AS rang,
                (
                    SELECT COUNT(DISTINCT ca2.guid)
                    FROM [STAGING_AGORA].[dbo].[PRE_CA] ca2
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].account fa ON fa.accountid = ca2.GUID
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titreca t2 ON ca2.CodeAnalytique = t2.bd_codeanalytique
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titresupport ts2 ON ts2.bd_titresupportid = t2.bd_titresupportid
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].BusinessUnit pole2 ON pole2.businessunitid = ts2.bd_poleid
                    WHERE pole.businessunitid = pole2.businessunitid
                          AND ca2.GUID IS NOT NULL
                          AND ca2.GUID <> 'NULL'
                ) total,
                       CodeSociete CodeSociete
                FROM [STAGING_AGORA].[dbo].[PRE_CA] ca
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].account fa ON fa.accountid = ca.GUID
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titreca t ON ca.CodeAnalytique = t.bd_codeanalytique
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titresupport ts ON ts.bd_titresupportid = t.bd_titresupportid
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].BusinessUnit pole ON pole.businessunitid = ts.bd_poleid
                WHERE ca.GUID IS NOT NULL
                      AND ca.GUID <> 'NULL'
                      AND pole.name IS NOT NULL
                GROUP BY pole.name,
                         pole.businessunitid,
                         ca.GUID,
                         CodeSociete
                --order by t.titresupport,sum(ca.TotalNet) desc

                UNION

                --Univers
                SELECT 'Univers' Type,
                       4 TypeValue,
                       ca.GUID,
                       NULL Media,
                       NULL Titre,
                       NULL Pole,
                       univers.businessunitid Univers,
                       Univers.name 'Desc',
                       SUM(ca.TotalNet) Montant,
                       ROW_NUMBER() OVER(PARTITION BY Univers.name ORDER BY SUM(ca.TotalNet) DESC) AS rang,
                (
                    SELECT COUNT(DISTINCT ca2.guid)
                    FROM [STAGING_AGORA].[dbo].[PRE_CA] ca2
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].account fa ON fa.accountid = ca2.GUID
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titreca t2 ON ca2.CodeAnalytique = t2.bd_codeanalytique
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titresupport ts2 ON ts2.bd_titresupportid = t2.bd_titresupportid
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].BusinessUnit pole2 ON pole2.businessunitid = ts2.bd_poleid
                         INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].BusinessUnit Univers2 ON Univers2.businessunitid = pole2.parentbusinessunitid
                    WHERE Univers2.businessunitid = Univers.businessunitid
                          AND ca2.GUID IS NOT NULL
                          AND ca2.GUID <> 'NULL'
                ) total,
                       CodeSociete CodeSociete
                FROM [STAGING_AGORA].[dbo].[PRE_CA] ca
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].account fa ON fa.accountid = ca.GUID
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titreca t ON ca.CodeAnalytique = t.bd_codeanalytique
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].bd_titresupport ts ON ts.bd_titresupportid = t.bd_titresupportid
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].BusinessUnit pole ON pole.businessunitid = ts.bd_poleid
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].BusinessUnit Univers ON Univers.businessunitid = pole.parentbusinessunitid
                WHERE ca.GUID IS NOT NULL
                      AND ca.GUID <> 'NULL'
                      AND univers.name IS NOT NULL
                GROUP BY Univers.name,
                         Univers.businessunitid,
                         ca.GUID,
                         CodeSociete;
         --order by t.titresupport,sum(ca.TotalNet) desc

     END;