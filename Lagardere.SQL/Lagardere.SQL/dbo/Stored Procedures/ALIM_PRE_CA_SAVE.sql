﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ALIM_PRE_CA_SAVE]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
truncate table [STAGING_AGORA].[dbo].PRE_CA
insert into [STAGING_AGORA].[dbo].PRE_CA
--Annonceur
SELECT [CodeSociete]
      ,convert(nvarchar(255),[CodeAgoraAnnonceur]) GUID
      ,convert(nvarchar(255),[CodeCipresAnnonceur]) CIPRES
      ,[CodeAnalytique]
      ,[TotalNet]
  FROM [STAGING_AGORA].[dbo].[FUN] f
  where convert(nvarchar(255),f.[CodeCipresAnnonceur]) is not null and convert(nvarchar(255),[CodeCipresAnnonceur])<>'NULL'
  union
--Groupe annonceur
  SELECT [CodeSociete]
      ,convert(nvarchar(255),[CodeAgoraGroupeAnnonceur]) GUID
      ,convert(nvarchar(255),[CodeCipresGroupeAnnonceur]) CIPRES
      ,[CodeAnalytique]
      ,[TotalNet]
  FROM [STAGING_AGORA].[dbo].[FUN] f
  where convert(nvarchar(255),f.[CodeCipresGroupeAnnonceur]) is not null and convert(nvarchar(255),f.[CodeCipresGroupeAnnonceur])<>'NULL'
   union
--Agence
  SELECT [CodeSociete]
      ,convert(nvarchar(255),[CodeAgoraAgence]) GUID
      ,convert(nvarchar(255),[CodeCipresAgence]) CIPRES
      ,[CodeAnalytique]
      ,[TotalNet]
  FROM [STAGING_AGORA].[dbo].[FUN] f
  where convert(nvarchar(255),f.[CodeCipresAgence]) is not null and convert(nvarchar(255),f.[CodeCipresAgence])<>'NULL'
   union
--groupe agence
  SELECT [CodeSociete]
      ,convert(nvarchar(255),[CodeAgoraGroupeAgence]) GUID
      ,convert(nvarchar(255),[CodeCipresGroupeAgence]) CIPRES
      ,[CodeAnalytique]
      ,[TotalNet]
  FROM [STAGING_AGORA].[dbo].[FUN] f
  where convert(nvarchar(255),f.[CodeCipresGroupeAgence]) is not null and convert(nvarchar(255),f.[CodeCipresGroupeAgence])<>'NULL'


insert into [STAGING_AGORA].[dbo].PRE_CA
select PRE_CA.CodeSociete,  A.ParentAccountId GUID,'' CIPRES, PRE_CA.CodeAnalytique, sum(PRE_CA.TotalNet) TotalNet from PRE_CA
inner join [LAGARDEREDEV_MSCRM].[dbo].Account A on a.AccountId = PRE_CA.GUID 
inner join [LAGARDEREDEV_MSCRM].[dbo].Account A2 on a.ParentAccountId = a2.AccountId
where a.ParentAccountId is not null
and a.bd_tetedegroupe = 0
and a2.bd_siret is null --and a.ParentAccountId not in (select GUID from PRE_CA)
--and a.ParentAccountIdName = 'SNCF'
group by  PRE_CA.CodeSociete, a.ParentAccountIdName, A.ParentAccountId, PRE_CA.CodeAnalytique
END