﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[ALIM_PRE_CA]
-- Add the parameters for the stored procedure here
	
AS
     BEGIN
         -- SET NOCOUNT ON added to prevent extra result sets from
         -- interfering with SELECT statements.

         SET NOCOUNT ON;

         -- Insert statements for procedure here

         TRUNCATE TABLE [STAGING_AGORA].[dbo].PRE_CA;

         INSERT INTO [STAGING_AGORA].[dbo].PRE_CA
         (CodeSociete,
          GUID,
          CIPRES,
          CodeAnalytique,
          TotalNet,
          GUIDChild
         )
                --Annonceur
                SELECT [CodeSociete],
                       CONVERT( NVARCHAR(255), [CodeAgoraAnnonceur]) AS GUID,
                       CONVERT( NVARCHAR(255), [CodeCipresAnnonceur]) AS CIPRES,
                       [CodeAnalytique],
                       [TotalNet],
                       NULL
                FROM [STAGING_AGORA].[dbo].[FUN] AS f
                WHERE CONVERT(NVARCHAR(255), f.[CodeCipresAnnonceur]) IS NOT NULL
                      AND CONVERT(NVARCHAR(255), [CodeCipresAnnonceur]) <> 'NULL'
                UNION
                --Groupe annonceur
                SELECT [CodeSociete],
                       CONVERT( NVARCHAR(255), [CodeAgoraGroupeAnnonceur]) AS GUID,
                       CONVERT( NVARCHAR(255), [CodeCipresGroupeAnnonceur]) AS CIPRES,
                       [CodeAnalytique],
                       [TotalNet],
                       NULL
                FROM [STAGING_AGORA].[dbo].[FUN] AS f
                WHERE CONVERT(NVARCHAR(255), f.[CodeCipresGroupeAnnonceur]) IS NOT NULL
                      AND CONVERT(NVARCHAR(255), f.[CodeCipresGroupeAnnonceur]) <> 'NULL'
                UNION
                --Agence
                SELECT [CodeSociete],
                       CONVERT( NVARCHAR(255), [CodeAgoraAgence]) AS GUID,
                       CONVERT( NVARCHAR(255), [CodeCipresAgence]) AS CIPRES,
                       [CodeAnalytique],
                       [TotalNet],
                       NULL
                FROM [STAGING_AGORA].[dbo].[FUN] AS f
                WHERE CONVERT(NVARCHAR(255), f.[CodeCipresAgence]) IS NOT NULL
                      AND CONVERT(NVARCHAR(255), f.[CodeCipresAgence]) <> 'NULL'
                UNION
                --groupe agence
                SELECT [CodeSociete],
                       CONVERT( NVARCHAR(255), [CodeAgoraGroupeAgence]) AS GUID,
                       CONVERT( NVARCHAR(255), [CodeCipresGroupeAgence]) AS CIPRES,
                       [CodeAnalytique],
                       [TotalNet],
                       NULL
                FROM [STAGING_AGORA].[dbo].[FUN] AS f
                WHERE CONVERT(NVARCHAR(255), f.[CodeCipresGroupeAgence]) IS NOT NULL
                      AND CONVERT(NVARCHAR(255), f.[CodeCipresGroupeAgence]) <> 'NULL';

         INSERT INTO [STAGING_AGORA].[dbo].PRE_CA
         (CodeSociete,
          GUID,
          CIPRES,
          CodeAnalytique,
          TotalNet,
          GUIDChild
         )
                SELECT PRE_CA.CodeSociete,
                       A.ParentAccountId AS GUID,
                       '' AS CIPRES,
                       PRE_CA.CodeAnalytique,
                       SUM(PRE_CA.TotalNet) AS TotalNet,
                       CONVERT( NVARCHAR(255), A.AccountId) AS GUIDChild
                FROM PRE_CA
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].Account AS A ON a.AccountId = PRE_CA.GUID
                     INNER JOIN [LAGARDEREDEV_MSCRM].[dbo].Account AS A2 ON a.ParentAccountId = a2.AccountId
                WHERE a.ParentAccountId IS NOT NULL
                      AND a.bd_tetedegroupe = 0
                      AND a2.bd_siret IS NULL --and a.ParentAccountId not in (select GUID from PRE_CA)
                --and a.ParentAccountIdName = 'SNCF'
                GROUP BY PRE_CA.CodeSociete,
                         a.ParentAccountIdName,
                         A.ParentAccountId,
                         PRE_CA.CodeAnalytique,
                         A.accountId;

         DELETE FROM PRE_CA
         WHERE GUID IN
         (
             SELECT GUIDChild
             FROM PRE_CA
             WHERE GUIDChild IS NOT NULL
         );
     END;