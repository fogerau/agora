﻿CREATE TABLE [dbo].[PRE_CA_SAVE] (
    [CodeSociete]    NVARCHAR (255) NULL,
    [GUID]           NVARCHAR (255) NULL,
    [CIPRES]         NVARCHAR (255) NULL,
    [CodeAnalytique] NVARCHAR (255) NULL,
    [TotalNet]       FLOAT (53)     NULL
);

