﻿CREATE TABLE [dbo].[FUN_Log] (
    [CodeSociete]               NVARCHAR (255) NULL,
    [Societe]                   NVARCHAR (255) NULL,
    [CodeCipresAnnonceur]       NVARCHAR (255) NULL,
    [CodeAgoraAnnonceur]        NVARCHAR (255) NULL,
    [Annonceur]                 NVARCHAR (255) NULL,
    [CodeCipresGroupeAnnonceur] NVARCHAR (255) NULL,
    [CodeAgoraGroupeAnnonceur]  NVARCHAR (255) NULL,
    [GroupeAnnonceur]           NVARCHAR (255) NULL,
    [CodeCipresAgence]          NVARCHAR (255) NULL,
    [CodeAgoraAgence]           NVARCHAR (255) NULL,
    [Agence]                    NVARCHAR (255) NULL,
    [CodeCipresGroupeAgence]    NVARCHAR (255) NULL,
    [CodeAgoraGroupeAgence]     NVARCHAR (255) NULL,
    [GroupeAgence]              NVARCHAR (255) NULL,
    [CodeAnalytique]            NVARCHAR (255) NULL,
    [Analytique]                NVARCHAR (255) NULL,
    [TotalNet]                  NVARCHAR (255) NULL
);

