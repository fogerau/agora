﻿CREATE TABLE [dbo].[CA] (
    [Type]        VARCHAR (30)     NOT NULL,
    [TypeValue]   INT              NOT NULL,
    [GUID]        UNIQUEIDENTIFIER NOT NULL,
    [Media]       INT              NULL,
    [Titre]       UNIQUEIDENTIFIER NULL,
    [Pole]        UNIQUEIDENTIFIER NULL,
    [Univers]     UNIQUEIDENTIFIER NULL,
    [Desc]        NVARCHAR (255)   NOT NULL,
    [TotalNet]    FLOAT (53)       NOT NULL,
    [Rang]        INT              NOT NULL,
    [TotalRang]   INT              NOT NULL,
    [Index]       INT              IDENTITY (1, 1) NOT NULL,
    [CodeSociete] NVARCHAR (255)   NULL
);

