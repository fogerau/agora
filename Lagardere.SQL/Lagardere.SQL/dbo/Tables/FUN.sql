﻿CREATE TABLE [dbo].[FUN] (
    [CodeSociete]               FLOAT (53)     NULL,
    [Societe]                   NVARCHAR (255) NULL,
    [CodeCipresAnnonceur]       FLOAT (53)     NULL,
    [CodeAgoraAnnonceur]        NVARCHAR (255) NULL,
    [Annonceur]                 NVARCHAR (255) NULL,
    [CodeCipresGroupeAnnonceur] NVARCHAR (255) NULL,
    [CodeAgoraGroupeAnnonceur]  NVARCHAR (255) NULL,
    [GroupeAnnonceur]           NVARCHAR (255) NULL,
    [CodeCipresAgence]          FLOAT (53)     NULL,
    [CodeAgoraAgence]           NVARCHAR (255) NULL,
    [Agence]                    NVARCHAR (255) NULL,
    [CodeCipresGroupeAgence]    FLOAT (53)     NULL,
    [CodeAgoraGroupeAgence]     NVARCHAR (255) NULL,
    [GroupeAgence]              NVARCHAR (255) NULL,
    [CodeAnalytique]            NVARCHAR (255) NULL,
    [Analytique]                NVARCHAR (255) NULL,
    [TotalNet]                  FLOAT (53)     NULL
);

