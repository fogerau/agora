﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsService.WindowsService;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Xml.Serialization;
using System.IO;

namespace TestRabbitMQ
{
    public class RetourTiersAgoraCipres
    {
        public string Statut { get; set; }
        public string GUID { get; set; }
        public string CodeCipres { get; set; }
        public string Message { get; set; }

        public RetourTiersAgoraCipres() { }
    }

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AgoraRabbitMQService myService = new AgoraRabbitMQService();
            myService.OnStart();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            IConnection connection;
            QueueingBasicConsumer consumer;
            IModel channel;
            string replyQueueName;

            var factory = new ConnectionFactory() { HostName = "localhost" };
            factory.UserName = "rmq-appdev";
            factory.Password = "Soleil1115";
            factory.VirtualHost = "/";
            factory.Protocol = Protocols.DefaultProtocol;
            factory.HostName = "rmq-svmappd1.siege.la.priv";
            factory.Port = AmqpTcpEndpoint.UseDefaultPort;


            // Création de la connexion
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            // Déclare un canal de communication qui pourra avoir plusieurs clients en terminaison
            channel.ExchangeDeclare(exchange: "TiersAgoraCipres",  type: "fanout", durable: true);
            replyQueueName = channel.QueueDeclare(queue: "TiersAgoraCipres-CIPRES", durable: true, exclusive: false, autoDelete: false, arguments: null).QueueName;
            consumer = new QueueingBasicConsumer(channel);
            channel.BasicConsume(queue: replyQueueName,
                                 noAck: true,
                                 consumer: consumer);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                String tData = "<?xml version='1.0' encoding='utf-16'?><RetourTiersAgoraCipres xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><Message>account With Id = 0da3a8e2-542c-df11-91c4-18a905459728 Does Not Exist</Message><Statut>KO</Statut><GUID>0DA3A8E2-542C-DF11-91C4-18A905459728</GUID><CodeCipres>263636</CodeCipres></RetourTiersAgoraCipres>";
                //String tData = "<RetourTiersAgoraCipres><Message>account With Id = 0da3a8e2-542c-df11-91c4-18a905459728 Does Not Exist</Message><Statut>KO</Statut><GUID>0DA3A8E2-542C-DF11-91C4-18A905459728</GUID><CodeCipres>263636</CodeCipres></RetourTiersAgoraCipres>";

                XmlSerializer serializer = new XmlSerializer(typeof(RetourTiersAgoraCipres));
                MemoryStream memStream = new MemoryStream(Encoding.Unicode.GetBytes(tData));

                RetourTiersAgoraCipres rb = serializer.Deserialize(memStream) as RetourTiersAgoraCipres;
                MessageBox.Show(rb.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
