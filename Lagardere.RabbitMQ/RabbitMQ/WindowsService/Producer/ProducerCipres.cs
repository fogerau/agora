﻿using Definitions;
using Microsoft.Xrm.Client.Services;
using Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace WindowsService
{
    public class ProducerCipres : Producer
    {
        // Singleton
        private static ProducerCipres instance;
        public static ProducerCipres Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProducerCipres();
                }
                return instance;
            }
        }

        /// <summary>
        /// Méthode de traitement du message envoyé par le producteur
        /// </summary>
        /// <returns>Retourne le callback à traiter pour le producteur</returns>
        protected override void ProcessCallback(string message)
        {
            try
            {
                EventLog.WriteEntry("AgoraRabbitMQService", "CIPRES " + message, EventLogEntryType.Information);

                RetourTiersAgoraCipres ret = RetourTiersAgoraCipres.Deserialize(message);
                Guid accountId = string.IsNullOrEmpty(ret.GUID) ? Guid.Empty : new Guid(ret.GUID);
                String codeCipres = ret.CodeCipres;

                if (accountId != Guid.Empty && !string.IsNullOrEmpty(ret.Statut))
                {//création de la connection au CRM(connexion string dans App.config)
                    using (var service = new OrganizationService("Xrm"))
                    {
                        LocalContext context = new LocalContext(service);
                        try
                        {
                            SynchroAccountService synchroService = new SynchroAccountService(context, accountId);
                            SYNCHRO_ACCOUNT_STATUT stat = ret.Statut.ToEnum<SYNCHRO_ACCOUNT_STATUT>();
                            synchroService.LogSynchro(accountId, CIBLE.CIPRES, stat, string.Empty, ret.Message);

                            // Si le retour est OK et que le code Ciprès n'est pas null on doit lancer une synchro vers GEMME et MediaPilot
                            if (!String.IsNullOrEmpty(codeCipres))
                            {
                                //On met à jour le code cipres - La MAJ du code ciprès ne doit pas déclencher le plugin
                                if (!synchroService.IsCodeCipresExist(accountId))
                                    synchroService.UpdateAccount(accountId, codeCipres);
                                
                                Tiers tiers = new Tiers(accountId.ToString(), codeCipres);
                                string messageGEMMEMP = Tiers.Serialize(tiers);
                                ProducerGemmeMPProducer.Instance.SendMessage(messageGEMMEMP);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("AgoraRabbitMQService", "ProcessCallback synchroService CIPRES " + ex.Message, EventLogEntryType.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AgoraRabbitMQService", String.Format("ProcessCallback CIPRES {0} {1} {2}", message, Environment.NewLine, ex.Message), EventLogEntryType.Error);
            }
        }

    }
}
