﻿using Definitions;
using Microsoft.Xrm.Client.Services;
using Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsService
{
    public class ProducerGemmeMPProducer : Producer
    {
        // Singleton
        private static ProducerGemmeMPProducer instance;
        public static ProducerGemmeMPProducer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProducerGemmeMPProducer();
                }
                return instance;
            }
        }

        /// <summary>
        /// Méthode de traitement du message envoyé par le producteur
        /// </summary>
        /// <returns>Retourne le callback à traiter pour le producteur</returns>
        protected override void ProcessCallback(string message)
        {
            try
            {
                EventLog.WriteEntry("AgoraRabbitMQService", "GEMME-MEDIAPILOT" + message, EventLogEntryType.Information);
                RetourTiersAgora ret = RetourTiersAgora.Deserialize(message);

                Guid accountId = string.IsNullOrEmpty(ret.GUID) ? Guid.Empty : new Guid(ret.GUID);

                if (accountId != Guid.Empty && !string.IsNullOrEmpty(ret.Statut))
                {//création de la connection au CRM(connexion string dans App.config)
                    using (var service = new OrganizationService("Xrm"))
                    {
                        LocalContext context = new LocalContext(service);
                        SynchroAccountService synchroService = new SynchroAccountService(context, accountId);
                        synchroService.LogSynchro(accountId, ret.Application, ret.Statut, string.Empty, ret.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AgoraRabbitMQService", String.Format("ProcessCallback GEMME {0} {1} {2}", message, Environment.NewLine, ex.Message), EventLogEntryType.Error);
            }
        }
    }
}
