﻿using Microsoft.Xrm.Client.Services;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace WindowsService
{
    partial class ConsumerCipresService : ServiceBase
    {
        public ConsumerCipresService()
        {
            InitializeComponent();

           
            

        }

        protected override void OnStart(string[] args)
        {
            int timer = 100;
            int.TryParse(ConfigurationManager.TimerInterval, out timer);

            timerConsummer.Interval = timer;
            eventLog1.WriteEntry("Démarrage du service ConsumerCipresService");
            timerConsummer.Enabled = true;
            timerConsummer.Start();
            
        }

        protected override void OnStop()
        {
            // TODO: ajoutez ici le code pour effectuer les destructions nécessaires à l'arrêt de votre service.
            eventLog1.WriteEntry("Arrêt du service ConsumerCipresService");
            
        }

        

        private void timerConsumer_Tick(object sender, EventArgs e)
        {
           //création de la connection au CRM(connexion string dans App.config)
            using (var service = new OrganizationService("Xrm"))
            {

                LocalContext context = new LocalContext(service);
                ConsumerCipresManager ConsumerCipres = new ConsumerCipresManager(context);
                ConsumerCipres.retrieveResponse();              
                
            }
        }
    }
}
