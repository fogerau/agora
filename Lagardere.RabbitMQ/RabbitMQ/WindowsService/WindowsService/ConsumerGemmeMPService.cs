﻿using Microsoft.Xrm.Client.Services;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace WindowsService
{
    partial class ConsumerGemmeMPService : ServiceBase
    {
        public ConsumerGemmeMPService()
        {
            InitializeComponent();
            
        }

        protected override void OnStart(string[] args)
        {
            // TODO: ajoutez ici le code pour démarrer votre service.
            int timer = 100;
            int.TryParse(ConfigurationManager.TimerInterval, out timer);

            timerConsumer.Interval = timer;
            eventLog1.WriteEntry("Démarrage du service ConsumerGemmeMPService");
            timerConsumer.Enabled = true;
            timerConsumer.Start();
        }

        protected override void OnStop()
        {
            // TODO: ajoutez ici le code pour effectuer les destructions nécessaires à l'arrêt de votre service.
            eventLog1.WriteEntry("Arrêt du service ConsumerGemmeMPService");
        }


        private void timerConsumer_Tick(object sender, EventArgs e)
        {

            using (var service = new OrganizationService("Xrm"))
            {

                LocalContext context = new LocalContext(service);
                ConsumerGemmeMpManager consumer = new ConsumerGemmeMpManager(context);
                consumer.retrieveResponse();

            }
        }

       
    }
}
