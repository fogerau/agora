﻿using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utils;

namespace WindowsService.WindowsService
{
    public partial class AgoraRabbitMQService : ServiceBase
    {
        private ServiceHost serviceHost = null;

        public AgoraRabbitMQService()
        {
            InitializeComponent();

            this.ServiceName = "AgoraRabbitMQService";
            this.EventLog.Log = "Application";

            ProducerCipres.Instance.HostName = ConfigurationManager.RabbitMQHost;
            ProducerCipres.Instance.User = ConfigurationManager.RabbitMQUser;
            ProducerCipres.Instance.Pwd = ConfigurationManager.RabbitMQPwd;
            ProducerCipres.Instance.QueueName = ConfigurationManager.CipresQueueName;
            ProducerCipres.Instance.IsListeningCallbacks = true;

            ProducerGemmeMPProducer.Instance.HostName = ConfigurationManager.RabbitMQHost;
            ProducerGemmeMPProducer.Instance.User = ConfigurationManager.RabbitMQUser;
            ProducerGemmeMPProducer.Instance.Pwd = ConfigurationManager.RabbitMQPwd;
            ProducerGemmeMPProducer.Instance.QueueName = ConfigurationManager.GemMPQueueName;
            ProducerGemmeMPProducer.Instance.IsListeningCallbacks = true;
        }

        // Start the Windows service.
        protected override void OnStart(string[] args)
        {
            try
            {
                // TODO: ajoutez ici le code pour démarrer votre service.
                // Démarrage du producteur
                //EventLog.WriteEntry("AgoraRabbitMQService", "Avant Launch Cipres");
                ProducerCipres.Instance.Launch();
                //EventLog.WriteEntry("AgoraRabbitMQService", "Après Launch Cipres - Avant Launch GEMME");
                ProducerGemmeMPProducer.Instance.Launch();
                //EventLog.WriteEntry("AgoraRabbitMQService", "Après Launch GEMME");

                if (serviceHost != null)
                    serviceHost.Close();

                // Démarrage du webservice
                // Create a ServiceHost for the CalculatorService type and 
                // provide the base address.
                serviceHost = new ServiceHost(typeof(AgoraRabbiMQWebservice));
  
                // Open the ServiceHostBase to create listeners and start 
                // listening for messages.
                serviceHost.Open();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Error open WS :" + ex.Message, EventLogEntryType.Error);
            }
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }
        }

        public void OnStart()
        {
            this.OnStart(null);
        }
    }
}
