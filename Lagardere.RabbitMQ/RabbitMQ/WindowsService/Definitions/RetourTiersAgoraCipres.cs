﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    [Serializable]
    public class RetourTiersAgoraCipres
    {
        public string Statut { get; set; }
        public string GUID { get; set; }
        public string CodeCipres { get; set; }
        public string Message { get; set; }

    }

    [Serializable]
    public class RetourTiersAgora
    {
        public string Application { get; set; }
        public string Statut { get; set; }
        public string GUID { get; set; }
        public string CodeCipres { get; set; }
        public string Message { get; set; }

    }

    [Serializable]
    public class Tiers
    {
         public string GUID { get; set; }
        public string CodeCipres { get; set; }

    }
}
