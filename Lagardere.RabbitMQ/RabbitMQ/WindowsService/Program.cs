﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using WindowsService.WindowsService;

namespace WindowsService
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        static void Main()
        {
            #if DEBUG
            System.Diagnostics.Debugger.Launch();
            AgoraRabbitMQService myService = new AgoraRabbitMQService();
            myService.OnStart();

            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[] 
            //{ 
            //    new AgoraRabbitMQService(),
            //};
            //ServiceBase.Run(ServicesToRun);

            #else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new AgoraRabbitMQService(),
            };
            ServiceBase.Run(ServicesToRun);
            #endif


        }
    }
}
