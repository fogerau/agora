﻿using Definitions;
using Microsoft.Xrm.Client.Services;
using Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace WindowsService
{
    /// <summary>
    /// Webservice embarqué dans le service Windows destiné à appeler le producteur
    /// </summary>
    public class AgoraRabbiMQWebservice : IAgoraRabbiMQWebservice
    {
        /// <summary>
        /// Méthode d'envoi du message au producteur
        /// </summary>
        public bool Send(string idCRM, string codeCipres)
        {
            try
            {
                codeCipres = ((codeCipres == "null") ? string.Empty : codeCipres);

                Tiers tiers = new Tiers(idCRM, codeCipres);
                string message = Tiers.Serialize(tiers);

                EventLog.WriteEntry("AgoraRabbitMQService", "Source Message : "+ message, EventLogEntryType.Information);

                bool Ok = false;
                int occurence = 1;
                string ErrorMsg = string.Empty;
                while (occurence <= 3 && !Ok)
                {
                    try
                    {
                        EventLog.WriteEntry("AgoraRabbitMQService", "Send message", EventLogEntryType.Information);
                        ProducerCipres.Instance.SendMessage(message);
                        //ProducerGemmeMPProducer.Instance.SendMessage(message);
                        Ok = true;
                    }
                    catch (Exception ex)
                    {
                        EventLog.WriteEntry("AgoraRabbitMQService", "Error Sending message", EventLogEntryType.Information);
                        ErrorMsg = ex.Message;
                        occurence++;
                    }
                }
                SYNCHRO_ACCOUNT_STATUT statut = SYNCHRO_ACCOUNT_STATUT.EnCours;
                if (!Ok)
                {
                    EventLog.WriteEntry("AgoraRabbitMQService", string.Format("Erreur lors de la publication du message : {0}  dans la file d'attente. Erreur = {1}", message, ErrorMsg), EventLogEntryType.Information);
                    //Récupération du destinataire et de l'expéditeur
                    string serverSMTP = ConfigurationManager.ServerSMTP;
                    string DEST = ConfigurationManager.RABBITADMINEMAIL;
                    string from = ConfigurationManager.CRMADMINEMAIL;
                    string titre = "Erreur lors la publication";
                    string msgMail = string.Format("Erreur lors de la publication du message : {0}  dans la file d'attente. Erreur = {1}", message, ErrorMsg);
                    statut = SYNCHRO_ACCOUNT_STATUT.Echoue;
                    EmailUtils.SendMail(titre, msgMail, DEST, serverSMTP, from);
                }
                LogSynchro(idCRM, string.Empty, codeCipres, statut, ErrorMsg);

                return Ok;
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("AgoraRabbitMQService", String.Format("SendMail to {0} from {1} ({2})", ConfigurationManager.CRMADMINEMAIL, ConfigurationManager.RABBITADMINEMAIL,e.Message), EventLogEntryType.Error);
                LogSynchro(idCRM, string.Empty, codeCipres, SYNCHRO_ACCOUNT_STATUT.Echoue, e.Message);
                return false;
            }
        }

        public void LogSynchro(string idCrm, string nom, string codeCipres, SYNCHRO_ACCOUNT_STATUT statut, string msg)
        {
            EventLog.WriteEntry("AgoraRabbitMQService", "Start LogSynchro", EventLogEntryType.Information);
                    
            Guid id = new Guid(idCrm);
            EventLog.WriteEntry("AgoraRabbitMQService", "guid done", EventLogEntryType.Information);
            using (var service = new OrganizationService("Xrm"))
            {
                EventLog.WriteEntry("AgoraRabbitMQService", "service created", EventLogEntryType.Information);
                LocalContext context = new LocalContext(service);
                EventLog.WriteEntry("AgoraRabbitMQService", "context created", EventLogEntryType.Information);
                SynchroAccountService synchroService = new SynchroAccountService(context, id);
                EventLog.WriteEntry("AgoraRabbitMQService", "synchroservice created created", EventLogEntryType.Information);

                synchroService.LogSynchro(id, CIBLE.CIPRES, statut, nom, msg);
                if (!string.IsNullOrEmpty(codeCipres) && codeCipres != "-1")
                {
                    synchroService.LogSynchro(id, CIBLE.GEMME, statut, nom, msg);
                    synchroService.LogSynchro(id, CIBLE.MEDIAPILOT, statut, nom, msg);
                    //synchroService.LogSynchro(id, CIBLE.ADRESA_NEWS, statut, nom, msg);
                    synchroService.LogSynchro(id, CIBLE.ADRESA, statut, nom, msg);
                }
            }
            EventLog.WriteEntry("AgoraRabbitMQService", "End LogSynchro", EventLogEntryType.Information);

        }
    }
}
