﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace WindowsService
{
    [ServiceContract]
    [XmlSerializerFormat]
    public interface IAgoraRabbiMQWebservice
    {
        [OperationContract]
        [WebGet(UriTemplate = "Send/{idCRM}/{codeCipres}")]
        bool Send(string idCRM,string codeCipres);
    }
}
