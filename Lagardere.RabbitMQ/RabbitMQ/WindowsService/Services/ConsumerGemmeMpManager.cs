﻿using Definitions;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ConsumerGemmeMpManager : DefaultService
    {

        public ConsumerGemmeMpManager(IContext context) : base(context) { }


        public void retrieveResponse()
        {

            ParameterService paramService = new ParameterService(Context);
            Dictionary<string, string> paramsList = paramService.RetrieveStringParams(CARACTERISTIQUE.RabbitMQ);

            string hostname = paramsList[ParameterService.RABBITMQHOST];
            string user = paramsList[ParameterService.RABBITMQUSER];
            string pwd = paramsList[ParameterService.RABBITMQPWD];

            string queueNameGEMMMP = paramsList[ParameterService.GEMMEMPQUEUENAME];

            Consumer cons = new Consumer(hostname, user, pwd, Context);
            RetourTiersAgora ret = cons.RetrieveResponse<RetourTiersAgora>(queueNameGEMMMP);


            Guid accountId = string.IsNullOrEmpty(ret.GUID) ? Guid.Empty : new Guid(ret.GUID);

            if (accountId != Guid.Empty && !string.IsNullOrEmpty(ret.Application) && !string.IsNullOrEmpty(ret.Statut))
            {
                SynchroAccountService synchroService = new SynchroAccountService(Context, accountId);
                synchroService.LogSynchro(accountId, ret.Application, ret.Statut, string.Empty);
            }
            else
            {

            }

        }


    }
}
