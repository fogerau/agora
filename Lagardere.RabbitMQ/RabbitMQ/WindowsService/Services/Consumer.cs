﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Definitions;
using System.IO;

namespace Services
{
    public class Consumer : DefaultRabbitMQ
    {
        private QueueingBasicConsumer consumer;

        /// <summary>
        /// Initialisation de la connexion avec RabbitMQ
        /// </summary>
        public Consumer(string rabbitMQHost, string rabbitMQUser, string rabbitMQPwd, IContext pcontext)
            : base(rabbitMQHost, rabbitMQUser, rabbitMQPwd, pcontext)
        { }

        /// <summary>
        ///  recupération des messages envoyés par le serveur
        /// </summary>
        /// <param name="queueName"></param>
        public T RetrieveResponse<T>(string queueName)
        {

            // Se rattache au canal de communication établi avec le serveur
            channel.ExchangeDeclare(exchange: queueName, type: "fanout");

            var qName = channel.QueueDeclare().QueueName;
            channel.QueueBind(queue: qName,
                              exchange: queueName,
                              routingKey: "");
            consumer = new QueueingBasicConsumer(channel);
            channel.BasicConsume(queue: qName,
                                    noAck: false,
                                    consumer: consumer);
            //Console.WriteLine("En attente de messages...");
            BasicDeliverEventArgs ea = (BasicDeliverEventArgs)consumer.Queue.Dequeue();
            byte[] body = ea.Body;
            var props = ea.BasicProperties;
            var replyProps = channel.CreateBasicProperties();
            replyProps.CorrelationId = props.CorrelationId;
            var message = Encoding.UTF8.GetString(body);

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringReader rdr = new StringReader(message);
            T resultingMessage = (T)serializer.Deserialize(rdr);

            return resultingMessage;

        }
    }
}
