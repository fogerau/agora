﻿

using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace Services
{
    public class ConsumerCipresManager : DefaultService
    {

        public ConsumerCipresManager(IContext context) : base(context) { }




        internal void retrieveResponse()
        {
            ParameterService paramService = new ParameterService(Context);
            Dictionary<string, string> paramsList = paramService.RetrieveStringParams(CARACTERISTIQUE.RabbitMQ);

            string hostname = paramsList[ParameterService.RABBITMQHOST];
            string user = paramsList[ParameterService.RABBITMQUSER];
            string pwd = paramsList[ParameterService.RABBITMQPWD];

            string queueNameCipres = paramsList[ParameterService.CIPRESQUEUENAME];

            Consumer cons = new Consumer(hostname, user, pwd, Context);
            RetourTiersAgoraCipres ret = cons.RetrieveResponse<RetourTiersAgoraCipres>(queueNameCipres);


            Guid accountId = string.IsNullOrEmpty(ret.GUID) ? Guid.Empty : new Guid(ret.GUID);

            if (accountId != Guid.Empty && !string.IsNullOrEmpty(ret.Statut))
            {
                SynchroAccountService synchroService = new SynchroAccountService(Context, accountId);
                SYNCHRO_ACCOUNT_STATUT stat = ret.Statut.ToEnum<SYNCHRO_ACCOUNT_STATUT>();

                synchroService.LogSynchro(accountId, CIBLE.CIPRES, stat, string.Empty);
            }
            else
            {
                
            }

        }
    }
}
