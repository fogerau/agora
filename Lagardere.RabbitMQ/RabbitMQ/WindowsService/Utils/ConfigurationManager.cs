﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Config = System.Configuration;


namespace Utils
{
    public class ConfigurationManager
    {
        const string _RabbitMQHost = "RabbitMQHost";
        const string _RabbitMQUser = "RabbitMQUser";
        const string _RabbitMQPwd = "RabbitMQPwd";
        const string _CipresQueueName = "CipresQueueName";
        const string _GemMPQueueName = "GemMPQueueName";
        const string _ServerSMTP = "ServerSMTP";
        const string _RABBITADMINEMAIL = "RabbitAdminEmail";
        const string _CRMADMINEMAIL = "CrmAdminEmail";

        /// <summary>
        /// URL du CRM
        /// </summary>
        public static string RabbitMQHost
        {
            get { return GetValueByCode(_RabbitMQHost); }
        }

        // <summary>
        /// URL du CRM
        /// </summary>
        public static string RabbitMQUser
        {
            get { return GetValueByCode(_RabbitMQUser); }
        }

        // <summary>
        /// URL du CRM
        /// </summary>
        public static string RabbitMQPwd
        {
            get { return GetValueByCode(_RabbitMQPwd); }
        }

        // <summary>
        /// URL du CRM
        /// </summary>
        public static string CipresQueueName
        {
            get { return GetValueByCode(_CipresQueueName); }
        }

        // <summary>
        /// URL du CRM
        /// </summary>
        public static string GemMPQueueName
        {
            get { return GetValueByCode(_GemMPQueueName); }
        }

        // <summary>
        /// URL du CRM
        /// </summary>
        public static string ServerSMTP
        {
            get { return GetValueByCode(_ServerSMTP); }
        }

        public static string RABBITADMINEMAIL
        {
            get { return GetValueByCode(_RABBITADMINEMAIL); }
        }

        public static string CRMADMINEMAIL
        {
            get { return GetValueByCode(_CRMADMINEMAIL); }
        }

        /// <summary>
        /// Récupère la valeur correspondant à la clé dans l'appsetting
        /// </summary>
        /// <param name="key">Clé</param>
        /// <returns>la valeur</returns>
        public static string GetValueByCode(string key)
        {
            string value = string.Empty;
            if (Config.ConfigurationManager.AppSettings[key] != null)
            {
                value = System.Configuration.ConfigurationManager.AppSettings[key] as string;
            }
            return value;
        }
    }
}
