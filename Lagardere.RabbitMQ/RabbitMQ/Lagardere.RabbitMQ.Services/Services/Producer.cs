﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Definitions;
using Microsoft.Xrm.Client.Services;

namespace Services
{
    /// <summary>
    /// Classe abstraite d'un producteur RabbitMQ
    /// </summary>
    public abstract class Producer
    {
        private IConnection connection;
        private IModel channel;
        private string hostName, user, pwd, queueName, replyQueueName;
        private bool isListeningCallbacks;
        private QueueingBasicConsumer consumer;

        public string HostName
        {
            get { return hostName; }
            set { hostName = value; }
        }
        public string User
        {
            get { return user; }
            set { user = value; }
        }
        public string Pwd
        {
            get { return pwd; }
            set { pwd = value; }
        }
        public string QueueName
        {
            get { return queueName; }
            set { queueName = value; }
        }
        public bool IsListeningCallbacks
        {
            get { return isListeningCallbacks; }
            set { isListeningCallbacks = value; }
        }

        public Producer()
        {
            this.isListeningCallbacks = false;
        }

        public Producer(string hostName, string user, string pwd, string queueName)
            : this()
        {
            this.hostName = hostName;
            this.user = user;
            this.pwd = pwd;
            this.queueName = queueName;
        }

        public Producer(string hostName, string user, string pwd, string queueName, bool isListeningCallbacks)
            : this(hostName, user, pwd, queueName)
        {
            this.isListeningCallbacks = isListeningCallbacks;
        }

        /// <summary>
        /// Initialisation de la connexion avec RabbitMQ
        /// </summary>
        public void Launch()
        {

            ConnectionFactory factory = new ConnectionFactory();
            factory.UserName = user;
            factory.Password = pwd;
            factory.VirtualHost = "/";
            factory.Protocol = Protocols.DefaultProtocol;
            factory.HostName = hostName;
            factory.Port = AmqpTcpEndpoint.UseDefaultPort;
            factory.AutomaticRecoveryEnabled = true; //Permet de récupérer automatiquement la connexion (JIRA-762)

            // Création de la connexion
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            // Déclare un canal de communication qui pourra avoir plusieurs clients en terminaison
            channel.ExchangeDeclare(exchange: queueName, type: "fanout", durable: true);
            replyQueueName = channel.QueueDeclare(queue: queueName + "Callback", durable: true, exclusive: false, autoDelete: false, arguments: null).QueueName;
            consumer = new QueueingBasicConsumer(channel);
            channel.BasicConsume(queue: replyQueueName,
                                                           noAck: false,
                                                            consumer: consumer);
            if (IsListeningCallbacks)
                ExecuteLoop();
        }

        /// <summary>
        /// Boucle de traitement des callbacks reçus après un envoi de message au client
        /// </summary>
        private void ExecuteLoop()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    BasicDeliverEventArgs ea = (BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    Task.Factory.StartNew(() =>
                    {
                        string callback = Encoding.UTF8.GetString(ea.Body);
                        channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                        ProcessCallback(callback);
                    });
                }
            });
        }

        protected abstract void ProcessCallback(string callback);

        /// <summary>
        /// Méthode d'envoi d'un message
        /// </summary>
        public void SendMessage(string message)
        {
            try
            {
                IBasicProperties props = channel.CreateBasicProperties();
                props.ReplyTo = replyQueueName;

                byte[] messageBytes = Encoding.UTF8.GetBytes(message);
                // Publication du message sur la file qui répartira à tous les clients
                channel.BasicPublish(exchange: queueName,
                                     routingKey: "",
                                     basicProperties: props,
                                     body: messageBytes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

