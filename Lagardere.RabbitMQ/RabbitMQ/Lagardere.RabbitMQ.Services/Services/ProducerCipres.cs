﻿using Definitions;
using Microsoft.Xrm.Client.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Utils;

namespace Services
{
     [Serializable]
    public class ProducerCipres : Producer
    {

        public ProducerCipres(string hostName, string user, string pwd, string queueName)
            : base(hostName, user, pwd, queueName)
        { }


        /// <summary>
        /// Méthode de traitement du message envoyé par le producteur
        /// </summary>
        /// <returns>Retourne le callback à traiter pour le producteur</returns>
        protected override void ProcessCallback(string message)
        {
            RetourTiersAgoraCipres ret = RetourTiersAgoraCipres.Deserialize(message);

            Guid accountId = string.IsNullOrEmpty(ret.GUID) ? Guid.Empty : new Guid(ret.GUID);

            if (accountId != Guid.Empty && !string.IsNullOrEmpty(ret.Statut))
            {//création de la connection au CRM(connexion string dans App.config)
                using (var service = new OrganizationService("Xrm"))
                {

                    LocalContext context = new LocalContext(service);
                    SynchroAccountService synchroService = new SynchroAccountService(context, accountId);
                    SYNCHRO_ACCOUNT_STATUT stat = ret.Statut.ToEnum<SYNCHRO_ACCOUNT_STATUT>();
                    synchroService.LogSynchro(accountId, CIBLE.CIPRES, stat, string.Empty,ret.Message);
                }
            }
        }
       
    }
}
