﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Services
{

    public class DefaultService
    {
        private LocalContext _svcInterfaceContext;

        public DefaultService(IContext context)
        {
            _svcInterfaceContext = context as LocalContext;


        }

        public LocalContext Context
        {
            get { return _svcInterfaceContext; }
        }

        protected IOrganizationService OrganizationService
        {
            get { return _svcInterfaceContext.OrganizationService; }
        }

    }

    public class LocalContext : IContext
    {


        public LocalContext(IOrganizationService service)
        {
            OrganizationService = service;


        }

        internal IOrganizationService OrganizationService
        {
            get;

            private set;
        }


        public void Trace(string msg)
        {
           


        }
    }
}
