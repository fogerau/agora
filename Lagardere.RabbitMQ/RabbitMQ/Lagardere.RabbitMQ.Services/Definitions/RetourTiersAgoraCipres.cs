﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Definitions
{
    [Serializable]
    public class RetourTiersAgoraCipres
    {
        public string Statut { get; set; }
        public string GUID { get; set; }
        public string CodeCipres { get; set; }
        public string Message { get; set; }

        public RetourTiersAgoraCipres() { }

        public static string Serialize(RetourTiersAgoraCipres tData)
        {
            var serializer = new XmlSerializer(typeof(RetourTiersAgoraCipres));

            TextWriter writer = new StringWriter();
            serializer.Serialize(writer, tData);

            return writer.ToString();
        }

        public static RetourTiersAgoraCipres Deserialize(string tData)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(RetourTiersAgoraCipres));
                MemoryStream memStream = new MemoryStream(Encoding.Unicode.GetBytes(tData));

                return serializer.Deserialize(memStream) as RetourTiersAgoraCipres;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AgoraRabbitMQService", "deserialize RetourTiersAgoraCipres " + ex.Message, EventLogEntryType.Error);
                return null;
            }
        }
    }

    [Serializable]
    public class RetourTiersAgora
    {
        public string Application { get; set; }
        public string Statut { get; set; }
        public string GUID { get; set; }
        public string CodeCipres { get; set; }
        public string Message { get; set; }

        public RetourTiersAgora() { }

        public static string Serialize(RetourTiersAgora tData)
        {
            var serializer = new XmlSerializer(typeof(RetourTiersAgora));

            TextWriter writer = new StringWriter();
            serializer.Serialize(writer, tData);

            return writer.ToString();
        }

        public static RetourTiersAgora Deserialize(string tData)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(RetourTiersAgora));
                MemoryStream memStream = new MemoryStream(Encoding.Unicode.GetBytes(tData));

                return serializer.Deserialize(memStream) as RetourTiersAgora;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AgoraRabbitMQService", "Deserialize RetourTiersAgora " + ex.Message, EventLogEntryType.Error);
                return null;
            }
        }
    }

    [Serializable]
    public class Tiers
    {
        public Tiers() { }
        public Tiers(string guid, string codecipres)
        {
            this.CodeCipres = codecipres;
            this.GUID = guid;
        }


        public string GUID { get; set; }
        public string CodeCipres { get; set; }

        public static string Serialize(Tiers tData)
        {
            var serializer = new XmlSerializer(typeof(Tiers));

            TextWriter writer = new StringWriter();
            serializer.Serialize(writer, tData);

            return writer.ToString();
        }

        public static Tiers Deserialize(string tData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Tiers));
            MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(tData));

            return serializer.Deserialize(memStream) as Tiers;
        }

    }
}
