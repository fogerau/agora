﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Utils
{
    public static class EnumExtensions
    {
        public static T ToEnum<T>(this int value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }

        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value);
        }

        public static string GetDescription(this Enum enumValue)
        {
            var descriptionAttribute = enumValue.GetType().GetField(enumValue.ToString()).GetCustomAttribute<DescriptionAttribute>();

            return descriptionAttribute == null ? null : descriptionAttribute.Description;
        }

        public static Int32 ToInt(this Enum enumValue)
        {
            return Convert.ToInt32(enumValue);
        }

        public static OptionSetValue ToOptionSetValue(this Enum enumValue)
        {
            return new OptionSetValue(enumValue.ToInt());
        }
    }

}
