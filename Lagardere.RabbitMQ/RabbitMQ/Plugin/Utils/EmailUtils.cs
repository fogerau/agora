﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class EmailUtils
    {
        public static bool mailSent = false;

        public static void SendMail(string titre, string msg, string destinataires, string serverSMTP, string from)
        {
            string chaineSmtp = serverSMTP;
            System.Net.Mail.SmtpClient smtpclient = new System.Net.Mail.SmtpClient(chaineSmtp);
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

            // Set destinations for the e-mail message.
            foreach (string dest in destinataires.Split(';'))
            {
                System.Net.Mail.MailAddress destinataire = new System.Net.Mail.MailAddress(dest);
                message.To.Add(destinataire);
            }
            message.From = new System.Net.Mail.MailAddress(from);

            // Specify the message content.                
            message.Body = msg;
            // Include some non-ASCII characters in body and subject.
            
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Subject = titre;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            // Set the method that is called back when the send operation ends.
            smtpclient.SendCompleted += new
            SendCompletedEventHandler(SendCompletedCallback);
            // The userState can be any object that allows your callback 
            // method to identify this send operation.
            // For this example, the userToken is a string constant.
            string userState = titre;
            smtpclient.SendAsync(message, userState);

            // Clean up.
            message.Dispose();

        }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message sent.");
            }
            mailSent = true;
        }


    }
}
