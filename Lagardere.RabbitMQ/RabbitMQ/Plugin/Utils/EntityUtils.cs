﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class EntityUtils
    {

        /// <summary>
        /// Compare avec l'imagePre pour savoir si les champs Cipres Obligatoire ont été modifiés
        /// </summary>
        /// <returns></returns>
        public static bool isDirties(Entity currentEntity, Entity preEntity, Type definition)
        {
            IList<PropertyInfo> props = new List<PropertyInfo>(definition.GetProperties());

            foreach (PropertyInfo prop in props)
            {
                var currValue = currentEntity.Attributes[prop.Name];
                var preValue = preEntity.Attributes[prop.Name];
                if (currValue != preValue)
                    return false;

                // Do something with propValue
            }

            return true;
        }


        public static bool isCompleted(Entity currentEntity, Entity preEntity, Type definition)
        {
            IList<PropertyInfo> props = new List<PropertyInfo>(definition.GetProperties());

            foreach (PropertyInfo prop in props)
            {
                var currValue = currentEntity.Attributes[prop.Name];
                var preValue = preEntity.Attributes[prop.Name];
                if (currValue != null && currValue  !=null)
                    return false;

                // Do something with propValue
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="targetEntity"></param>
        /// <param name="preIMG"></param>
        /// <param name="fieldMeta"></param>
        /// <returns></returns>
        public static T getValue<T>(Entity targetEntity, Entity preIMG, string fieldMeta)
        {
            T rtr = default(T);
            if (targetEntity.Contains(fieldMeta))
            {
                rtr = targetEntity.GetAttributeValue<T>(fieldMeta);
            }
            else if (preIMG.Contains(fieldMeta))
                rtr = preIMG.GetAttributeValue<T>(fieldMeta);

            return rtr;
        }

    }
}
