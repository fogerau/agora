﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    public class Definition
    {
        public Type DefinitionType { get; private set; }

        public Definition(Type type)
        {
            DefinitionType = type;
        }

        public string EntityName
        {
            get
            {
                return DefinitionType.GetField("EntityName").GetValue(null) as string;
            }
        }

        public IEnumerable<CrmLookupAttribute> GetCrmLookupAttributes(string attributeName)
        {
            var field = DefinitionType.GetNestedType("Columns").GetFields().Single(f => (f.GetValue(null) as string) == attributeName);

            return field.GetCustomAttributes<CrmLookupAttribute>();
        }

        public string PrimaryIdAttributeName
        {
            get
            {
                var field = DefinitionType.GetNestedType("Columns").GetFields().First(f => f.GetCustomAttribute<PrimaryAttributeAttribute>() != null);

                return field.GetValue(null) as string;
            }
        }

        public AttributeTypeCode GetAttributeType(string attributeName)
        {
            var field = DefinitionType.GetNestedType("Columns").GetFields().Single(f => (f.GetValue(null) as string) == attributeName);

            var attr = field.GetCustomAttribute<AttributeMetadataAttribute>();

            return attr != null ? attr.Type : AttributeTypeCode.String;
        }

        public bool IsPrimaryAttribute(string attributeName, PrimaryAttributeType type)
        {
            var field = DefinitionType.GetNestedType("Columns").GetFields().FirstOrDefault(f => (f.GetValue(null) as string) == attributeName);

            if (field != null)
            {
                var primary = field.GetCustomAttribute<PrimaryAttributeAttribute>();

                return primary != null && primary.Type == type;
            }
            return false;
        }

        public IEnumerable<Relationship> GetRelationshipsFromAttributeName(string attributeName)
        {
            var field = DefinitionType.GetNestedType("Columns").GetFields().Single(f => (f.GetValue(null) as string) == attributeName);

            var relationshipAttributes = field.GetCustomAttributes<CrmLookupAttribute>();

            return relationshipAttributes.Select(crmRelationshipAttribute => crmRelationshipAttribute.GetRelationship());
        }

        //public Relationship GetRelationshipFromAttributeName(string attributeName)
        //{
        //    return GetRelationshipsFromAttributeName(attributeName).FirstOrDefault();
        //}

        //public Definition GetSubDefinition(string attributeName, string entityName = null)
        //{
        //    var lookupAttributes = GetCrmLookupAttributes(attributeName);

        //    var lookupAttribute = lookupAttributes.FirstOrDefault(l => string.IsNullOrEmpty(entityName) || l.TargetEntityName == entityName);
        //    if (lookupAttribute == null)
        //    {
        //        throw new KeyNotFoundException(string.Format("Attribute {0} is not a lookup attribute", attributeName));
        //    }

        //    return DefinitionCache.GetDefinition(lookupAttribute.TargetEntityName);
        //}

        public bool IsLookupAttribute(string attributeName)
        {
            var attributeType = GetAttributeType(attributeName);
            return attributeType == AttributeTypeCode.Lookup || attributeType == AttributeTypeCode.Owner || attributeType == AttributeTypeCode.Customer;
        }

        public bool IsKey(string attributeName)
        {
            var field = DefinitionType.GetNestedType("Columns").GetFields().Single(f => (f.GetValue(null) as string) == attributeName);
            return field.GetCustomAttribute<KeyAttribute>() != null;
        }
    }
}
