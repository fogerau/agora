﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Definitions
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true)]
    public class CrmLookupAttribute : Attribute
    {
        public CrmLookupAttribute(string targetEntityName, string attributeName, bool allowNotExisting = false)
        {
            TargetEntityName = targetEntityName;
            AttributeName = attributeName;
            AllowNotExisting = allowNotExisting;
        }
        public CrmLookupAttribute(Type definitionType, string attributeName, bool allowNotExisting = false)
        {
            DefinitionType = definitionType;
            AttributeName = attributeName;
            AllowNotExisting = allowNotExisting;
        }


        public Relationship GetRelationship()
        {
            var relationship = new Relationship(RelationshipName);
            relationship.PrimaryEntityRole = EntityRole.Referencing;

            return relationship;
        }

        public string RelationshipName { get; set; }

        private Type DefinitionType { get; set; }

        public string TargetEntityName { get; private set; }

        public string AttributeName { get; private set; }

        public bool AllowNotExisting { get; private set; }
    }
}
