﻿using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AttributeMetadataAttribute : Attribute
    {
        public AttributeMetadataAttribute(AttributeTypeCode type)
        {
            Type = type;
        }

        public AttributeTypeCode Type { get; private set; }
    }
}
