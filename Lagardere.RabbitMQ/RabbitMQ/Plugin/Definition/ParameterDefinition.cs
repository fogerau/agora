﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    public class ParameterDefinition 
    {
        public const string EntityName = "bd_parametre";

        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public static class Columns
        {
            /// <summary>
            /// Nom
            /// Type : String            
            /// </summary>
            public const string Nom = "bd_name";

            /// <summary>
            /// Valeur (Texte)
            /// Type : String            
            /// </summary>
            public const string ValeurTexte = "bd_textvalue";

            /// <summary>
            /// Type de parametre
            /// Type : Picklist            
            /// </summary>
            public const String TypeParametre = "bd_typeparametre";

            /// <summary>
            /// Caracteristique
            /// Type : String            
            /// </summary>
            public const string Caracteristique = "bd_caracteristique";
        }
    }
}
