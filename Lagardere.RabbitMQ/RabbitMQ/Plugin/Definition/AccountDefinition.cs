﻿using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    [EntityDefinition]
    public class AccountDefinition 
    {
        public const string EntityName = "account";

        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public static class Columns
        {
            /// <summary>
            /// Nom
            /// Type : String            
            /// </summary>
            public const string Nom = "name";
            
            /// <summary>
            /// Adresse légale : Adresse
            /// Type : String            
            /// </summary>
            public const string AddrLegaleAddr = "address2_line1";

            /// <summary>
            /// Adresse légale : CP
            /// Type : String            
            /// </summary>
            public const string AddrLegaleCp = "address2_postalcode";

            /// <summary>
            /// Adresse légale : Ville
            /// Type : String            
            /// </summary>
            public const string AddrLegalecity = "address2_city";

            /// <summary>
            /// Adresse légale : Pays
            /// Type : Lookup --> entity : bd_pays           
            /// </summary>
            public const string AddrLegalePays = "bd_payslegal";

            /// <summary>
            /// Adresse de facturation : Adresse
            /// Type : String            
            /// </summary>
            public const string AddrFacturationAdd = "bd_adresse3";

            /// <summary>
            /// Adresse de facturation : CP
            /// Type : String            
            /// </summary>
            public const string AddrFacturationCp = "bd_codepostal3";

            /// <summary>
            /// Adresse de facturation : Ville
            /// Type : String            
            /// </summary>
            public const string AddrFacturationVille = "bd_ville3";



            /// <summary>
            /// Adresse de facturation : Pays
            /// Type : Lookup --> entity : bd_pays             
            /// </summary>
            public const string AddrFacturationPays = "bd_paysfacturation";


            /// <summary>
            /// Type (Annonceur ou Agence)
            /// Type :             
            /// </summary>
            public const string Type = "customertypecode";


            /// <summary>
            /// Langue
            /// Type : Lookup --> entity : bd_langue              
            /// </summary>
            public const string Langue = "bd_langueid";


            /// <summary>
            /// Devise
            /// Type : Lookup --> entity : bd_deviseciprs                 
            /// </summary>
            public const string Devise = "bd_devisecipres";


            /// <summary>
            /// Code TVA client
            /// Type : Lookup --> entity : bd_tva       
            /// </summary>
            public const string CodeTVAclient = "bd_codetvaclientid";


            /// <summary>
            /// Code TVA fournisseur
            /// Type : Lookup --> entity : bd_tva               
            /// </summary>
            public const string CodeTVAfournisseur = "bd_codetvafournisseurid";


            /// <summary>
            /// Niveau (de risque)
            /// Type : Lookup --> entity : bd_niveauderisque             
            /// </summary>
            public const string NiveauRisque = "bd_niveauderisqueid";

            /// <summary>
            /// Encours autorisé Cipres
            /// Type :             
            /// </summary>
            public const string EncoursAutoriseCipres = "bd_encoursautorisecipres";


            /// <summary>
            /// Encours conseillé
            /// Type : String            
            /// </summary>
            public const string EncoursConseille = "bd_encoursconseille";


            /// <summary>
            /// Titrisable
            /// Type : String            
            /// </summary>
            public const string Titrisable = "bd_titrisable";


            /// <summary>
            /// Titrisé
            /// Type : String            
            /// </summary>
            public const string Titrise = "bd_titrise";


            /// <summary>
            /// Mode de règlement national OU
            /// Type : Lookup --> entity : bd_modedereglement             
            /// </summary>
            public const string ModeReglementNatio = "bd_modederglementnationalid";


            /// <summary>
            /// Mode de règlement métropole OU
            /// Type : Lookup --> entity : bd_modedereglement             
            /// </summary>
            public const string ModeReglementMetro = "bd_modedereglementmetropoleid";


            /// <summary>
            /// Mode de règlement international
            /// Type : Lookup --> entity : bd_modedereglement            
            /// </summary>
            public const string ModeReglementInter = "bd_modedereglementinternationalid";


            /// <summary>
            /// Échéance de règlement national OU
            /// Type : Lookup --> entity : bd_echeancedereglement           
            /// </summary>
            public const string EcheanceReglementNat = "bd_echeancedereglementnationalid";


            /// <summary>
            /// Échéance de règlement métropole OU
            /// Type : Lookup --> entity : bd_echeancedereglement               
            /// </summary>
            public const string EcheanceReglementMetro = "bd_echeancedereglementmetropoleid";


            /// <summary>
            /// Échéance de règlement international
            /// Type : Lookup --> entity : bd_echeancedereglement                
            /// </summary>
            public const string EcheanceReglementInter = "bd_echeancedereglementinternatioid";


            /// <summary>
            /// Compte collectif
            /// Type : String            
            /// </summary>
            public const string CompteCollectif = "";

            /// <summary>
            /// Contact interne national OU
            /// Type : Lookup --> entity : bd_contactinterne            
            /// </summary>
            public const string ContactInterneNationalOU = "bd_contactinternenationalid";


            /// <summary>
            /// Contact interne métropole OU
            /// Type : Lookup --> entity : bd_contactinterne             
            /// </summary>
            public const string ContactInterneMetroOU = "bd_contactinternemetropoleid";


            /// <summary>
            /// Contact interne international
            /// Type : Lookup --> entity : bd_contactinterne            
            /// </summary>
            public const string ContactInterneInter = "bd_contactinterneinternationalid";


            /// <summary>
            /// Régime fiscal
            /// Type : Lookup --> entity : bd_regimefiscal           
            /// </summary>
            public const string RegimeFiscal = "bd_regimefiscalid";


            /// <summary>
            /// Imputation vente
            /// Type : Lookup --> entity : bd_imputationvente              
            /// </summary>
            public const string ImputationVente = "bd_imputationventeid";


            /// <summary>
            /// Num TVA Intracommunautaire
            /// Type :             
            /// </summary>
            public const string NumTVAIntracomm = "bd_numintracommunautaire";


            /// <summary>
            /// Date de synchro mandat
            /// Type : Date & Time            
            /// </summary>
            public const string DateSyncMandat = "bd_datesyncromandat";

            /// <summary>
            /// No SIRET
            /// Type : String            
            /// </summary>
            [AttributeMetadata(AttributeTypeCode.String)]
            public const string NoSIRET = "bd_siret";

            /// <summary>
            /// Doublons vérifié
            /// Type : Booleen            
            /// </summary>
            [AttributeMetadata(AttributeTypeCode.Boolean)]
            public const string DoublonVerifie = "bd_doublon";

            /// <summary>
            /// Code Cipres
            /// </summary>
            [AttributeMetadata(AttributeTypeCode.String)]
            public const string CodeCipres = "bd_codecipres";
            
        }


    }
}
