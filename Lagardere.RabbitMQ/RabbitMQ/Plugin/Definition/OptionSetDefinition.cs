﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    
    public enum TYPE_PARAMETRE
    {
        /// <summary>
        /// Booleen
        /// </summary>
        Booleen = 117280000,
        /// <summary>
        /// Date
        /// </summary>
        Date = 117280001,
        /// <summary>
        /// Date et heure
        /// </summary>
        DateEtHeure = 117280002,
        /// <summary>
        /// Decimal
        /// </summary>
        Decimal = 117280003,
        /// <summary>
        /// Entier
        /// </summary>
        Entier = 117280004,
        /// <summary>
        /// Memo
        /// </summary>
        Memo = 117280005,
        /// <summary>
        /// Booleen
        /// </summary>
        Devise = 117280006,
        /// <summary>
        /// Booleen
        /// </summary>
        Texte = 117280007,
        /// <summary>
        /// Utilisateur CRM
        /// </summary>
        UtilisateurCRM = 117280008,
        /// <summary>
        /// ID Compte
        /// </summary>
        IDCompte = 117280009

    }

    public enum CARACTERISTIQUE
    {
        /// <summary>
        /// RabbitMQ
        /// </summary>
        RabbitMQ = 899240000,
        /// <summary>
        /// WS
        /// </summary>
        WS = 899240001,
        SMTP
    }

    public enum ACCOUNT_TYPE
    {
        /// <summary>
        /// Agence
        /// </summary>
        Null = -1,
        
        /// <summary>
        /// Agence
        /// </summary>
        AgenceMedia = 899240005,
        AgenceConseil = 899240006,
        AgenceRP = 899240007,
        /// <summary>
        /// Annonceur
        /// </summary>
        Annonceur = 899240000,
        /// <summary>
        /// Editeur
        /// </summary>
        Editeur =899240003,
        /// <summary>
        /// Relation Presse
        /// </summary>
        RelationPresse=899240004,
        /// <summary>
        /// Tiers de facturation Annonceur
        /// </summary>
        TiersFacturationAnnonceur=100000000,
        /// <summary>
        /// Autre
        /// </summary>
        Autre = 12

    }

    public enum CIBLE
    {
        /// <summary>
        /// Agence
        /// </summary>
        Null = -1,
        /// <summary>
        /// CIPRES
        /// </summary>
        [Description("CIPRES")]
        CIPRES = 899240000,
        /// <summary>
        /// GEMME
        /// </summary>
        [Description("GEMME")]
        GEMME = 899240001,
        /// <summary>
        /// MEDIAPILOT
        /// </summary>
        [Description("MEDIAPILOT")]
        MEDIAPILOT = 899240002,
        /// <summary>
        /// Tous
        /// </summary> 
        [Description("Tous")]       
        Tous = 899240003,
        /// <summary>
        /// AdResa News
        /// </summary> 
        [Description("ADRESA")]
        ADRESA = 899240004,
        /// <summary>
        /// AdResa Presse
        /// </summary> 
        /*[Description("ADRESA PRESSE")]
        ADRESA_PRESSE = 899240005*/
    }

    public enum SYNCHRO_ACCOUNT_STATUT
    {
       
        Null = -1,
        /// <summary>
        /// En cours
        /// </summary>
        [Description("En cours")]
        EnCours = 899240000,
        /// <summary>
        /// Réussi
        /// </summary>
        [Description("Réussi")]
        Reussi = 899240001,
        /// <summary>
        /// OK Ajouté par Fred le 10/02
        /// </summary>
        [Description("OK")]
        OK = 899240001,
        /// Échoué
        /// </summary>
        [Description("Échoué")]
        Echoue = 899240002,
        /// <summary>
        /// KO Ajouté par Fred le 29/01
        /// </summary>
        [Description("KO")]
        KO = 899240002,
    }


}
