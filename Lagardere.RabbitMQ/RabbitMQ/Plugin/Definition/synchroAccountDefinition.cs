﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    public class synchroAccountDefinition
    {
        public const string EntityName = "bd_synchrocomptes";

        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public static class Columns
        {
            /// <summary>
            /// Titre
            /// Type : String            
            /// </summary>
            public const string Titre = "bd_titre";

            /// <summary>
            /// Titre
            /// Type : String            
            /// </summary>
            public const string Compte = "bd_compte";

            /// <summary>
            /// Statut
            /// Type : String            
            /// </summary>
            public const string Statut = "bd_statut";

            /// <summary>
            /// Cible
            /// Type : Picklist            
            /// </summary>
            public const string Cible = "bd_cible";

            /// <summary>
            /// Message d'erreur
            /// Type : String            
            /// </summary>
            public const string Message = "bd_msgerreur";
        }
    }
}
