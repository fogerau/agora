﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using System.Collections.ObjectModel;
using System.Globalization;
using System.ServiceModel;
using Services;

namespace Plugins
{
    public class PluginBase<TEntity> : IPlugin where TEntity : Entity, new()
    {

        public PluginBase()
        {

        }


        public LocalPluginContext Localcontext
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the name of the child class.
        /// </summary>
        /// <value>The name of the child class.</value>
        protected string ChildClassName
        {
            get { return this.GetType().Name; }
        }

        

        protected bool isSetSate
        {
            get { return Localcontext.PluginExecutionContext.MessageName == "SetState" || Localcontext.PluginExecutionContext.MessageName == "SetStateDynamicEntity"; }
        }

        public TEntity TargetEntity { get; private set; }

        /// <summary>
        /// get the preimage with the name passed as parameter
        /// </summary>
        /// <param name="name">the name of the pre image</param>
        /// <returns></returns>
        public TEntity GetPreEntityImage(string name)
        {
            if (Localcontext.PluginExecutionContext.PreEntityImages.Contains(name))
                return (TEntity)Localcontext.PluginExecutionContext.PreEntityImages[name];
            else
                if (Localcontext.PluginExecutionContext.PostEntityImages.Contains(name))
                    return (TEntity)Localcontext.PluginExecutionContext.PostEntityImages[name];
                else
                    return null;
        }

        /// <summary>
        /// get the postimage with the name passed as parameter
        /// </summary>
        /// <param name="name">the name of the postimage</param>
        /// <returns></returns>
        public TEntity GetPostEntityImage(string name)
        {

            if (Localcontext.PluginExecutionContext.PostEntityImages.Contains(name))
                return (TEntity)Localcontext.PluginExecutionContext.PostEntityImages[name];
            else
                return null;

        }

        public TEntity PostEntityImage
        {
            get
            {
                if (Localcontext.PluginExecutionContext.PostEntityImages.Count > 0)
                    return Localcontext.PluginExecutionContext.PostEntityImages.Values.FirstOrDefault() as TEntity;
                else
                    return null;
            }
        }

        public int State
        {
            get
            {
                int _state = -1;
                if (Localcontext.PluginExecutionContext.InputParameters.Contains("State") && Localcontext.PluginExecutionContext.InputParameters["State"] != null)
                {
                    _state = ((OptionSetValue)Localcontext.PluginExecutionContext.InputParameters["State"]).Value;
                }
                return _state;
            }
        }


        public EntityReference TargetEntityRef { get; private set; }

        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances. 
        /// The plug-in's Execute method should be written to be stateless as the constructor 
        /// is not called for every invocation of the plug-in. Also, multiple system threads 
        /// could execute the plug-in at the same time. All per invocation state information 
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        public void Execute(IServiceProvider serviceProvider)
        {

            if (serviceProvider == null)
            {
                throw new ArgumentNullException("serviceProvider");
            }

            // Construct the Local plug-in context.
            Localcontext = new LocalPluginContext(serviceProvider);
            Localcontext.Trace("Start plugin");
            try
            {
                if (Localcontext.PluginExecutionContext.InputParameters.Contains("Target"))
                {
                    // Get Updated Entity from parameter and convert to correct type
                    if (Localcontext.PluginExecutionContext.InputParameters["Target"] is Entity)
                    {
                        var entityFromContext = (Entity)Localcontext.PluginExecutionContext.InputParameters["Target"];
                        TargetEntity = entityFromContext.ToEntity<TEntity>();
                    }
                    else if (Localcontext.PluginExecutionContext.InputParameters["Target"] is EntityReference)
                    {
                        TargetEntityRef = (EntityReference)Localcontext.PluginExecutionContext.InputParameters["Target"];
                    }
                }

                if (Localcontext.PluginExecutionContext.InputParameters.Contains("EntityMoniker"))
                {
                    TargetEntityRef = (EntityReference)Localcontext.PluginExecutionContext.InputParameters["EntityMoniker"];
                }
                Localcontext.Trace(string.Format("Start : {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff")));
                Localcontext.Trace(string.Format(
                    CultureInfo.InvariantCulture,
                    "{0} is firing for Entity: {1}, Message: {2}",
                    this.ChildClassName,
                    Localcontext.PluginExecutionContext.PrimaryEntityName,
                    Localcontext.PluginExecutionContext.MessageName));

                Execute();
            }

            catch (FaultException<OrganizationServiceFault> e)
            {
                Localcontext.Trace(string.Format("Correlation Id: {0}, Initiating User: {1}",
                    Localcontext.PluginExecutionContext.CorrelationId,
                    Localcontext.PluginExecutionContext.InitiatingUserId));

                Localcontext.Trace(string.Format(CultureInfo.InvariantCulture, "Exception: {0}", e.ToString()));

                // Handle the exception.
                throw;
            }
            finally
            {
                Localcontext.Trace(string.Format(CultureInfo.InvariantCulture, "Exiting {0}.Execute()", this.ChildClassName));
            }
        }

        /// <summary>
        /// Child classes should override the execute method.
        /// </summary>
        public virtual void Execute()
        {
        }

       

    }


    public class LocalPluginContext : IContext
    {
        internal IServiceProvider ServiceProvider
        {
            get;

            private set;
        }

        internal IOrganizationService OrganizationService
        {
            get;

            private set;
        }

        internal IPluginExecutionContext PluginExecutionContext
        {
            get;

            private set;
        }

        internal ITracingService TracingService
        {
            get;

            private set;
        }

        private LocalPluginContext()
        {
        }

        internal LocalPluginContext(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
            {
                throw new ArgumentNullException("serviceProvider");
            }

            // Obtain the execution context service from the service provider.
            this.PluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // Obtain the tracing service from the service provider.
            this.TracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the Organization Service factory service from the service provider
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            // Use the factory to generate the Organization Service.
            this.OrganizationService = factory.CreateOrganizationService(this.PluginExecutionContext.UserId);
        }

        public void Trace(string message)
        {
            if (string.IsNullOrWhiteSpace(message) || this.TracingService == null)
            {
                return;
            }

            if (this.PluginExecutionContext == null)
            {
                this.TracingService.Trace(message);
            }
            else
            {
                this.TracingService.Trace("{0}", message);
            }
        }
    }

}
