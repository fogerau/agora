﻿using Microsoft.Xrm.Sdk;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plugins
{
    public class AgoraToRabbitMQsynch : PluginBase<Entity>, IPlugin
    {
        public override void Execute()
        {
            Localcontext.Trace("Execute AgoraToRabbitMQsynch");
            RabbitMQservice service = new RabbitMQservice(Localcontext);

            Entity v_PreImage = GetPreEntityImage("PreImg");
            SynchroAccountService sas = new SynchroAccountService(Localcontext, v_PreImage.Id);
            Boolean v_IsCreation = sas.IsStatutSynchroCipresCreation(v_PreImage.Id);

            if (!v_IsCreation)
                service.SendToQueue(TargetEntity, GetPreEntityImage("PreImg"));
        }
    }
}
