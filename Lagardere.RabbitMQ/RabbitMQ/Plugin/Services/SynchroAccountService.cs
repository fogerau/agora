﻿
using Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Definitions;
using System.Diagnostics;
using System.Web.Services.Protocols;


namespace Services
{
    public class SynchroAccountService : DefaultService
    {
        private Dictionary<CIBLE, Guid> ListSynchroAccount;

        public SynchroAccountService(IContext context, Guid idAccount)
            : base(context)
        {

            ListSynchroAccount = RetrieveAllSynchroAccount(idAccount);
        }

        public Dictionary<CIBLE, Guid> RetrieveAllSynchroAccount(Guid idAccount)
        {
            Dictionary<CIBLE, Guid> listeCible = new Dictionary<CIBLE, Guid>();

            try
            {
                QueryExpression qe = new QueryExpression(synchroAccountDefinition.EntityName);
                qe.ColumnSet = new ColumnSet(true);
                qe.Criteria.AddCondition(synchroAccountDefinition.Columns.Compte, ConditionOperator.Equal, idAccount);
                qe.AddOrder("modifiedon", OrderType.Descending);

                EntityCollection ec = OrganizationService.RetrieveMultiple(qe);

                foreach (Entity synch in ec.Entities)
                {
                    if (synch.Contains(synchroAccountDefinition.Columns.Cible) && synch[synchroAccountDefinition.Columns.Cible] != null)
                    {
                        CIBLE cible = synch.GetAttributeValue<OptionSetValue>(synchroAccountDefinition.Columns.Cible).Value.ToEnum<CIBLE>();
                        if (!listeCible.ContainsKey(cible))
                            listeCible.Add(cible, synch.Id);
                        else
                            OrganizationService.Delete(synchroAccountDefinition.EntityName, synch.Id);
                    }
                }
            }
            catch (SoapException sEx)
            {
                EventLog.WriteEntry("AgoraRabbitMQService", String.Format("SynchroAccountService RetrieveAllSynchroAccount Soap : {0}", sEx.Detail.InnerText), EventLogEntryType.Error);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AgoraRabbitMQService", String.Format("SynchroAccountService RetrieveAllSynchroAccount : {0}", ex.Message), EventLogEntryType.Error);
            }

            return listeCible;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAccount"></param>
        /// <param name="cible"></param>
        /// <param name="statut"></param>
        /// <param name="accountName"></param>
        public void LogSynchro(Guid idAccount, CIBLE cible, SYNCHRO_ACCOUNT_STATUT statut, string accountName, string msg)
        {
            try
            {
                EventLog.WriteEntry("AgoraRabbitMQService", msg + " " + statut.ToString(), EventLogEntryType.Information);
                Context.Trace("Création du log de la synchro compte");
                Entity log = new Entity(synchroAccountDefinition.EntityName);
                log[synchroAccountDefinition.Columns.Statut] = new OptionSetValue(statut.ToInt());
                log[synchroAccountDefinition.Columns.Message] = msg;
                EventLog.WriteEntry(  "AgoraRabbitMQService", " create? "+!ListSynchroAccount.ContainsKey(cible), EventLogEntryType.Information);
                foreach (var elt in ListSynchroAccount.Keys)
                {
                    EventLog.WriteEntry("AgoraRabbitMQService", " cible  " + elt, EventLogEntryType.Information);
                
                }
                if (!ListSynchroAccount.ContainsKey(cible))
                {
                    EventLog.WriteEntry("AgoraRabbitMQService", "Create entry", EventLogEntryType.Information);
            
                    log[synchroAccountDefinition.Columns.Titre] = string.Format("Synchro entre CRM et {0} pour le compte {1}", cible.GetDescription(), accountName);
                    log[synchroAccountDefinition.Columns.Compte] = new EntityReference(AccountDefinition.EntityName, idAccount);
                    log[synchroAccountDefinition.Columns.Cible] = new OptionSetValue(cible.ToInt());

                    Guid id= OrganizationService.Create(log);
                }
                else
                {
                    EventLog.WriteEntry("AgoraRabbitMQService", "Update entry", EventLogEntryType.Information);
                    log.Id = ListSynchroAccount[cible];
                    OrganizationService.Update(log);
                }

                Context.Trace("Purge des logs");
                ListSynchroAccount = RetrieveAllSynchroAccount(idAccount);
                Context.Trace("Création du log de la synchro compte réussi");
            }
            catch (SoapException sEx)
            {
                EventLog.WriteEntry("AgoraRabbitMQService", String.Format("SynchroAccountService LogSynchro Soap : {0}", sEx.Detail.InnerText), EventLogEntryType.Error);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AgoraRabbitMQService", "SynchroAccountService LogSynchro " + ex.Message, EventLogEntryType.Error);
            }
        }

        public void LogSynchro(Guid accountID, string cible, string statut, string accountName, string msg)
        {
            CIBLE target = cible.ToEnum<CIBLE>();
            SYNCHRO_ACCOUNT_STATUT stat = statut.ToEnum<SYNCHRO_ACCOUNT_STATUT>();

            LogSynchro(accountID, target, stat, accountName, msg);
        }

        /// <summary>
        /// Mise à jour du code cipres de l'annonceur
        /// </summary>
        /// <param name="idAccount">Guid de l'annonceur</param>
        /// <param name="CodeCipres">Code Cipres</param>
        public void UpdateAccount(Guid idAccount, String CodeCipres)
        {
            try
            {
                Entity Account = new Entity("account");
                Account.Attributes["accountid"] = idAccount;
                Account.Attributes["bd_codecipres"] = CodeCipres;
                OrganizationService.Update(Account);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Permet de savoir si la société contient déjà un code cipres
        /// </summary>
        /// <param name="p_AccountId">AccountId de la société</param>
        /// <returns>True or False</returns>
        public Boolean IsCodeCipresExist(Guid p_AccountId)
        {
            Boolean v_bCodeCipres = false;
            try
            {
                ColumnSet cols = new ColumnSet(new String[] { "bd_codecipres" });

                Entity v_Account = OrganizationService.Retrieve("account", p_AccountId, cols);

                if (v_Account.Contains("bd_codecipres") && !String.IsNullOrEmpty(v_Account.Attributes["bd_codecipres"].ToString()))
                    v_bCodeCipres = true;

                return v_bCodeCipres;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("{0} : {1} ", "IsCodeCipresExist", ex.Message));
            }
        }

        /// <summary>
        /// Est-ce que la synchro Ciprès est en cours ?
        /// </summary>
        /// <param name="AccountId"></param>
        /// <returns>Vrai ou Faux</returns>
        public Boolean IsStatutSynchroCipresCreation(Guid AccountId)
        {
            Boolean v_IsStatutCreation = false;
            try
            {
                QueryExpression qr = new QueryExpression(synchroAccountDefinition.EntityName);
                qr.ColumnSet = new ColumnSet("bd_statut");
                qr.Criteria.AddCondition(new ConditionExpression("bd_compte", ConditionOperator.Equal, AccountId));
                qr.Criteria.AddCondition(new ConditionExpression("bd_cible", ConditionOperator.Equal, "899240000")); // Cipres
                EntityCollection synchro_comptes = OrganizationService.RetrieveMultiple(qr);

                if (synchro_comptes.Entities.Count > 0)
                {
                    if (((OptionSetValue)synchro_comptes.Entities[0].Attributes["bd_statut"]).Value == 899240000) // Statut en cours
                        v_IsStatutCreation = true;
                    else
                        v_IsStatutCreation = false;
                }   
            }
            catch (Exception)
            {
                throw;
            }
            return v_IsStatutCreation;
        }
    }
}
