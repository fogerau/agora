﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Services
{
    /// <summary>
    /// Classe de l'application occupant le rôle de serveur qui répartit les envois de Code Cipres aux clients
    /// </summary>
    public class producer : DefaultRabbitMQ
    {        

        /// <summary>
        /// Initialisation de la connexion avec RabbitMQ
        /// </summary>
        public producer(string rabbitMQHost, string rabbitMQUser, string rabbitMQPwd, IContext pcontext) : base(rabbitMQHost, rabbitMQUser, rabbitMQPwd, pcontext) { }



        /// <summary>
        /// Méthode d'envoi d'un message à tous les clients contenant le Code Cipres 
        /// </summary>
        /// <param name="codeCipres">Code de l'annonceur</param>
        public void Call(string message,string sQueueName)
        {
            queueName = sQueueName; // le nom de la file doit avoir le même nom sur les 2 applications
            Context.Trace("Déclare un canal de communication qui pourra avoir plusieurs clients en terminaison");
            // Déclare un canal de communication qui pourra avoir plusieurs clients en terminaison
            channel.ExchangeDeclare(exchange: queueName, type: "fanout");
            replyQueueName = channel.QueueDeclare().QueueName;


            Context.Trace("[.] Envoi du code "+ message);

            var corrId = Guid.NewGuid().ToString();
            var props = channel.CreateBasicProperties();
            props.ReplyTo = replyQueueName;
            props.CorrelationId = corrId;

            var messageBytes = Encoding.UTF8.GetBytes(message);
            // Publication du message sur la file qui répartira à tous les clients
            channel.BasicPublish(exchange: queueName,
                                 routingKey: "",
                                 basicProperties: props,
                                 body: messageBytes);

        }
    }

}
