﻿
using Definitions;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins;
using Utils;
using System.Net.Http;
using System.Net;
using System.IO;

namespace Services
{
    public class CallWSService : DefaultService
    {
        public CallWSService(LocalPluginContext context) : base(context) { }

        internal void CallWS(Entity targetEntity, Entity preIMG)
        {
            //Vérification des attributs obligatoire
            bool nom = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.Nom);

            //Adresse Légale
            bool addl = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.AddrLegaleAddr);
            bool addlcp = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.AddrLegaleCp);
            bool addlville = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.AddrLegalecity);
            bool addlpays = getIsNull<OptionSetValue>(targetEntity, preIMG, AccountDefinition.Columns.AddrLegalePays);
            bool addleg = (addl && addlcp && addlville && addlpays);

            //Adresse de facturation
            bool addf = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.AddrFacturationAdd);
            bool addfcp = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.AddrFacturationCp);
            bool addfville = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.AddrFacturationVille);
            bool addfpays = getIsNull<OptionSetValue>(targetEntity, preIMG, AccountDefinition.Columns.AddrFacturationPays);
            bool addfac = (addf && addfcp && addfville && addfpays);

            OptionSetValue opType = getValue<OptionSetValue>(targetEntity, preIMG, AccountDefinition.Columns.Type);
            ACCOUNT_TYPE type = opType != null ? opType.Value.ToEnum<ACCOUNT_TYPE>() : ACCOUNT_TYPE.Null;
            bool typeverif = (type == ACCOUNT_TYPE.Agence || type == ACCOUNT_TYPE.Annonceur);

            bool langue = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.Langue);
            bool devise = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.Devise);

            //TVA
            bool tvaclient = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.CodeTVAclient);
            bool tvafournisseur = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.CodeTVAfournisseur);

            //Niveau de risque
            bool nivderisque = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.NiveauRisque);

            //Encours
            bool encoursaut = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.EncoursAutoriseCipres);
            bool encourscons = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.EncoursConseille);

            //Mode de règlement
            bool moderegnat = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.ModeReglementNatio);
            bool moderegmet = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.ModeReglementMetro);
            bool moderegint = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.ModeReglementInter);
            bool modereg = (moderegnat || moderegmet || moderegint);

            //Echéance de règlement
            bool echregnat = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.EcheanceReglementNat);
            bool echregmet = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.EcheanceReglementMetro);
            bool echregint = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.EcheanceReglementInter);
            bool echreg = (echregnat || echregmet || echregint);

            //Compte interne
            bool cptintnat = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.ContactInterneNational);
            bool cptintmet = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.ContactInterneMetro);
            bool cptintint = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.ContactInterneInter);
            bool cptint = (cptintnat || cptintmet || cptintint);

            //Regime fiscal
            bool regfiscal = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.RegimeFiscal);

            //Imputation vente
            bool impvente = getIsNull<EntityReference>(targetEntity, preIMG, AccountDefinition.Columns.ImputationVente);

            //TVA intracommunautaire
            bool TVAintra = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.NumTVAIntracomm);

            //No SIRET
            bool siret = getIsNull<string>(targetEntity, preIMG, AccountDefinition.Columns.NumTVAIntracomm);

            //Doublons
            bool bDoublon = getValue<bool>(targetEntity, preIMG, AccountDefinition.Columns.DoublonVerifie);

            bool isDirties = EntityUtils.isDirties(targetEntity, preIMG, typeof(AccountDefinition.Columns));

            if (nom && addleg && addfac && typeverif && langue && devise && tvaclient && tvafournisseur && encoursaut
                && encourscons && modereg && echreg && cptint && regfiscal && impvente && TVAintra && siret && bDoublon
                && isUpdateMessage && isDirties)
            {
                //Get method
                Guid accountguid = targetEntity.Id;
                string codecipres = getValue<string>(targetEntity, preIMG, AccountDefinition.Columns.CodeCipres);
                WebRequest req = WebRequest.Create(@"http://localhost:81/Send/" + accountguid.ToString() + codecipres);

                req.Method = "GET";

                HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
                if (resp.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream respStream = resp.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(respStream, Encoding.UTF8);
                        Console.WriteLine(reader.ReadToEnd());
                    }
                }
                else
                {
                    Context.Trace(string.Format("Status Code: {0}, Status Description: {1}", resp.StatusCode, resp.StatusDescription));
                    throw new Exception("Impossible d'interroger le webservice");
                }
            }
        }
    }
}
