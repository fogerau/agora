﻿
using Definitions;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins;
using Utils;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Diagnostics;
using System.Net.Mail;

namespace Services
{
    public class RabbitMQservice : DefaultService
    {
        public RabbitMQservice(LocalPluginContext context) : base(context) { }

        internal void SendToQueue(Entity targetEntity, Entity preIMG)
        {
            string ErrorMsg = string.Empty;
            string uri = string.Empty;
            var test = string.Empty;
            try
            {
                bool bDoublon = EntityUtils.getValue<bool>(targetEntity, preIMG, AccountDefinition.Columns.DoublonVerifie);
                OptionSetValue opType = EntityUtils.getValue<OptionSetValue>(targetEntity, preIMG, AccountDefinition.Columns.Type);

                ACCOUNT_TYPE type = opType != null ? opType.Value.ToEnum<ACCOUNT_TYPE>() : ACCOUNT_TYPE.Null;
                string nom = EntityUtils.getValue<string>(targetEntity, preIMG, AccountDefinition.Columns.Nom);
                bool isDirties = EntityUtils.isDirties(targetEntity, preIMG, typeof(AccountDefinition.Columns));
                bool isCompleted = EntityUtils.isCompleted(targetEntity, preIMG, typeof(AccountDefinition.Columns));
                test += "point 1\r\n";
                if ((type == ACCOUNT_TYPE.AgenceMedia || type == ACCOUNT_TYPE.AgenceConseil || type == ACCOUNT_TYPE.AgenceRP || type == ACCOUNT_TYPE.Annonceur) && bDoublon && isUpdateMessage && isDirties && isCompleted)
                {
                    test += "point 2\r\n";
                    // Démarrage de la connexion à RabbitMQ
                    ParameterService paramService = new ParameterService(Context);
                    Dictionary<string, string> paramsList = paramService.RetrieveStringParams(CARACTERISTIQUE.RabbitMQ);

                    if (paramsList == null && paramsList.Count == 0)
                    {
                        Context.Trace("Impossible de récupérer la liste des paramètres");
                        throw new Exception("Impossible de récupérer la liste des paramètres");
                    }
                    test += "point 3\r\n";

                    string codeCipres = EntityUtils.getValue<string>(targetEntity, preIMG, AccountDefinition.Columns.CodeCipres);

                    //codeCipres = string.IsNullOrEmpty(codeCipres) ? "-1" : codeCipres;
                    codeCipres = string.IsNullOrEmpty(codeCipres) ? "null" : codeCipres;
                    uri = string.Format(paramsList[ParameterService.PRODUCERRABBITWS], targetEntity.Id, codeCipres);
                    Context.Trace("trace" );
                    test += "point 4\r\n";
                    EventLog.WriteEntry("AgoraRabbitMQService", uri, EventLogEntryType.Information);
                    //HttpClient client = new HttpClient();
                    //HttpResponseMessage wcfResponse = client.GetAsync(uri).Result;
                    //HttpContent stream = wcfResponse.Content;
                    //var data = stream.ReadAsStringAsync();
                    Context.Trace("uri :" + uri);
                    HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(uri);
                    test += "point 5\r\n";
                    HttpWebResponse httpWResp = (HttpWebResponse)httpWReq.GetResponse();
                    test += "point 6\r\n";
                    System.Text.Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    test += "point 7\r\n";
                    StreamReader loResponseStream = new StreamReader(httpWResp.GetResponseStream(), enc);
                    test += "point 8\r\n";
                    string retour = loResponseStream.ReadToEnd();
                    test += "point 9\r\n";
                    Context.Trace(retour);
                    test += "point 105\r\n";

                    //Si le code ciprès est null on met le statut en mode création sinon en mode modification
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AgoraRabbitMQService", uri + Environment.NewLine + ex.Message, EventLogEntryType.Error);
                Write(test);
                throw ex;
            }
            Write(test);
        }

        public static void Write(string msg)
        {

            if (0 == 1)
            {
                SmtpClient smtpClient = new SmtpClient();
                MailMessage message = new MailMessage();
                MailAddress fromAddress = new MailAddress("noreply-agora@cmi.france");
                // setup up the host, increase the timeout to 5 minutes
                smtpClient.Host = "smtp-app.cmimedia.fr";
                smtpClient.UseDefaultCredentials = false;
                //smtpClient.Credentials = new System.Net.NetworkCredential("omnilog-f.ogerau", "Allblacks99", "SIEGE");
                smtpClient.Timeout = (60 * 5 * 1000);
                message.From = fromAddress;
                message.Subject = "Trace";
                message.IsBodyHtml = true;
                message.Body = msg;
                message.To.Add(new MailAddress("OMNILOG-F.OGERAU@Lagardere-Active.com", "Frédéric Ogerau"));
                smtpClient.Send(message);
            }
        }
    }
}
