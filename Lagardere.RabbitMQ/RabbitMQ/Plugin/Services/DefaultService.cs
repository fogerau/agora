﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Definitions;
using Plugins;

namespace Services
{
    public class DefaultService
    {
        private readonly LocalPluginContext _context;

        public DefaultService(IContext context)
        {
            _context = context as LocalPluginContext;
           
        }

        protected IContext Context
        {
            get { return _context; }
        }

        protected IOrganizationService OrganizationService
        {
            get { return _context.OrganizationService; }
        }

        protected bool isCreateMessage
        {
            get { return _context.PluginExecutionContext.MessageName == "Create"; }
        }

        protected bool isUpdateMessage
        {
            get { return _context.PluginExecutionContext.MessageName == "Update"; }
        }
    }
}
