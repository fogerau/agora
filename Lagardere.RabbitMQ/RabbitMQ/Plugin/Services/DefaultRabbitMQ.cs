﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Services
{
    public class DefaultRabbitMQ
    {
        public IConnection connection;
        public IModel channel;
        public string queueName, replyQueueName;

        public IContext Context;

         /// <summary>
        /// Initialisation de la connexion avec RabbitMQ
        /// </summary>
        public DefaultRabbitMQ(string rabbitMQHost, string rabbitMQUser, string rabbitMQPwd, IContext pcontext)
        {
            // Paramètres de connexion au serveur RabbitMQ
            string hostname = rabbitMQHost;
            string user = rabbitMQUser;
            string pwd = rabbitMQPwd;
            
            Context = pcontext;
            Context.Trace("Création de la connection Factory");
            var factory = new ConnectionFactory();
            if (!string.IsNullOrEmpty(user))
                factory.UserName = user;
            if (!string.IsNullOrEmpty(pwd))
                factory.Password = pwd;
            factory.VirtualHost = "/";
            factory.Protocol = Protocols.DefaultProtocol;
            factory.HostName = hostname;
            factory.Port = AmqpTcpEndpoint.UseDefaultPort;
            Context.Trace("Création de la connexion");
            // Création de la connexion
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            
        }


    }
}
