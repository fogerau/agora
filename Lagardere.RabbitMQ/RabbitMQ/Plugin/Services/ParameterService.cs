﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk;
using Definitions;
using Utils;


namespace Services
{
    public class ParameterService : DefaultService
    {
        public const string PRODUCERRABBITWS = "ProducerRabbitWS";
        
        public const string SERVERSMTP = "ServerSMTP";
        public const string RABBITADMINEMAIL = "RabbitAdminEmail";
        public const string CRMADMINEMAIL = "CrmAdminEmail";

        public ParameterService(IContext context) : base(context) { }

        public Dictionary<string, string> RetrieveStringParams(CARACTERISTIQUE siname)
        {
            Dictionary<string, string> ListParams = new Dictionary<string, string>();

            QueryExpression qe = new QueryExpression(ParameterDefinition.EntityName);
            qe.ColumnSet = new ColumnSet(ParameterDefinition.Columns.Nom, ParameterDefinition.Columns.ValeurTexte);
            qe.Criteria.AddCondition(ParameterDefinition.Columns.Caracteristique, ConditionOperator.Equal, siname.ToInt());
            Context.Trace("Récupération des paramètres");
            EntityCollection ec = OrganizationService.RetrieveMultiple(qe);

            if (ec != null && ec.Entities != null & ec.Entities.Count > 0)
            {
                Context.Trace(string.Format("{0} Paramètres récupérés", ec.Entities.Count));

                foreach (Entity param in ec.Entities)
                {
                    string key = param.GetAttributeValue<string>(ParameterDefinition.Columns.Nom);
                    string val = param.GetAttributeValue<string>(ParameterDefinition.Columns.ValeurTexte);
                    if (!string.IsNullOrEmpty(key))
                        ListParams.Add(key, val);
                }
            }

            return ListParams;
        }
    }
}
