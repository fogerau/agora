﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Lagardere.CRM.Plugin.Budget
{
    public class Budget : IPlugin
    {
        /// <summary>
        /// Reporte les modifications sur account
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            tracingService.Trace("Start");

            try
            {
                tracingService.Trace("init");
                Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
                            serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                
                try
                {

                    Entity entity = null;
                    if (context.PostEntityImages.Contains("Image"))
                    {
                        entity = (Entity)context.PostEntityImages["Image"];
                    }
                    else
                    {
                        throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin Budget, Image absent.");
                    }

                    if ("CREATE".Equals(context.MessageName.ToUpper()) || "UPDATE".Equals(context.MessageName.ToUpper()))
                    {
                        string name = entity.GetAttributeValue<string>("bd_budget").Trim();
                        entity.Attributes["bd_budget"] = name;
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidPluginExecutionException("Une erreur s'est produite lors du report des données Budget vers account. ", ex);
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin Budget.", ex);
            }
            catch (Exception ex)
            {
                tracingService.Trace("perimetreADVDeSociété : {0}", ex.ToString());
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin Budget.", ex); ;
            }
        }
    }
}
