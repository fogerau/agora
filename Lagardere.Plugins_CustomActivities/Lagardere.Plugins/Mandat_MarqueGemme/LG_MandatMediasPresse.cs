﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Lagardere.CRM.Plugin.Mandat_MarqueGemme
{
    public class LG_MandatMediasPresse : IPlugin
    {
        /// <summary>
        /// Si Medias presse = OUI alors il faut au moins une marqueGemme associé au mandat
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {
                // Obtain the execution context from the service provider.
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);

                Entity MandatSource = null;
                Boolean MediaPress = false;

                 #region Test Association entité

                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity &&
                    ((Entity)context.InputParameters["Target"]).LogicalName == "bd_mandat")
                {
                    MandatSource = (Entity)context.InputParameters["Target"];
                    MediaPress = (Boolean)MandatSource.Attributes["bd_presse"];
                    if (MediaPress == true)
                    {
                        if (getMarqueGammeMandatFromIdMandat(service, MandatSource.Id).Entities.Count == 0)
                        {
                            throw new InvalidPluginExecutionException(OperationStatus.Retry, "Si Medias presse = OUI alors il faut au moins une marque Gemme");
                        }
                    }
                }
                 #endregion
            }
            catch (Exception ex)
            {
                tracingService.Trace("LG_MandatMediasPresse: {0}", ex.ToString());
                throw;
            }
        }

        private EntityCollection getMarqueGammeMandatFromIdMandat(IOrganizationService service, Guid MandatId)
        {
            try
            {

                QueryExpression query = new QueryExpression
                {
                    EntityName = "bd_marquesgemmes_mandat",
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_mandatid",
                                Operator = ConditionOperator.Equal,
                                Values = {MandatId}
                            }
                        }
                    }
                };

                EntityCollection oListMarquesGemmesMandat = service.RetrieveMultiple(query);

                return oListMarquesGemmesMandat;

            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getMarqueGammeMandatFromIdMarquesGemmes : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getMarqueGammeMandatFromIdMarquesGemmes : " + e.Message);
            }
        }
    }
}
