﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Lagardere.CRM.Plugin.Mandat_MarqueGemme
{
    public class LG_Mandat_MarqueGemmeAsso : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {
                // Obtain the execution context from the service provider.
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
                EntityReference entityMarquesGemmes = null;
                EntityReference entityMandat = null;
                EntityCollection MarquesGemmes = null;

                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);

                #region Test Association entité

                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is EntityReference &&
                    context.InputParameters.Contains("RelatedEntities") &&
                    ((EntityReferenceCollection)context.InputParameters["RelatedEntities"]).Count > 0  && ((EntityReferenceCollection)context.InputParameters["RelatedEntities"])[0] is EntityReference)
                {
                    if (((EntityReference)context.InputParameters["Target"]).LogicalName == "bd_marquesgemmes")
                    { entityMarquesGemmes = (EntityReference)context.InputParameters["Target"]; }
                    else { return; }
                    if (((EntityReference)((EntityReferenceCollection)context.InputParameters["RelatedEntities"])[0]).LogicalName == "bd_mandat")
                    { entityMandat = ((EntityReference)((EntityReferenceCollection)context.InputParameters["RelatedEntities"])[0]); }
                    else { return; }

                    #region Si la source n'est pas egale a mandat ou si la cible n'est pas margesgemmes alors on quitte tous
                    if (entityMandat == null || entityMarquesGemmes == null) { return; }
                    #endregion

                    #region Ok Association
                    /*SI ON ARRIVE ICI ALORS ON EST BIEN SUR UN ASSOCIATION DE BD_MANDAT ET BD_MARQUESGEMMES*/
                    #region récupére la marque gemme
                    MarquesGemmes = getMarqueGammeMandatFromIdMarquesGemmes(service, entityMarquesGemmes.Id);
                    if (MarquesGemmes.Entities.Count == 0) { return; } // Si il n'y a pas de relation il n'y a pas 2 mandats actif

                    #region Plusieur Mandat donc vérification qu'il y est au maximum 1 seul mandat actif
                    Entity myMandat = getMandatFromId(service, ((Guid)entityMandat.Id));
                    foreach (Entity UneRelation in MarquesGemmes.Entities)
                    {
                        Entity tmp = getMandatFromId(service, ((Guid)UneRelation.Attributes["bd_mandatid"]));
                        if (IsMandatActif(tmp) && IsMandatActif(myMandat))
                        {
                            if (tmp.Id != entityMandat.Id)
                            {
                                throw new Exception("La marque gemme a déjà un mandat actif");
                            }
                        }

                    }
                    #endregion


                    #endregion
                    #endregion

                }
                else
                    return;
                #endregion
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException("An error occurred in the LG_Budget_MarqueGemmeAsso plug-in.", ex);
            }

            catch (Exception ex)
            {
                tracingService.Trace("FollowupPlugin | LG_Budget_MarqueGemmeAsso: {0}", ex.ToString());
                throw;
            }
        }

        private bool IsMandatActif(Entity Mandat)
        {
            bool b_datefin = false;
            bool b_IsValide = false;
            bool b_IsActive = false;
            if (Mandat.LogicalName == "bd_mandat")
            {
                if (Mandat.Attributes.Contains("bd_datefin"))
                {
                    DateTime DateFin = (DateTime)Mandat.Attributes["bd_datefin"];
                    if (DateFin >= DateTime.Now) { b_datefin = true; }
                }
                else { b_datefin = true; }

                if (Mandat.Attributes.Contains("bd_validite"))
                {
                    OptionSetValue IsValide = (OptionSetValue)Mandat.Attributes["bd_validite"];
                    if(IsValide.Value == 899240001) //899240001 = Validé ADV
                    { b_IsValide = true; }
                }

                if (Mandat.Attributes.Contains("statecode"))
                {
                    OptionSetValue IsActive = (OptionSetValue)Mandat.Attributes["statecode"];
                    if(IsActive.Value == 0) // 0 statusCode = ACTIVE
                    { b_IsActive = true; }
                }
            }
            return b_datefin && b_IsValide && b_IsActive;
        }

        private Entity getMandatFromId(IOrganizationService service, Guid MandatId)
        {
            try
            {

                QueryExpression query = new QueryExpression
                {
                    EntityName = "bd_mandat",
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_mandatid",
                                Operator = ConditionOperator.Equal,
                                Values = {MandatId}
                            }
                        }
                    }
                };

                EntityCollection oListMandat = service.RetrieveMultiple(query);

                if (oListMandat.Entities.Count > 0)
                {
                    return oListMandat.Entities[0];
                }
                else
                {
                    return null;
                }

            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getMarqueGammeMandatFromIdMarquesGemmes : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getMarqueGammeMandatFromIdMarquesGemmes : " + e.Message);
            }
        }

        private EntityCollection getMarqueGammeMandatFromIdMarquesGemmes(IOrganizationService service, Guid MarqueGemmeId)
        {
            try
            {

                QueryExpression query = new QueryExpression
                {
                    EntityName = "bd_marquesgemmes_mandat",
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_marquesgemmesid",
                                Operator = ConditionOperator.Equal,
                                Values = {MarqueGemmeId}
                            }
                        }
                    }
                };

                EntityCollection oListMarquesGemmes = service.RetrieveMultiple(query);

                return oListMarquesGemmes;

            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getMarqueGammeMandatFromIdMarquesGemmes : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getMarqueGammeMandatFromIdMarquesGemmes : " + e.Message);
            }
        }
    }
}
