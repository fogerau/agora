﻿using System;
using System.ServiceModel;

// Microsoft Dynamics CRM namespace(s)
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Lagardere.CRM.Plugin;

namespace Lagardere.Plugin
{
    #region CompteRenduPostUpdate
    public class CompteRenduPostUpdate : IPlugin
    {
        /// <summary>
        /// Partage de chaque Marque à chaque Contact participant.
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            Log.Write("Début CompteRenduPostUpdate");

            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {
                // Obtain the execution context from the service provider.
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Log.Write("CompteRenduPostUpdate Depth : " + context.Depth);
                if (context.Depth < 2)
                {
                    Entity TargetRdv = null;

                    if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                    {
                        TargetRdv = (Entity)context.InputParameters["Target"];
                        if (TargetRdv.LogicalName != "appointment") { return; }
                    }
                    else
                        return;

                    // Mettre à jour les informations du compte-rendu
                    // Date de rendez-vous
                    EntityCollection ecRdv = new EntityCollection();

                    if (TargetRdv.Contains("scheduledstart") && TargetRdv.Attributes["scheduledstart"] != null)
                    {
                        ecRdv = GetAllCr(service, TargetRdv.Id);

                        foreach (Entity eCr in ecRdv.Entities)
                        {
                            eCr.Attributes["bd_daterdv"] = TargetRdv.Attributes["scheduledstart"];
                            service.Update(eCr);
                        }

                    }
                }
 
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                Log.Write("CompteRenduPostUpdate :\r\n " + ex.Message);

                throw new InvalidPluginExecutionException("Erreur lors de la mise à jour du rendez-vous. Veuilllez contacter votre administrateur. (CompteRenduPostUpdate)", ex);
            }
            //</snippetFollowupPlugin3>

            catch (Exception ex)
            {
                tracingService.Trace("FollowupPlugin: {0}", ex.ToString());
                throw;
            }
        }

        private EntityCollection GetAllCr(IOrganizationService service, Guid RdvId)
        {
            EntityCollection ecCr = new EntityCollection();
            QueryExpression Query = new QueryExpression("bd_compterendu");
            Query.ColumnSet = new ColumnSet("bd_compterenduid");
            Query.Criteria.AddCondition(new ConditionExpression("bd_rendezvous", ConditionOperator.Equal, RdvId));
            ecCr = service.RetrieveMultiple(Query);

            return ecCr;
        }



    #endregion

    }
}
