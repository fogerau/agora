﻿using System;
using System.ServiceModel;

// Microsoft Dynamics CRM namespace(s)
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Lagardere.CRM.Plugin;

namespace Lagardere.Plugin
{
    #region CompteRenduPostUpdate
    public class LG_CompteRendu_UpdateMarques : IPlugin
    {
        /// <summary>
        /// Permet d'associer les marques à un compte-rendu
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            Log.Write("Début LG_CompteRendu_UpdateMarques");
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {
                // Obtain the execution context from the service provider.
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Entity entity = null;

                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    entity = (Entity)context.InputParameters["Target"];
                    if (entity.LogicalName != "bd_compterendu")
                    { return; }
                    else
                    {
                        //On récupére tous les guids des marques dans le champs cachés
                        if (context.PostEntityImages.ContainsKey("PostImage"))
                        {
                            String sConcat_marques = String.Empty;
                            String Guid = String.Empty;
                            String[] ListeGuid = new String[] { };

                            EntityReferenceCollection oCollRefMarqueDisassociate = new EntityReferenceCollection();
                            EntityReferenceCollection oCollRefMarqueAssociate = new EntityReferenceCollection();
                            EntityReferenceCollection v_refSecteurdactiviteDisassociate = new EntityReferenceCollection();
                            EntityReferenceCollection v_refSecteurdactiviteAssociate = new EntityReferenceCollection();

                            EntityReference oRefSecteur = new EntityReference();

                            Entity EntityCR = (Entity)context.PostEntityImages["PostImage"];

                            //On Dissociate toutes les marques
                            oCollRefMarqueDisassociate = getMarquesFromCR(service, EntityCR.Id);
                            service.Disassociate("bd_compterendu", EntityCR.Id, new Relationship("bd_bd_compterendu_bd_marque"), oCollRefMarqueDisassociate);

                            //On récupère la liste des marques à associer
                            if (EntityCR.Contains("bd_listeguidmarques"))
                            {
                                Guid = EntityCR.Attributes["bd_listeguidmarques"].ToString();
                                ListeGuid = Guid.Split(';');
                            }

                            //On enregistre toutes les marques dans une référence de collection
                            foreach (String item in ListeGuid)
                            {
                                if (!String.IsNullOrEmpty(item))
                                {
                                    try
                                    {
                                        Guid MarqueId = new Guid(item);
                                        EntityReference oRefMarque = new EntityReference("bd_marque", MarqueId);

                                        //Pour chaque marque on récupère le secteur d'activité
                                        oRefSecteur = getSecteurMarque(service, oRefMarque.Id);
                                        if (!oCollRefMarqueAssociate.Contains(oRefMarque)) //Pas 2 fois la même marque
                                        {
                                            oCollRefMarqueAssociate.Add(oRefMarque);
                                        }

                                        if (oRefSecteur != null && !v_refSecteurdactiviteAssociate.Contains(oRefSecteur)) // A condition que le secteur n'xiste pas déjà
                                            v_refSecteurdactiviteAssociate.Add(oRefSecteur);
                                    }
                                    catch (Exception ex)
                                    {
                                        // plante si la marque n'existe plus. Ne rien faire
                                    }
                                }
                            }

                            //On associate toutes les marques
                            if (oCollRefMarqueAssociate.Count > 0)
                                service.Associate("bd_compterendu", EntityCR.Id, new Relationship("bd_bd_compterendu_bd_marque"), oCollRefMarqueAssociate);

                            //On dissociate tous les secteurs
                            v_refSecteurdactiviteDisassociate = getSecteurCR(service, EntityCR.Id, "bd_compterendu_secteurdactivite", "bd_secteurdactivite");
                            if (v_refSecteurdactiviteDisassociate.Count > 0)
                                service.Disassociate("bd_compterendu", EntityCR.Id, new Relationship("bd_compterendu_secteurdactivite"), v_refSecteurdactiviteDisassociate);

                            //On associe les secteurs au CR
                            if (v_refSecteurdactiviteAssociate.Count > 0)
                                service.Associate("bd_compterendu", EntityCR.Id, new Relationship("bd_compterendu_secteurdactivite"), v_refSecteurdactiviteAssociate);

                            // On associe toutes les marques aux contacts présents dans le compte-rendu
                            // On retire toutes les marques d'un contact
                            // On récupère toutes les marques des comptes rendus du contact
                            EntityCollection oCollContact = getContactsFromCR(service, EntityCR.Id);
                            foreach (Entity oContact in oCollContact.Entities)
                            {
                                EntityReferenceCollection oRefCollMarques = getMarquesOfContact(service, oContact.Id);
                                service.Disassociate("contact", oContact.Id, new Relationship("bd_contact_marque"), oRefCollMarques);

                                EntityReferenceCollection oRefCollMarquesCR = getMarquesCROfContact(service, oContact.Id);
                                service.Associate("contact", oContact.Id, new Relationship("bd_contact_marque"), oRefCollMarquesCR);
                            }
                        }
                    };
                }
                else
                    return;
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                Log.Write("LG_CompteRendu_UpdateMarques :\r\n " + ex.Message);
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin LG_CompteRendu_UpdateMarques.", ex);
            }
            //</snippetFollowupPlugin3>

            catch (Exception ex)
            {
                Log.Write("LG_CompteRendu_UpdateMarques :\r\n " + ex.Message);
                tracingService.Trace("FollowupPlugin: {0}", ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Permet de retrouver la liste des contacts d'un compte-rendu
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="v_InUserId"></param>
        /// <returns></returns>
        private EntityCollection getContactsFromCR(IOrganizationService p_service, Guid v_CompteRenduId)
        {
            try
            {
                QueryExpression query = new QueryExpression("contact");
                query.ColumnSet = new ColumnSet(true);
                LinkEntity linkEntity1 = new LinkEntity("contact", "bd_contact_bd_compterendu", "contactid", "contactid", JoinOperator.Inner);
                LinkEntity linkEntity2 = new LinkEntity("bd_contact_bd_compterendu", "bd_compterendu", "bd_compterenduid", "bd_compterenduid", JoinOperator.Inner);
                linkEntity1.LinkEntities.Add(linkEntity2);
                query.LinkEntities.Add(linkEntity1);
                linkEntity2.LinkCriteria = new FilterExpression();
                linkEntity2.LinkCriteria.AddCondition(new ConditionExpression("bd_compterenduid", ConditionOperator.Equal, v_CompteRenduId));
                EntityCollection collRecords = p_service.RetrieveMultiple(query);

                return collRecords;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getContactsFromCR : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getContactsFromCR : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de savoir si une association Contact-Marque existe déjà
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="p_MarqueId"></param>
        /// <param name="p_ContactId"></param>
        /// <returns></returns>
        private EntityReferenceCollection getMarquesOfContact(IOrganizationService p_service, Guid p_ContactId)
        {
            try
            {
                QueryExpression query = new QueryExpression("bd_contact_marque");
                query.ColumnSet = new ColumnSet("bd_marqueid");
                query.Criteria.AddCondition(new ConditionExpression("contactid", ConditionOperator.Equal, p_ContactId));
                EntityCollection oCollRecords = p_service.RetrieveMultiple(query);

                EntityReferenceCollection oRefCollMarques = new EntityReferenceCollection();
                foreach (Entity oRecord in oCollRecords.Entities)
                {
                    oRefCollMarques.Add(new EntityReference("bd_marque", (Guid)oRecord.Attributes["bd_marqueid"]));
                }

                return oRefCollMarques;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getContactMarque : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getContactMarque : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de retrouver la liste des contacts d'un compte-rendu
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="v_InUserId"></param>
        /// <returns></returns>
        private EntityReferenceCollection getMarquesFromCR(IOrganizationService p_service, Guid v_CompteRenduId)
        {
            EntityReferenceCollection oCollRefMarque = new EntityReferenceCollection();

            try
            {
                QueryExpression query = new QueryExpression("bd_marque");
                query.ColumnSet = new ColumnSet(true);
                LinkEntity linkEntity1 = new LinkEntity("bd_marque", "bd_bd_compterendu_bd_marque", "bd_marqueid", "bd_marqueid", JoinOperator.Inner);
                LinkEntity linkEntity2 = new LinkEntity("bd_bd_compterendu_bd_marque", "bd_compterendu", "bd_compterenduid", "bd_compterenduid", JoinOperator.Inner);
                linkEntity1.LinkEntities.Add(linkEntity2);
                query.LinkEntities.Add(linkEntity1);
                linkEntity2.LinkCriteria = new FilterExpression();
                linkEntity2.LinkCriteria.AddCondition(new ConditionExpression("bd_compterenduid", ConditionOperator.Equal, v_CompteRenduId));
                EntityCollection collRecords = p_service.RetrieveMultiple(query);

                foreach (Entity oMarque in collRecords.Entities)
                {
                    oCollRefMarque.Add(oMarque.ToEntityReference());
                }

                return oCollRefMarque;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getMarquesFromCR : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getMarquesFromCR : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de récupérer les secteurs associés à un compte rendu
        /// </summary>
        /// <param name="service">Service CRM</param>
        /// <param name="idRDV">Id du RDV</param>
        /// <returns>Entité compte-rendu (Id du CR + RDV)</returns>
        private EntityReferenceCollection getSecteurCR(IOrganizationService service, Guid idCR, string p_relationName, string logicalName)
        {
            try
            {
                EntityReferenceCollection oRefContactCR = new EntityReferenceCollection();

                QueryExpression query = new QueryExpression
                {
                    EntityName = p_relationName,
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_compterenduid",
                                Operator = ConditionOperator.Equal,
                                Values = {idCR}
                            }
                        }
                    }
                };

                DataCollection<Entity> oListContactCR = service.RetrieveMultiple(query).Entities;

                foreach (Entity item in oListContactCR.ToArray())
                {
                    oRefContactCR.Add(new EntityReference(logicalName, (Guid)item.Attributes[logicalName + "id"]));
                }

                return oRefContactCR;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getSecteurCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getSecteurCR : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de savoir si une association Contact-Marque existe déjà
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="p_MarqueId"></param>
        /// <param name="p_ContactId"></param>
        /// <returns></returns>
        private EntityReference getSecteurMarque(IOrganizationService p_service, Guid p_MarqueId)
        {
            try
            {
                EntityReference oRef = new EntityReference();
                QueryExpression query = new QueryExpression("bd_marque");
                query.ColumnSet = new ColumnSet("bd_secteuractivite");
                query.Criteria.AddCondition(new ConditionExpression("bd_marqueid", ConditionOperator.Equal, p_MarqueId));
                EntityCollection collRecords = p_service.RetrieveMultiple(query);
                if (collRecords.Entities.Count > 0)
                {
                    oRef = (EntityReference)collRecords.Entities[0].Attributes["bd_secteuractivite"];
                }

                return oRef;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getSecteurMarque : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getSecteurMarque : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
        }


        /// <summary>
        /// Permet de savoir si une association Contact-Marque dans compte
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="p_MarqueId"></param>
        /// <param name="p_ContactId"></param>
        /// <returns></returns>
        private EntityReferenceCollection getMarquesCROfContact(IOrganizationService p_service, Guid p_ContactId)
        {
            try
            {
                QueryExpression query = new QueryExpression("bd_bd_compterendu_bd_marque");
                query.ColumnSet = new ColumnSet("bd_marqueid");
                query.Distinct = true;
                LinkEntity linkEntity1 = new LinkEntity("bd_bd_compterendu_bd_marque", "bd_contact_bd_compterendu", "bd_compterenduid", "bd_compterenduid", JoinOperator.Inner);
                linkEntity1.LinkCriteria.AddCondition(new ConditionExpression("contactid", ConditionOperator.Equal, p_ContactId));
                query.LinkEntities.Add(linkEntity1);
                EntityCollection oCollRecords = p_service.RetrieveMultiple(query);

                EntityReferenceCollection oRefCollMarques = new EntityReferenceCollection();
                foreach (Entity oRecord in oCollRecords.Entities)
                {
                    oRefCollMarques.Add(new EntityReference("bd_marque", (Guid)oRecord.Attributes["bd_marqueid"]));
                }

                return oRefCollMarques;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getMarquesCROfContact : Une erreur est survenue dans le plugin LG_CompteRendu_UpdateMarques getMarqueFromCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getMarquesCROfContact : Une erreur est survenue dans le plugin LG_CompteRendu_UpdateMarques getMarqueFromCR : " + e.Message);
            }
        }


    #endregion

    }
}
