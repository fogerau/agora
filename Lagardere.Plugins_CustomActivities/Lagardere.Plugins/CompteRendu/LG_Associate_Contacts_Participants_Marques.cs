﻿using Lagardere.CRM.Plugin;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace LG_Associate_Contacts_Participants_Marques
{
    public class LG_Associate_Contacts_Participants_Marques : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            Log.Write("Début LG_Associate_Contacts_Participants_Marques");

            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            string errorText="debut";
            try
            {
                // Obtain the execution context from the service provider.
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);

                EntityReference targetEntity = null;
                EntityReference relatedEntity = null;
                EntityReferenceCollection relatedEntities = null;
                String relationshipName = string.Empty;
                if (context.Depth <= 2)
                {
                    errorText = "0";
                    #region Récupération des entités
                    // On récupère le  nom de la relation pour vérification ultérieure
                    if (context.InputParameters.Contains("Relationship"))
                    {
                        relationshipName = context.InputParameters["Relationship"].ToString();
                    }

                    // récupération de l'entité cible
                    if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is EntityReference)
                    {
                        targetEntity = (EntityReference)context.InputParameters["Target"]; //Compte-rendu
                    }

                    // Récupération de l'entité liée
                    if (context.InputParameters.Contains("RelatedEntities") && context.InputParameters["RelatedEntities"] is EntityReferenceCollection)
                    {
                        relatedEntities = context.InputParameters["RelatedEntities"] as EntityReferenceCollection;
                        if (relatedEntities.Count == 0)
                            return;

                        relatedEntity = relatedEntities[0]; // Contact, SystemUser, Marque

                    }
                    #endregion

                    #region Associate
                    if (context.MessageName == "Associate")
                    {
                        errorText = "Contact - Compte-rendu";

                        #region Contact - Compte-rendu
                        // On vérifie que l'on est bien dans l'association Compte-rendu - Contact
                        if (relationshipName == "bd_contact_bd_compterendu.")
                        {
                            String sConcat_contacts = String.Empty;

                            Guid v_compteRendu = (targetEntity.LogicalName == "bd_compterendu") ? targetEntity.Id : relatedEntity.Id;
                            Guid v_contact = (targetEntity.LogicalName == "contact") ? targetEntity.Id : relatedEntity.Id;

                            EntityCollection oMarques = getMarquesFromCR(service, v_compteRendu);
                            EntityReferenceCollection oRefCollMarques = new EntityReferenceCollection();
                            foreach (Entity item in oMarques.Entities)
                            {
                                if (getContactMarque(service, item.Id, v_contact).Entities.Count == 0)
                                    oRefCollMarques.Add(new EntityReference("bd_marque", item.Id));
                           }

                            service.Associate("contact", v_contact, new Relationship("bd_contact_marque"), oRefCollMarques);

                            EntityCollection oContacts = getContactsFromCR(service, v_compteRendu);
                            foreach (Entity oContact in oContacts.Entities)
                            {
                                sConcat_contacts += oContact.Attributes["fullname"].ToString() + ";";
                            }

                            Entity CompteRendu = service.Retrieve("bd_compterendu", v_compteRendu, new ColumnSet("bd_concatenation_contacts"));
                            CompteRendu.Attributes["bd_concatenation_contacts"] = sConcat_contacts;
                            service.Update(CompteRendu);
                        }
                        #endregion
                        errorText = "Compte-rendu Systemuser";

                        #region Compte-rendu Systemuser
                        // On vérifie que l'on est bien dans l'association Compte-rendu - Systemuser
                        if (relationshipName == "bd_compterendu_systemuser.")
                        {

                            String sConcat_systemusers = String.Empty;
                            EntityCollection oSystemUsers = getUsersFromCR(service, targetEntity.Id);
                            Entity CompteRendu = service.Retrieve(targetEntity.LogicalName, targetEntity.Id, new ColumnSet("bd_concatenation_userstitres", "bd_daterdv"));

                            foreach (Entity oSystemUser in oSystemUsers.Entities)
                            {
                                //TODO: On ne doit prendre que les TS à la date du compte-rendu
                                EntityCollection oTitreSupports = GetTitresSupports(service, oSystemUser.Id, ((DateTime)CompteRendu.Attributes["bd_daterdv"]));
                                if (oTitreSupports.Entities.Count > 0)
                                {
                                    foreach (Entity oTitreSupport in oTitreSupports.Entities)
                                    {
                                        Entity TitreSupport = service.Retrieve(oTitreSupport.LogicalName, oTitreSupport.Id, new ColumnSet("bd_titresupport"));
                                        sConcat_systemusers += oSystemUser.Attributes["fullname"].ToString() + "-" + ((EntityReference)(TitreSupport.Attributes["bd_titresupport"])).Name + ";";
                                    }
                                }
                                else
                                {
                                    sConcat_systemusers += oSystemUser.Attributes["fullname"].ToString() + ";";
                                }
                            }

                            CompteRendu.Attributes["bd_concatenation_userstitres"] = sConcat_systemusers;
                            service.Update(CompteRendu);
                        }
                        #endregion
                    }
                    #endregion
                    errorText = "autre";


                    if (context.MessageName == "Associate" || context.MessageName == "Disassociate")
                    {
                        #region Mandat - Marque Gemme
                        // On vérifie que l'on est bien dans l'association Mandat - Marque Gemme
                        if (relationshipName == "bd_marquesgemmes_mandat.")
                        {
                            Entity Mandat = new Entity();
                            if (targetEntity.LogicalName == "bd_mandat")
                            {
                                Mandat = service.Retrieve(targetEntity.LogicalName, targetEntity.Id, new ColumnSet("bd_annonceurafacturer"));

                                if (Mandat.Id != Guid.Empty)
                                {
                                    Entity Societe = new Entity("account");
                                    Societe.Id = ((EntityReference)Mandat.Attributes["bd_annonceurafacturer"]).Id;
                                    Societe.Attributes.Add("bd_datesynchromandat", DateTime.Now);
                                    service.Update(Societe);
                                }
                            }

                            Entity MarqueGemme = new Entity();
                            EntityReference tmp = new EntityReference();

                            if ((targetEntity.LogicalName == "bd_marquesgemmes") || (relatedEntity.LogicalName == "bd_marquesgemmes"))
                            {

                                if ((targetEntity.LogicalName == "bd_marquesgemmes"))
                                    tmp = targetEntity;

                                if (relatedEntity.LogicalName == "bd_marquesgemmes")
                                    tmp = relatedEntity;

                                MarqueGemme = service.Retrieve(tmp.LogicalName, tmp.Id, new ColumnSet("bd_annonceur"));
                                if (MarqueGemme.Id != Guid.Empty)
                                {
                                    Entity Societe = new Entity("account");
                                    Societe.Id = ((EntityReference)MarqueGemme.Attributes["bd_annonceur"]).Id;
                                    Societe.Attributes.Add("bd_datesynchromarquegemme", DateTime.Now);
                                    service.Update(Societe);
                                }
                            }


                            if ((targetEntity.LogicalName == "bd_mandat") || (relatedEntity.LogicalName == "bd_mandat"))
                            {

                                if ((targetEntity.LogicalName == "bd_mandat"))
                                    tmp = targetEntity;

                                if (relatedEntity.LogicalName == "bd_mandat")
                                    tmp = relatedEntity;

                                Mandat = service.Retrieve(tmp.LogicalName, tmp.Id, new ColumnSet("bd_annonceurafacturer"));

                                if (Mandat.Id != Guid.Empty)
                                {
                                    Entity Societe = new Entity("account");
                                    Societe.Id = ((EntityReference)Mandat.Attributes["bd_annonceurafacturer"]).Id;
                                    Societe.Attributes.Add("bd_datesynchromandat", DateTime.Now);
                                    service.Update(Societe);
                                }
                            }
                        }
                        #endregion
                        errorText = "Compte-rendu Marque";

                        #region Compte-rendu Marque
                        /*//Pour info : Le champ concaténation est alimenté via la Web Ressource
                    // On vérifie que l'on est bien dans l'association Compte-rendu - Marque
                    if (relationshipName == "bd_bd_compterendu_bd_marque.")
                    {
                        EntityCollection oContacts = getContactsFromCR(service, targetEntity.Id);

                        EntityReferenceCollection oCollMarque = new EntityReferenceCollection();
                        oCollMarque.Add(new EntityReference("bd_marque", relatedEntity.Id));

                        foreach (Entity oContact in oContacts.Entities)
                        {
                            if (context.MessageName == "Associate")
                            {
                                if (getContactMarque(service,relatedEntity.Id,oContact.Id).Entities.Count == 0)
                                service.Associate("contact", oContact.Id, new Relationship("bd_contact_marque"), oCollMarque);
                            }
                            else
                            {
                                service.Disassociate("contact", oContact.Id, new Relationship("bd_contact_marque"), oCollMarque);
                            }
                        }
                    }*/
                        #endregion
                        errorText = "Fin";

                    }
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                Log.Write("Erreur : " + errorText);
                Log.Write("LG_Associate_Contacts_Participants_Marques :\r\n " + ex.Message);
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin LG_Associate_Contacts_Participants_Marques.", ex);
            }
            catch (Exception ex)
            {
                Log.Write("Erreur : " + errorText);
                Log.Write("LG_Associate_Contacts_Participants_Marques :\r\n " + ex.Message);

                tracingService.Trace("FollowupPlugin | LG_Associate_Contacts_Participants_Marques {0}", ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Permet de retrouver la liste des contacts d'un compte-rendu
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="v_InUserId"></param>
        /// <returns></returns>
        private EntityCollection getContactsFromCR(IOrganizationService p_service, Guid v_CompteRenduId)
        {
            try
            {
                QueryExpression query = new QueryExpression("contact");
                query.ColumnSet = new ColumnSet(true);
                LinkEntity linkEntity1 = new LinkEntity("contact", "bd_contact_bd_compterendu", "contactid", "contactid", JoinOperator.Inner);
                LinkEntity linkEntity2 = new LinkEntity("bd_contact_bd_compterendu", "bd_compterendu", "bd_compterenduid", "bd_compterenduid", JoinOperator.Inner);
                linkEntity1.LinkEntities.Add(linkEntity2);
                query.LinkEntities.Add(linkEntity1);
                linkEntity2.LinkCriteria = new FilterExpression();
                linkEntity2.LinkCriteria.AddCondition(new ConditionExpression("bd_compterenduid", ConditionOperator.Equal, v_CompteRenduId));
                EntityCollection collRecords = p_service.RetrieveMultiple(query);

                return collRecords;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getContactsFormCR : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getContactFormCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getContactsFormCR : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getContactFormCR : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de retrouver la liste des rédacteurs (user) d'un compte-rendu
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="v_InUserId"></param>
        /// <returns></returns>
        private EntityCollection getUsersFromCR(IOrganizationService p_service, Guid v_CompteRenduId)
        {
            try
            {
                QueryExpression query = new QueryExpression("systemuser");
                query.ColumnSet = new ColumnSet(true);
                LinkEntity linkEntity1 = new LinkEntity("systemuser", "bd_compterendu_systemuser", "systemuserid", "systemuserid", JoinOperator.Inner);
                LinkEntity linkEntity2 = new LinkEntity("bd_compterendu_systemuser", "bd_compterendu", "bd_compterenduid", "bd_compterenduid", JoinOperator.Inner);
                linkEntity1.LinkEntities.Add(linkEntity2);
                query.LinkEntities.Add(linkEntity1);
                linkEntity2.LinkCriteria = new FilterExpression();
                linkEntity2.LinkCriteria.AddCondition(new ConditionExpression("bd_compterenduid", ConditionOperator.Equal, v_CompteRenduId));
                EntityCollection collRecords = p_service.RetrieveMultiple(query);

                return collRecords;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getUsersFromCR : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getContactFormCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getUsersFromCR : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getContactFormCR : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de récupérer les titres supports d'un utilisateur
        /// </summary>
        /// <param name="service"></param>
        /// <param name="SystemUserId"></param>
        /// <returns></returns>
        private EntityCollection GetTitresSupports(IOrganizationService service, Guid SystemUserId, DateTime dateRDV)
        {

            EntityCollection oReturnCollection = new EntityCollection();

            try
            {
                QueryExpression query = new QueryExpression("bd_titresupporthistorique");
                query.ColumnSet = new ColumnSet(true);

                FilterExpression filteruser = new FilterExpression(LogicalOperator.And);
                filteruser.Conditions.Add(new ConditionExpression("bd_utilisateur", ConditionOperator.Equal, SystemUserId));

                query.Criteria = filteruser;

                EntityCollection oListTitreSupport = service.RetrieveMultiple(query);

                //Impossiblme d'utiliser le filtre condition expression avec prise en compte de l'heure donc on filtre après

                DateTime v_debut;
                DateTime v_fin;

                foreach (Entity oTs in oListTitreSupport.Entities)
                {
                    if (oTs.Contains("bd_datededbut"))
                        v_debut = (DateTime)oTs.Attributes["bd_datededbut"];
                    else
                        v_debut = dateRDV;

                    if (oTs.Contains("bd_datedefin"))
                        v_fin = (DateTime)oTs.Attributes["bd_datedefin"];
                    else
                        v_fin = dateRDV;

                    if (dateRDV >= v_debut && dateRDV <= v_fin)
                        oReturnCollection.Entities.Add(oTs);
                }

                return oReturnCollection;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("GetTitresSupports : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getContactFormCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("GetTitresSupports : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getContactFormCR : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de retrouver la liste des contacts d'un compte-rendu
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="v_InUserId"></param>
        /// <returns></returns>
        private EntityCollection getMarquesFromCR(IOrganizationService p_service, Guid v_CompteRenduId)
        {
            try
            {
                QueryExpression query = new QueryExpression("bd_marque");
                query.ColumnSet = new ColumnSet(true);
                LinkEntity linkEntity1 = new LinkEntity("bd_marque", "bd_bd_compterendu_bd_marque", "bd_marqueid", "bd_marqueid", JoinOperator.Inner);
                LinkEntity linkEntity2 = new LinkEntity("bd_bd_compterendu_bd_marque", "bd_compterendu", "bd_compterenduid", "bd_compterenduid", JoinOperator.Inner);
                linkEntity1.LinkEntities.Add(linkEntity2);
                query.LinkEntities.Add(linkEntity1);
                linkEntity2.LinkCriteria = new FilterExpression();
                linkEntity2.LinkCriteria.AddCondition(new ConditionExpression("bd_compterenduid", ConditionOperator.Equal, v_CompteRenduId));
                EntityCollection collRecords = p_service.RetrieveMultiple(query);

                return collRecords;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getMarquesFromCR : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getMarquesFromCR : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de savoir si une association Contact-Marque existe déjà
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="p_MarqueId"></param>
        /// <param name="p_ContactId"></param>
        /// <returns></returns>
        private EntityCollection getContactMarque(IOrganizationService p_service, Guid p_MarqueId, Guid p_ContactId)
        {
            try
            {
                QueryExpression query = new QueryExpression("contact");
                query.ColumnSet = new ColumnSet(true);
                LinkEntity linkEntity1 = new LinkEntity("contact", "bd_contact_marque", "contactid", "contactid", JoinOperator.Inner);
                LinkEntity linkEntity2 = new LinkEntity("bd_contact_marque", "bd_marque", "bd_marqueid", "bd_marqueid", JoinOperator.Inner);
                linkEntity1.LinkEntities.Add(linkEntity2);
                query.LinkEntities.Add(linkEntity1);
                linkEntity2.LinkCriteria = new FilterExpression();
                linkEntity2.LinkCriteria.AddCondition(new ConditionExpression("bd_marqueid", ConditionOperator.Equal, p_MarqueId));
                linkEntity1.LinkCriteria = new FilterExpression();
                linkEntity1.LinkCriteria.AddCondition(new ConditionExpression("contactid", ConditionOperator.Equal, p_ContactId));
                EntityCollection collRecords = p_service.RetrieveMultiple(query);

                return collRecords;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getContactMarque : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getContactMarque : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de savoir si une association Contact-Marque existe déjà
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="p_MarqueId"></param>
        /// <param name="p_ContactId"></param>
        /// <returns></returns>
        private EntityReference getSecteurMarque(IOrganizationService p_service, Guid p_MarqueId)
        {
            try
            {
                EntityReference oRef = new EntityReference();
                QueryExpression query = new QueryExpression("bd_marque");
                query.ColumnSet = new ColumnSet("bd_secteuractivite");
                query.Criteria.AddCondition(new ConditionExpression("bd_marqueid", ConditionOperator.Equal, p_MarqueId));
                EntityCollection collRecords = p_service.RetrieveMultiple(query);
                if (collRecords.Entities.Count > 0)
                {
                    oRef = (EntityReference)collRecords.Entities[0].Attributes["bd_secteuractivite"];
                }

                return oRef;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getSecteurMarque : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getSecteurMarque : Une erreur est survenue dans le plugin LG_Associate_Contacts_Participants_Marques getMarqueFromCR : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de récupérer les secteurs associés à un compte rendu
        /// </summary>
        /// <param name="service">Service CRM</param>
        /// <param name="idRDV">Id du RDV</param>
        /// <returns>Entité compte-rendu (Id du CR + RDV)</returns>
        private EntityReferenceCollection getSecteurCR(IOrganizationService service, Guid idCR, string p_relationName, string logicalName)
        {
            try
            {
                EntityReferenceCollection oRefContactCR = new EntityReferenceCollection();

                QueryExpression query = new QueryExpression
                {
                    EntityName = p_relationName,
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_compterenduid",
                                Operator = ConditionOperator.Equal,
                                Values = {idCR}
                            }
                        }
                    }
                };

                DataCollection<Entity> oListContactCR = service.RetrieveMultiple(query).Entities;

                foreach (Entity item in oListContactCR.ToArray())
                {
                    oRefContactCR.Add(new EntityReference(logicalName, (Guid)item.Attributes[logicalName + "id"]));
                }

                return oRefContactCR;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getSecteurCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getSecteurCR : " + e.Message);
            }
        }

    }
}
