﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;

namespace Lagardere.CRM.Plugin.Societe
{
    public class SociétéRecherche : IPlugin
    {
        /// <summary>
        /// Associe le secteur d'activité de la marque à l'annonceur
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            EntityReferenceCollection v_Secteur_Activite = new EntityReferenceCollection();

            try
            {
                Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
                         serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                Entity entity = null;
                Entity preentity = null;
                if (context.PostEntityImages.Contains("PostImage"))
                {
                    entity = context.PostEntityImages["PostImage"];
                }
                else
                {
                    throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin SociétéRecherche, PostImage absent.");
                }
                EntityReference preParent = null;
                if (context.PreEntityImages.Contains("PreImage"))
                {
                    preentity = context.PreEntityImages["PreImage"];
                    preParent = preentity.GetAttributeValue<EntityReference>("parentaccountid");
                }
                //throw new InvalidPluginExecutionException(entity.LogicalName+"-"+ context.Depth);
                if (entity.LogicalName != "account" || context.Depth >= 2)
                    return;

                try
                {
                    EntityReference postParent = entity.GetAttributeValue<EntityReference>("parentaccountid");

                    if ((preParent ==null || preParent.Id == null || preParent.Id == Guid.Empty) &&  (postParent != null && postParent.Id != null && postParent.Id != Guid.Empty))
                    {
                    //    throw new InvalidPluginExecutionException("1");
                        UpdateParentAccount(service, postParent);
                    }
                    else if ((preParent != null && preParent.Id != null && preParent.Id != Guid.Empty) && (postParent == null || postParent.Id == null || postParent.Id == Guid.Empty))
                    {
                    //    throw new InvalidPluginExecutionException("2");
                        UpdateParentAccount(service, preParent);
                    } else if (preParent != null && preParent.Id != Guid.Empty && postParent != null && postParent.Id != Guid.Empty && preParent.Id != postParent.Id)
                    {
                    //    throw new InvalidPluginExecutionException("3");
                        UpdateParentAccount(service, preParent);
                        UpdateParentAccount(service, postParent);
                    }
                    //throw new InvalidPluginExecutionException("4");
                    string res = string.IsNullOrEmpty(entity.GetAttributeValue<String>("bd_codecipres")) ? "" : " - " + entity.GetAttributeValue<String>("bd_codecipres");
                        res = entity.GetAttributeValue<Boolean>("bd_tiersdefacturation") ? "Client ADV" + res : "Fiche comm." + res;
                        entity.Attributes.Add("la_typeetcipres", res);
                        service.Update(entity);
                }
                catch (Exception ex)
                {
                    throw new InvalidPluginExecutionException("Une erreur s'est produite lors de SociétéRecherche. ", ex);
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin SociétéRecherche.", ex);
            }
            catch (Exception ex)
            {
                tracingService.Trace("SociétéRecherche : {0}", ex.ToString());
                throw;
            }
        }
/// <summary>
/// Met à jour le compteur de fils dans la société parente
/// </summary>
/// <param name="entity">L'entité fille</param>
/// <param name="pre">Nature de limage</param>
        private void UpdateParentAccount(IOrganizationService service, EntityReference parent)
        {
            //throw new InvalidPluginExecutionException("Coucou.");
            if (parent != null)
            {
                QueryExpression query = new QueryExpression
                {
                    EntityName = "account",
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "parentaccountid",
                                Operator = ConditionOperator.Equal,
                                Values = { parent.Id}
                            }
                        }
                    }
                };
                DataCollection<Entity> oListAccount = service.RetrieveMultiple(query).Entities;
                Entity Societe = new Entity("account");
                Societe.Id = parent.Id;
                Societe.Attributes["la_nbrattachements"] = oListAccount.Count.ToString();
                service.Update(Societe);
            }
        }

    }

}
