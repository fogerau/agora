﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;

namespace Lagardere.CRM.Plugin.Societe
{
    public class SociétéMarque : IPlugin
    {
        /// <summary>
        /// Associe le secteur d'activité de la marque à l'annonceur
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            EntityReferenceCollection v_Secteur_Activite = new EntityReferenceCollection();

            try
            {
                Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
                         serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);

                Entity entity = null;
                if (context.PostEntityImages.Contains("PostImage"))
                {
                    entity = (Entity)context.PostEntityImages["PostImage"];
                }
                else
                {
                    throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin SocieteMarque, PostImage absent.");
                }

                if (entity.LogicalName != "account") { return; }
                //   if (!entity.Attributes.Contains("bd_secteuractivite")) { return; }
                try
                {
                    if (entity.GetAttributeValue<OptionSetValue>("statecode").Value != 0)
                    {
                        // la société n'est pas active, il faut désactiver ses marque 

                        QueryExpression queryMarque = new QueryExpression
                        {
                            EntityName = "bd_marque",
                            ColumnSet = new ColumnSet { AllColumns = true }
                        };
                        queryMarque.Criteria.AddCondition(new ConditionExpression("bd_annonceuragence", ConditionOperator.Equal, entity.Id));

                        EntityCollection tmp = service.RetrieveMultiple(queryMarque);
                        foreach (Entity marque in tmp.Entities)
                        {
                            SetStateRequest setStateReq = new SetStateRequest();
                            setStateReq.EntityMoniker = new EntityReference(marque.LogicalName, marque.Id);
                            setStateReq.State = new OptionSetValue(1);
                            setStateReq.Status = new OptionSetValue(-1);
                            SetStateResponse response = (SetStateResponse)service.Execute(setStateReq);
                        }

                        //AGORA-1109 Desactiver aussi les contacts
                        QueryExpression queryContact = new QueryExpression
                        {
                            EntityName = "contact",
                            ColumnSet = new ColumnSet { AllColumns = true }
                        };
                        queryContact.Criteria.AddCondition(new ConditionExpression("parentcustomerid", ConditionOperator.Equal, entity.Id));

                        tmp = service.RetrieveMultiple(queryContact);
                        foreach (Entity contact in tmp.Entities)
                        {
                            SetStateRequest setStateReq = new SetStateRequest();
                            setStateReq.EntityMoniker = new EntityReference(contact.LogicalName, contact.Id);
                            setStateReq.State = new OptionSetValue(1);
                            setStateReq.Status = new OptionSetValue(-1);
                            SetStateResponse response = (SetStateResponse)service.Execute(setStateReq);
                        }

                        //FIN AGORA-1109 Desactiver aussi les contacts
                    }
                    if (context.Depth < 2)
                    {
                        string res = string.IsNullOrEmpty(entity.GetAttributeValue<String>("bd_codecipres")) ? "" : " - " + entity.GetAttributeValue<String>("bd_codecipres");
                        res = entity.GetAttributeValue<Boolean>("bd_tiersdefacturation") ? "Client ADV" + res : "Fiche comm." + res;
                        entity.Attributes["la_typeetcipres"] = res;
                        service.Update(entity);
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidPluginExecutionException("Une erreur s'est produite lors de l'inactivation - marque/contact. ", ex);
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin SocieteMarque.", ex);
            }
            catch (Exception ex)
            {
                tracingService.Trace("SocieteMarque : {0}", ex.ToString());
                throw;
            }
        }


    }
}
