﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;

namespace Lagardere.CRM.Plugin.AgenceMarque
{
    public class AgenceMarque : IPlugin
    {
        /// <summary>
        /// Associe le secteur d'activité de la marque à l'annonceur
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            EntityReferenceCollection v_Secteur_Activite = new EntityReferenceCollection();

            try
            {
                Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
                         serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
   
                try
                {
                    if (context.Depth==1 && ("Associate".Equals(context.MessageName) || "Disassociate".Equals(context.MessageName)))
                    {
                        EntityReference target = (EntityReference)context.InputParameters["Target"];
                        if ("account" == target.LogicalName)
                        {
                            // Check the Message Name and the Target Entity
                            EntityReferenceCollection relatedentities = (EntityReferenceCollection)context.InputParameters["RelatedEntities"];
                            foreach (EntityReference relatedEntity in relatedentities)
                            {
                                if ("account".Equals(relatedEntity.LogicalName))
                                {
                                    EntityCollection marques;
                                    EntityReferenceCollection agences = new EntityReferenceCollection();
                                    agences.Add(relatedEntity);
                                    // On est dans le cas ou il faut faire le job
                                    if ("Associate".Equals(context.MessageName)){
                                        // Ajouter l'agence a toutes les marques
                                        marques = this.getMarques(service, target.Id);
                                        foreach (Entity Marque in marques.Entities)
                                        {
                                            Log.Write("1");
                                            if (!ExistsAssoc(service, Marque, agences[0]))
                                            {
                                                Log.Write("2");
                                                service.Associate("bd_marque", Marque.Id, new Relationship("bd__marque_agence"), agences);
                                            }
                                            Log.Write("3");

                                        }
                                    }
                                    else
                                    {
                                        // enlever l'agence a toutes les marques
                                        marques = this.getMarques(service, target.Id);
                                        foreach (Entity Marque in marques.Entities)
                                        {
                                            
                                                Log.Write("4");
                                                if (ExistsAssoc(service, Marque, agences[0]))
                                                {
                                                    Log.Write("5");
                                                    service.Disassociate("bd_marque", Marque.Id, new Relationship("bd__marque_agence"), agences);
                                                }
                                                Log.Write("6");

                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(ex.Message);
                    throw new InvalidPluginExecutionException("Une erreur s'est produite lors de AgenceMarque. ", ex);
                }
                   
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin AgenceMarque.", ex);
            }
            catch (Exception ex)
            {
                tracingService.Trace("SociétéRecherche : {0}", ex.ToString());
                throw;
            }
        }

        private bool ExistsAssoc(IOrganizationService service, Entity marque, EntityReference agence)
        {
            QueryExpression query = new QueryExpression("bd_marque");
            query.ColumnSet = new ColumnSet(true);
            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition(new ConditionExpression("bd_marqueid", ConditionOperator.Equal, marque.Id));

            LinkEntity linkEntity1 = new LinkEntity("bd_marque", "bd__marque_agence", "bd_marqueid", "bd_marqueid", JoinOperator.Inner);

            query.LinkEntities.Add(linkEntity1);

            // Filtre sur la société
            linkEntity1.LinkCriteria = new FilterExpression();
//            linkEntity1.LinkCriteria.AddCondition(new ConditionExpression("bd_marqueid", ConditionOperator.Equal, source.Id));
            linkEntity1.LinkCriteria.AddCondition(new ConditionExpression("accountid", ConditionOperator.Equal, agence.Id));

            EntityCollection collRecords = service.RetrieveMultiple(query);
            Log.Write("nombre : " + collRecords.Entities.Count );
            return collRecords.Entities.Count>0;
        }

        private EntityCollection getMarques(IOrganizationService service, Guid accountid)
        {
            try
            {

                QueryExpression query = new QueryExpression
                {
                    EntityName = "bd_marque",
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_annonceuragence",
                                Operator = ConditionOperator.Equal,
                                Values = { accountid }
                            }
                        }
                    }
                };
               
                return service.RetrieveMultiple(query);

              

            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getMarqueGammeMandatFromIdMarquesGemmes : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getMarqueGammeMandatFromIdMarquesGemmes : " + e.Message);
            }
        }

    }
}
