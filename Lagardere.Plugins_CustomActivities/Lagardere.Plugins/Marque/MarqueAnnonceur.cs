﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Lagardere.CRM.Plugin.Marque
{
    public class MarqueAnnonceur : IPlugin
    {
        /// <summary>
        /// Associe le secteur d'activité de la marque à l'annonceur
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            Log.Write("Début MarqueAnnonceur");
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            EntityReferenceCollection v_Secteur_Activite = new EntityReferenceCollection();

            try
            {
                Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
                         serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);

                //   if (!entity.Attributes.Contains("bd_secteuractivite")) { return; }
                if ("Associate".Equals(context.MessageName) || "Disassociate".Equals(context.MessageName))
                {
                    EntityReference target = (EntityReference)context.InputParameters["Target"];
                    if ("bd_marque" == target.LogicalName)
                    {
                        // Check the Message Name and the Target Entity
                        EntityReferenceCollection relatedentities = (EntityReferenceCollection)context.InputParameters["RelatedEntities"];
                        foreach (EntityReference relatedEntity in relatedentities)
                        {
                            if ("account".Equals(relatedEntity.LogicalName))
                            {
                                // On est dans le cas ou il faut faire le job
                                Entity marque = service.Retrieve("bd_marque", target.Id, new ColumnSet("bd_annonceuragence"));
                                Guid accountId = marque.GetAttributeValue<EntityReference>("bd_annonceuragence").Id;
                                // Supprimer les associations annonceur-Agence
                                DeleteAssociationsAgence(service, accountId);
                                // Recuperer toutes les associations marque-Agence
                                AssociateAgence(service, accountId);

                                // associer annonceurs-agences

                            }
                        }


                    }
                }
                else
                {
                    Entity entity = null;
                    String v_accountId;
                    if (context.PostEntityImages.Contains("Image"))
                    {
                        entity = (Entity)context.PostEntityImages["Image"];
                    }
                    else
                    {
                        entity = (Entity)context.PreEntityImages["Image"];
                    }
                    if (entity.LogicalName != "bd_marque") { return; }
                    try
                    {
                        tracingService.Trace("Début");
                        v_accountId = entity.GetAttributeValue<EntityReference>("bd_annonceuragence").Id.ToString();
                        tracingService.Trace("annonceur");

                        DeleteAssociationsSecteur(service, entity.GetAttributeValue<EntityReference>("bd_annonceuragence").Id);
                        tracingService.Trace("deleted");

                        //Recupérer l'ensemble des secteurs
                        QueryExpression query = new QueryExpression("bd_marque");
                        query.ColumnSet = new ColumnSet(true);
                        query.Criteria.AddCondition("bd_annonceuragence", ConditionOperator.Equal, v_accountId);
                        EntityCollection collMarques = service.RetrieveMultiple(query);
                        foreach (Entity marque in collMarques.Entities)
                        {
                            if (marque.Contains("bd_secteuractivite"))
                            {
                                EntityReference sect = marque.GetAttributeValue<EntityReference>("bd_secteuractivite");
                                if (sect != null && sect.Id != Guid.Empty)
                                {
                                    tracingService.Trace("bd_secteuractivite");
                                    tracingService.Trace(sect.Id.ToString());
                                    tracingService.Trace(sect.Name);
                                    if (!v_Secteur_Activite.Contains(sect))
                                    {
                                        v_Secteur_Activite.Add(sect);
                                    }
                                }
                            }
                        }
                        tracingService.Trace("Suite");
                        if (v_Secteur_Activite.Count > 0)
                        {
                            service.Associate("account", entity.GetAttributeValue<EntityReference>("bd_annonceuragence").Id, new Relationship("bd_account_bd_secteurdactivite"), v_Secteur_Activite);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Write("MarqueAnnonceur :\r\n " + ex.Message);

                        throw new InvalidPluginExecutionException("Une erreur s'est produite lors de l'association Secteur d'activité - Annonceur. ", ex);
                    }
                    // AGORA-1237 création marque ADV et budget
                    /*if (context.MessageName.ToUpper().Equals("CREATE") && entity.Contains("bd_secteuractivite"))
                    {
                        Entity marqueADV = new Entity("bd_marquesgemmes");
                        marqueADV.Attributes.Add("bd_marquegemme", entity.Attributes["bd_name"]);
                        marqueADV.Attributes.Add("bd_annonceur", entity.Attributes["bd_annonceuragence"]);
                        marqueADV.Attributes.Add("bd_secteuractivite", entity.Attributes["bd_secteuractivite"]);
                        Guid marqueId = service.Create(marqueADV);

                        Entity budget = new Entity("bd_budget");
                        budget.Attributes.Add("bd_budget", entity.Attributes["bd_name"]);
                        budget.Attributes.Add("bd_marquegemme", new EntityReference("bd_marquesgemmes", marqueId));
                        budget.Attributes.Add("bd_secteuractivite", entity.Attributes["bd_secteuractivite"]);
                        service.Create(budget);
                    }*/

                    // En cas de création, il faut aller chercher les agences de l'annonceur
                    if ("Create".Equals(context.MessageName)){
                        string Societe = "account";
                        string relationshipEntityName = "la_societe_agence";

                        QueryExpression query = new QueryExpression(Societe);
                        query.ColumnSet = new ColumnSet(true);

                        LinkEntity linkEntity1 = new LinkEntity(Societe, relationshipEntityName, "accountid", "accountidtwo", JoinOperator.Inner);

                        query.LinkEntities.Add(linkEntity1);

                        // Filtre sur la société
                        linkEntity1.LinkCriteria = new FilterExpression();
                        linkEntity1.LinkCriteria.AddCondition(new ConditionExpression("accountidone", ConditionOperator.Equal, entity.GetAttributeValue<EntityReference>("bd_annonceuragence").Id));
                        
                        EntityCollection collRecords = service.RetrieveMultiple(query);
                        EntityReferenceCollection refrec = new EntityReferenceCollection();
                        foreach(Entity e in collRecords.Entities)
                        {
                            refrec.Add(e.ToEntityReference());
                        }
                        
                        service.Associate("bd_marque", entity.Id, new Relationship("bd__marque_agence"), refrec);
                    }
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin MarqueAnnonceur."+ ex.Message, ex);
            }
            catch (Exception ex)
            {
                tracingService.Trace("MarqueAnnonceur : {0}", ex.ToString());
                throw;
            }
        }



        /// <summary>
        /// supprime toutes les associations société/secteur
        /// </summary>
        /// <param name="p_service">WS CRM</param>
        /// <returns>True or False</returns>
        private void DeleteAssociationsSecteur(IOrganizationService p_service, Guid p_accountId)
        {
            string Societe = "account";
            string Secteur = "bd_secteurdactivite";
            string relationshipEntityName = "bd_account_bd_secteurdactivite";

            QueryExpression query = new QueryExpression(Secteur);
            query.ColumnSet = new ColumnSet(true);

            LinkEntity linkEntity1 = new LinkEntity(relationshipEntityName, Societe, "accountid", "accountid", JoinOperator.Inner);
            LinkEntity linkEntity2 = new LinkEntity(Secteur, relationshipEntityName, "bd_secteurdactiviteid", "bd_secteurdactiviteid", JoinOperator.Inner);

            linkEntity2.LinkEntities.Add(linkEntity1);
            query.LinkEntities.Add(linkEntity2);

            // Filtre sur la société
            linkEntity1.LinkCriteria = new FilterExpression();
            linkEntity1.LinkCriteria.AddCondition(new ConditionExpression("accountid", ConditionOperator.Equal, p_accountId));

            Relationship relationship = new Relationship(relationshipEntityName);

            EntityCollection collRecords = p_service.RetrieveMultiple(query);
            EntityReferenceCollection collSecteur = new EntityReferenceCollection();
            foreach (Entity secteur in collRecords.Entities)
            {
                collSecteur.Add(secteur.ToEntityReference());
            }
            p_service.Disassociate(Societe, p_accountId, relationship, collSecteur);

        }


        /// <summary>
        /// supprime toutes les associations agence/secteur
        /// </summary>
        /// <param name="p_service">WS CRM</param>
        /// <returns>True or False</returns>
        private void DeleteAssociationsAgence(IOrganizationService p_service, Guid p_accountId)
        {
            
            //throw new Exception(marque.GetAttributeValue<EntityReference>("bd_annonceuragence").Name);
            string Societe = "account";
            string Agence = "account";
            string relationshipEntityName = "la_societe_agence";

            QueryExpression query = new QueryExpression(Agence);
            query.ColumnSet = new ColumnSet(true);

            LinkEntity linkEntity1 = new LinkEntity(relationshipEntityName, Societe, "accountidone", "accountid", JoinOperator.Inner);
            LinkEntity linkEntity2 = new LinkEntity(Agence, relationshipEntityName, "accountid", "accountidtwo", JoinOperator.Inner);

            linkEntity2.LinkEntities.Add(linkEntity1);
            query.LinkEntities.Add(linkEntity2);

            // Filtre sur la société
            linkEntity1.LinkCriteria = new FilterExpression();
            linkEntity1.LinkCriteria.AddCondition(new ConditionExpression("accountid", ConditionOperator.Equal, p_accountId));

            Relationship relationship = new Relationship(relationshipEntityName);
            relationship.PrimaryEntityRole = EntityRole.Referencing;
            EntityCollection collRecords = p_service.RetrieveMultiple(query);
            EntityReferenceCollection collAgence = new EntityReferenceCollection();
            string res = "";
            foreach (Entity agence in collRecords.Entities)
            {
                res += "//" + agence.GetAttributeValue<string>("name");
                collAgence.Add(agence.ToEntityReference());
            }
            //throw new Exception(res);
            p_service.Disassociate(Societe, p_accountId, relationship, collAgence);

        }
        private void AssociateAgence(IOrganizationService p_service, Guid p_accountId)
        {

            //throw new Exception(marque.GetAttributeValue<EntityReference>("bd_annonceuragence").Name);
            string Marque = "bd_marque";
            string Agence = "account";
            string relationshipEntityName = "bd__marque_agence";

            QueryExpression query = new QueryExpression(Agence);
            query.ColumnSet = new ColumnSet(true);

            LinkEntity linkEntity1 = new LinkEntity(relationshipEntityName, Marque, "bd_marqueid", "bd_marqueid", JoinOperator.Inner);
            LinkEntity linkEntity2 = new LinkEntity(Agence, relationshipEntityName, "accountid", "accountid", JoinOperator.Inner);

            linkEntity2.LinkEntities.Add(linkEntity1);
            query.LinkEntities.Add(linkEntity2);

            // Filtre sur la société
            linkEntity1.LinkCriteria = new FilterExpression();
            linkEntity1.LinkCriteria.AddCondition(new ConditionExpression("bd_annonceuragence", ConditionOperator.Equal, p_accountId));
            // Filtre sur agence
            query.Criteria= new FilterExpression();
            query.Criteria.AddCondition(new ConditionExpression("customertypecode", ConditionOperator.NotEqual, 899240000));


            Relationship relationship = new Relationship("la_societe_agence");
            relationship.PrimaryEntityRole = EntityRole.Referencing;
            EntityCollection collRecords = p_service.RetrieveMultiple(query);
            EntityReferenceCollection collAgence = new EntityReferenceCollection();
            foreach (Entity agence in collRecords.Entities)
            {
                if (!collAgence.Contains(agence.ToEntityReference()))
                {
                    collAgence.Add(agence.ToEntityReference());
                }
            }
            //throw new Exception(res);
            p_service.Associate(Agence, p_accountId, relationship, collAgence);

        }

    }
}
