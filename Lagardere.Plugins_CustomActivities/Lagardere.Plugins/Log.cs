﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Lagardere.CRM.Plugin
{
    public class Log
    {
        const string EventSource = "AgoraRabbitMQService";
        const string EventLogName = "Application";
        public static void Write(string msg)
        {

            if (!EventLog.SourceExists(EventSource))
                EventLog.CreateEventSource(EventSource, EventLogName);
            EventLog.WriteEntry(EventSource, msg);
            if (0 == 1)
            {
                SmtpClient smtpClient = new SmtpClient();
                //int i = 0;
                //int j = 10;
                //int k = j / i;
                MailMessage message = new MailMessage();
                MailAddress fromAddress = new MailAddress("noreply-agora@lagardere-active.com");
                // setup up the host, increase the timeout to 5 minutes
                smtpClient.Host = "smtp-app.siege.la.priv";
                smtpClient.UseDefaultCredentials = false;
                //smtpClient.Credentials = new System.Net.NetworkCredential("omnilog-f.ogerau", "Allblacks99", "SIEGE");
                smtpClient.Timeout = (60 * 5 * 1000);
                message.From = fromAddress;
                message.Subject = "Trace";
                message.IsBodyHtml = true;
                message.Body = msg;
                message.To.Add(new MailAddress("OMNILOG-F.OGERAU@Lagardere-Active.com", "Frédéric Ogerau"));
                smtpClient.Send(message);
            }
        }
        public static void Write(string source,string msg)
        {
            if (!EventLog.SourceExists(EventSource))
                EventLog.CreateEventSource(EventSource, EventLogName);
            EventLog.WriteEntry(EventSource, source);
            EventLog.WriteEntry(EventSource, msg);
            if (0 == 10)
            {
                SmtpClient smtpClient = new SmtpClient();
                //int i = 0;
                //int j = 10;
                //int k = j / i;
                MailMessage message = new MailMessage();
                MailAddress fromAddress = new MailAddress("noreply-agora@lagardere-active.com");
                // setup up the host, increase the timeout to 5 minutes
                smtpClient.Host = "smtp-app.siege.la.priv";
                smtpClient.UseDefaultCredentials = false;
                //smtpClient.Credentials = new System.Net.NetworkCredential("omnilog-f.ogerau", "Allblacks99","SIEGE");
                smtpClient.Timeout = (60 * 5 * 1000);
                message.From = fromAddress;
                message.Subject = source;
                message.IsBodyHtml = true;
                message.Body = msg;
                message.To.Add(new MailAddress("OMNILOG-F.OGERAU@Lagardere-Active.com", "Frédéric Ogerau"));
                smtpClient.Send(message);
            }
        }
    }
}