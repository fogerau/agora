﻿using System;
using System.ServiceModel;

// Microsoft Dynamics CRM namespace(s)
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;

namespace Lagardere.Plugin
{
    #region CompteRenduPostUpdate
    public class ListAddListMembers_RemoveMembers : IPlugin
    {
        /// <summary>
        /// Partage de chaque Marque à chaque Contact participant.
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            //Guid v_perimetre = new Guid();
            EntityReference refCompte = new EntityReference();

            //Trace SandBox
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {
                // Récupération du contexte
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);

                Entity TDJ = (Entity)context.PostEntityImages["PAC"];

                EntityReference refPAC = ((Microsoft.Xrm.Sdk.Entity)(context.InputParameters["Target"])).ToEntityReference();
                Guid v_utilisateur = ((EntityReference)TDJ.Attributes["ownerid"]).Id;
                Guid v_PortefeuilleId = ((EntityReference)TDJ.Attributes["bd_portefeuille"]).Id;

                switch (context.MessageName)
                {
                    case "Update":

                        #region Ajout des nouveaux membres
                        EntityReferenceCollection oRefCollAccount = getMembersOfPac(service, v_PortefeuilleId);
                        foreach (EntityReference oAccount in oRefCollAccount)
                        {
                            //On récupère tous les secteurs de l'annonceur et on crée autant de périmètre que de secteur
                            EntityReferenceCollection oSecteurs = GetSecteursAnnonceur(service, oAccount.Id);
                            if (oSecteurs.Count > 0)
                            {
                                foreach (EntityReference oSecteur in oSecteurs)
                                {
                                    if (!IsPerimetreExist(service, v_utilisateur, oAccount.Id,oSecteur.Id))
                                    {
                                        CreatePerimetre(refPAC, oAccount, v_utilisateur, service, oSecteur);
                                    }
                                }
                            }
                            else
                            {
                                if (!IsPerimetreExist(service, v_utilisateur, oAccount.Id))
                                {
                                    CreatePerimetre(refPAC, oAccount, v_utilisateur, service, null);
                                }
                            }
                        }
                        #endregion

                        #region Suprresion des anciens membre
                        EntityCollection oCollPerimetre = GetPerimetresPAC(service, refPAC.Id);
                        foreach (Entity oPerimetre in oCollPerimetre.Entities)
                        {
                            if (IsMemberNotExist(service, v_PortefeuilleId, ((EntityReference)oPerimetre.Attributes["bd_annonceuragence"]).Id))
                            {
                                DeletePerimetre(service, ((EntityReference)oPerimetre.Attributes["bd_annonceuragence"]).Id);
                            }
                        }
                        #endregion

                        break;

                    default:
                        throw new InvalidPluginExecutionException("Default - Erreur lors de la création du périmètre");
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin. Veuillez contacter l'administrateur de l'application.", ex);
            }
            //</snippetFollowupPlugin3>

            catch (Exception ex)
            {
                tracingService.Trace("FollowupPlugin: {0}", ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Retourne les membres du portefeuille associé au PAC
        /// </summary>
        /// <param name="p_service">Service d'organisation</param>
        /// <param name="p_PortefeuilleId">ID du portefeuille associé au PAC</param>
        /// <returns></returns>
        private EntityReferenceCollection getMembersOfPac(IOrganizationService p_service, Guid p_TDJId)
        {
            EntityCollection oCollAccounts = new EntityCollection();
            EntityReferenceCollection oRefCollAccounts = new EntityReferenceCollection();

            try
            {
                QueryExpression query = new QueryExpression("account");
                query.ColumnSet = new ColumnSet(true);
                query.Criteria.AddCondition(new ConditionExpression("bd_tiersdefacturation", ConditionOperator.Equal, "0"));
                LinkEntity listlink = query.AddLink("listmember", "accountid", "entityid").AddLink("list", "listid", "listid");
                listlink.LinkCriteria.AddCondition("listid", ConditionOperator.Equal, p_TDJId);
                oCollAccounts = p_service.RetrieveMultiple(query);

                foreach (Entity oAccount in oCollAccounts.Entities)
                {
                    oRefCollAccounts.Add(oAccount.ToEntityReference());
                }
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("Erreur lors de la récupération des membres du portefeuille : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("Erreur lors de la récupération des membres du portefeuille : " + e.Message);
            }
            return oRefCollAccounts;
        }

        /// <summary>
        /// Permet de créer le périmètre.
        /// </summary>
        private void CreatePerimetre(EntityReference p_RefPAC, EntityReference p_RefCompte, Guid p_Utilisateur, IOrganizationService service, EntityReference p_Secteur)
        {
            Entity newPerimetre = new Entity("bd_ligneterraindejeu");
            newPerimetre["bd_terraindejeu"] = p_RefPAC; // Terrain de Jeu
            newPerimetre["bd_annonceuragence"] = p_RefCompte; // Compte
            newPerimetre["bd_name"] = p_RefCompte.Name; // Compte
            if (p_Secteur != null)
                newPerimetre["bd_secteurdactivite"] = p_Secteur; // Compte
            OptionSetValue oPerimetreStatutCommercial = new OptionSetValue(899240003); // Statut commercial inactif
            newPerimetre["bd_statutcommercial"] = oPerimetreStatutCommercial;
            Guid perimetreID = service.Create(newPerimetre);

            //Puis on l'affecte au commercial
            AssignRequest oAssignRequest = new AssignRequest
            {
                Assignee = new EntityReference("systemuser", p_Utilisateur),
                Target = new EntityReference("bd_ligneterraindejeu", perimetreID)
            };
            service.Execute(oAssignRequest);
        }

        /// <summary>
        /// Retourne l'utilisateur propriétaire de la liste
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="p_listId"></param>
        /// <returns></returns>
        private Guid getUserListFromId(IOrganizationService p_service, Guid p_listId)
        {
            try
            {
                QueryExpression query = new QueryExpression
                {
                    EntityName = "list",
                    ColumnSet = new ColumnSet("ownerid"),
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "listid",
                                Operator = ConditionOperator.Equal,
                                Values = {p_listId}
                            }
                        }
                    }
                };

                EntityCollection oListList = p_service.RetrieveMultiple(query);

                if (oListList.Entities.Count > 0)
                {
                    return ((EntityReference)(oListList.Entities[0].Attributes["ownerid"])).Id;
                }
                else
                {
                    return Guid.Empty;
                }
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("ListAddListMembers_RemoveMembers : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("ListAddListMembers_RemoveMembers : " + e.Message);
            }
        }

        /// <summary>
        /// Retourne le PAC de l'utilisateur
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="p_listId"></param>
        /// <returns></returns>
        private EntityReference getPacFromUserId(IOrganizationService p_service, Guid p_userId, Guid p_Portefeuille)
        {
            EntityReference refPAC = null;

            try
            {
                QueryExpression query = new QueryExpression("bd_terraindejeu");
                query.ColumnSet = new ColumnSet("bd_terraindejeuid", "bd_name");
                query.Criteria = new FilterExpression(LogicalOperator.And);
                ConditionExpression User = new ConditionExpression("ownerid", ConditionOperator.Equal, p_userId);
                ConditionExpression Portefeuille = new ConditionExpression("bd_portefeuille", ConditionOperator.Equal, p_Portefeuille);
                query.Criteria.AddCondition(User);
                query.Criteria.AddCondition(Portefeuille);

                EntityCollection oListPAC = p_service.RetrieveMultiple(query);

                if (oListPAC.Entities.Count > 0)
                {
                    refPAC = new EntityReference();
                    refPAC.Id = (Guid)oListPAC.Entities[0].Attributes["bd_terraindejeuid"];
                    refPAC.Name = oListPAC.Entities[0].Attributes["bd_name"].ToString();
                    refPAC.LogicalName = "bd_terraindejeu";
                }

                return refPAC;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("ListAddListMembers_RemoveMembers getPacFromUserId : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("ListAddListMembers_RemoveMembers getPacFromUserId : " + e.Message);
            }
        }

        /// <summary>
        /// Retourne le PAC de l'utilisateur
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="p_listId"></param>
        /// <returns></returns>
        private EntityReference getCompteFromAccountId(IOrganizationService p_service, Guid p_AccountId)
        {
            EntityReference refCompte = null;

            try
            {
                QueryExpression query = new QueryExpression
                {
                    EntityName = "account",
                    ColumnSet = new ColumnSet("name"),
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "accountid",
                                Operator = ConditionOperator.Equal,
                                Values = {p_AccountId}
                            }
                        }
                    }
                };

                EntityCollection oListUser = p_service.RetrieveMultiple(query);

                if (oListUser.Entities.Count > 0)
                {
                    refCompte = new EntityReference();
                    refCompte.Id = p_AccountId;
                    refCompte.Name = oListUser.Entities[0].Attributes["name"].ToString();
                    refCompte.LogicalName = "account";
                }

                return refCompte;

            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("ListAddListMembers_RemoveMembers getCompteFromAccountId : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("ListAddListMembers_RemoveMembers getCompteFromAccountId : " + e.Message);
            }
        }

        /// <summary>
        /// Retourne le périmètre du PAC de l'utilisateur correspondant au compte et au secteur
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="p_listId"></param>
        /// <returns></returns>
        private Boolean IsPerimetreExist(IOrganizationService p_service, Guid p_userId, Guid p_accountId, Guid? p_secteurId = null)
        {
            try
            {
                QueryExpression query = new QueryExpression();
                query.EntityName = "bd_ligneterraindejeu";
                query.ColumnSet.AddColumns("bd_ligneterraindejeuid");
                query.Criteria = new FilterExpression();
                query.Criteria.AddCondition("ownerid", ConditionOperator.Equal, p_userId);
                query.Criteria.AddCondition("bd_annonceuragence", ConditionOperator.Equal, p_accountId);
                if (p_secteurId.HasValue && p_secteurId.Value != Guid.Empty)
                    query.Criteria.AddCondition("bd_secteurdactivite", ConditionOperator.Equal, p_secteurId.Value);

                EntityCollection oListPerimetre = p_service.RetrieveMultiple(query);

                if (oListPerimetre.Entities.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("Erreur lors de la récupération du périmètre : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("Erreur lors de la récupération du périmètre : " + e.Message);
            }
        }

        /// <summary>
        /// Retourne une collection d'entité des périmètres d'activité de l'annonceur
        /// </summary>
        /// <param name="p_service">Service d'organisation</param>
        /// <param name="AccountID">ID de la société</param>
        /// <returns></returns>
        private EntityCollection GetPerimetresPAC(IOrganizationService p_service, Guid p_PacId)
        {
            EntityCollection oCollPerimetres = new EntityCollection();
            try
            {
                QueryExpression query = new QueryExpression("bd_ligneterraindejeu");
                query.ColumnSet = new ColumnSet("bd_ligneterraindejeuid", "bd_annonceuragence");
                query.Criteria.AddCondition("bd_terraindejeu", ConditionOperator.Equal, p_PacId);

                oCollPerimetres = p_service.RetrieveMultiple(query);

                return oCollPerimetres;

            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("Erreur lors de la récupération des secteurs de la société : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("Erreur lors de la récupération des secteurs de la société : " + e.Message);
            }
        }

        /// <summary>
        /// Retourne une collection d'entité des secteurs d'activité de l'annonceur
        /// </summary>
        /// <param name="p_service">Service d'organisation</param>
        /// <param name="AccountID">ID de la société</param>
        /// <returns></returns>
        private EntityReferenceCollection GetSecteursAnnonceur(IOrganizationService p_service, Guid AccountID)
        {
            EntityReferenceCollection oSecteurs = new EntityReferenceCollection();

            try
            {
                QueryExpression query = new QueryExpression
                {
                    EntityName = "bd_account_bd_secteurdactivite",
                    ColumnSet = new ColumnSet("bd_secteurdactiviteid"),
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "accountid",
                                Operator = ConditionOperator.Equal,
                                Values = {AccountID}
                            }
                        }
                    }
                };

                foreach (Entity Secteur in p_service.RetrieveMultiple(query).Entities)
                {
                    oSecteurs.Add(new EntityReference("bd_secteurdactivite", new Guid(Secteur.Attributes["bd_secteurdactiviteid"].ToString())));
                }
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("Erreur lors de la récupération des secteurs de la société : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("Erreur lors de la récupération des secteurs de la société : " + e.Message);
            }
            return oSecteurs;
        }

        /// <summary>
        /// Vérifie l'existence de la société dans le portefeuille
        /// </summary>
        /// <param name="p_service">Service d'organisation</param>
        /// <param name="p_PortefeuilleId">ID du portefeuille associé au PAC</param>
        /// <returns></returns>
        private Boolean IsMemberNotExist(IOrganizationService p_service, Guid p_Portefeuille, Guid p_Account)
        {
            EntityCollection oCollAccounts = new EntityCollection();
            EntityReferenceCollection oRefCollAccounts = new EntityReferenceCollection();

            try
            {
                QueryExpression query = new QueryExpression("account");
                query.ColumnSet = new ColumnSet(true);
                query.Criteria.AddCondition("accountid", ConditionOperator.Equal, p_Account);
                LinkEntity listlink = query.AddLink("listmember", "accountid", "entityid"); //.AddLink("list", "listid", "listid");
                listlink.LinkCriteria.AddCondition("listid", ConditionOperator.Equal, p_Portefeuille);
                oCollAccounts = p_service.RetrieveMultiple(query);

                if (oCollAccounts.Entities.Count > 0)
                    return false;
                else
                    return true;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("Erreur IsMembeNotExist lors de la récupération d'un membre du portefeuille : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("Erreur IsMembeNotExist lors de la récupération d'un membre du portefeuille : " + e.Message);
            }
        }

        /// <summary>
        /// Supprime les périmètres d'une société (Annonceur ou Agence)
        /// </summary>
        /// <param name="p_service">Service d'organisation</param>
        /// <param name="p_Account">Id de la société</param>
        private void DeletePerimetre(IOrganizationService p_service, Guid p_Account)
        {
            EntityCollection oCollPerimetres;

            QueryExpression Query = new QueryExpression("bd_ligneterraindejeu");
            Query.ColumnSet = new ColumnSet("bd_ligneterraindejeuid");
            Query.Criteria.AddCondition(new ConditionExpression("bd_annonceuragence", ConditionOperator.Equal, p_Account));
            oCollPerimetres = p_service.RetrieveMultiple(Query);

            foreach (Entity oPerimetre in oCollPerimetres.Entities)
            {
                p_service.Delete("bd_ligneterraindejeu", oPerimetre.Id);
            }
        }
    }
    #endregion
}
