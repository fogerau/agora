﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// These namespaces are found in the Microsoft.Xrm.Sdk.dll assembly
// found in the SDK\bin folder.
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Discovery;

// This namespace is found in Microsoft.Crm.Sdk.Proxy.dll assembly
// found in the SDK\bin folder.
using Microsoft.Crm.Sdk.Messages;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Messages;

namespace Lagardere.CRM.Plugin.RendezVous
{
    #region RendezVousPostUpdate
    public class ParticipantClientRendezVousPostUpdate : IPlugin
    {
        string g_BUFFER = "";

        private ITracingService tracingService;

        public void Execute(IServiceProvider serviceProvider)
        {
            Entity entity = null;
            EntityReferenceCollection v_refContact = null;
            EntityReferenceCollection v_refContactNew = new EntityReferenceCollection();
            EntityReferenceCollection v_refAccountNew = new EntityReferenceCollection();
            EntityReferenceCollection v_refSystemUser = new EntityReferenceCollection();
            EntityReferenceCollection v_refSystemUserNew = new EntityReferenceCollection();
            EntityReferenceCollection v_refSecteurdactivite = new EntityReferenceCollection();
            string errorText = "0";
            try
            {
                Log.Write("Début ParticipantClientRendezVousPostUpdate");
                tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

                Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
                       serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));

                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);

                if (context.Depth<2 && context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    Log.Write("target OK");
                    entity = (Entity)context.InputParameters["Target"];

                    #region Récuperation des contacts et secteurs déjà associés
                    errorText = "0";
                    if ((entity.LogicalName != "appointment") && (entity.LogicalName != "bd_compterendu"))
                        return;
                    errorText = "1";
                    if (entity.LogicalName == "bd_compterendu" && context.MessageName != "create")
                    {
                        errorText = "2";
                        entity = (Entity)context.PostEntityImages["PostImage"];
                        entity = getRDVduCR(service, ((EntityReference)entity.Attributes["bd_rendezvous"]).Id);
                    }
                    errorText = "3";
                    //On récupère les secteurs de l'annonceur
                    //Guid v_AccounId = ((EntityReference)((Entity)context.PostEntityImages["PostImage"]).Attributes["bd_annonceuragence"]).Id;
                    //EntityReferenceCollection v_refSecteurdactiviteNew = getSecteurAnnonceurAgence(service, v_AccounId);

                    EntityReferenceCollection v_refParticipants = getParticipant("requiredattendees", entity);
                    EntityReferenceCollection v_refParticipantLagardere = getParticipant("optionalattendees", entity);
                    foreach (EntityReference item in v_refParticipantLagardere) { v_refParticipants.Add(item); }
                    errorText = "4";
                    foreach (EntityReference item in v_refParticipants)
                    {
                        errorText = "5";
                        switch (item.LogicalName.ToUpper())
                        {
                            case "CONTACT":
                                errorText = "6";
                                v_refContactNew.Add(item);
                                break;
                            case "ACCOUNT":
                                errorText = "7";
                                v_refAccountNew.Add(item);
                                break;
                            case "SYSTEMUSER":
                                errorText = "8";
                                v_refSystemUserNew.Add(item);
                                break;
                            default:
                                break;
                        }
                    }
                    #endregion
                    errorText = "9";

                    Entity CR = getCompteRendu(service, entity.Id);
                    errorText = "10";
                    if (CR != null)
                    {
                        #region On récupère les anciennes associations
                        errorText = "11";
                        v_refContact = getContactCR(service, CR.Id, "bd_contact_bd_compterendu", "contact");
                        v_refSystemUser = getContactCR(service, CR.Id, "bd_compterendu_systemuser", "systemuser");
                        v_refSecteurdactivite = getContactCR(service, CR.Id, "bd_compterendu_secteurdactivite", "bd_secteurdactivite");
                        errorText = "12";
                        Entity CRavantMAJ = null;
                        if (context.PreEntityImages.Contains("Target") && context.PreEntityImages["Target"] is Entity)
                        {
                            errorText = "13";
                            CRavantMAJ = context.PreEntityImages["Target"];
                        }
                        //v_refContacMarque = "bd_bd_compterendu_bd_marque"; RESTE A RECUP LES MARQUES A DE-ASSOCIER
                        //v_refContacMarque = getMarqueCR(service, CR.Id, "bd_bd_compterendu_bd_marque");
                        /*if (CRavantMAJ != null)
                        {
                            v_refContacMarque = getMarqueContactAvantUpdate(service, CRavantMAJ);
                        }*/
                        #endregion

                        #region De-association / Association
                        //On désassocie les participants clients (contacts du RDV) du compte-rendu 
                        if (v_refContactNew.Count > 0)
                        {
                            errorText = "14";
                            if (v_refContact.Count > 0)
                            {
                                service.Disassociate(CR.LogicalName, CR.Id, new Relationship("bd_contact_bd_compterendu"), v_refContact);
                            }
                            errorText = "144";
                            service.Associate(CR.LogicalName, CR.Id, new Relationship("bd_contact_bd_compterendu"), v_refContactNew);
                        }
                        errorText = "141";
                        //On ajoute le propriétaire en tant que commercial
                        if (context.PostEntityImages.ContainsKey("PostImage"))
                        {
                            errorText = "15";
                            Entity EntityCR = (Entity)context.PostEntityImages["PostImage"];
                            EntityReference Proprio = (EntityReference)EntityCR.Attributes["ownerid"];
                            v_refSystemUser.Add(Proprio);
                            v_refSystemUserNew.Add(Proprio);
                        }
                        errorText = "151";
                        if (v_refSystemUserNew.Count > 0)
                        {
                            errorText = "16";
                            service.Disassociate(CR.LogicalName, CR.Id, new Relationship("bd_compterendu_systemuser"), v_refSystemUser);
                            errorText = "162";
                            service.Associate(CR.LogicalName, CR.Id, new Relationship("bd_compterendu_systemuser"), v_refSystemUserNew);
                        }
                        errorText = "161";
                        /*if (v_refSecteurdactiviteNew.Count > 0)
                        {
                            service.Disassociate(CR.LogicalName, CR.Id, new Relationship("bd_compterendu_secteurdactivite"), v_refSecteurdactivite);
                            service.Associate(CR.LogicalName, CR.Id, new Relationship("bd_compterendu_secteurdactivite"), v_refSecteurdactiviteNew);
                        }*/
                        #endregion
                    }
                }
                else
                    return;
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                Log.Write(errorText + "\r\nLagardere.CRM.Plugin.RendezVous :\r\n " + ex.Message);
                Log.Write(ex.StackTrace);
                throw new InvalidPluginExecutionException("Une erreur est survenue dans le plugin Lagardere.CRM.Plugin.RendezVous", ex);
            }
            catch (Exception ex)
            {
                tracingService.Trace("Lagardere.CRM.Plugin.RendezVous : {0}", ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        private Entity getRDVduCR(IOrganizationService service, Guid guid)
        {
            try
            {
                Entity RDV = service.Retrieve("appointment", guid, new ColumnSet { AllColumns = true });

                return RDV;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getRDVduCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getRDVduCR : " + e.Message);
            }
        }

        /// <summary>
        /// Retourne les marques des contacts avant la modification des contacts du RDV
        /// </summary>
        /// <param name="p_service">service du crm plugin</param>
        /// <param name="p_CRavantMAJ">Image ENTITY avant l'update</param>
        /// <returns>EntityReferenceCollection contenant les Marques associées aux anciens contacts</returns>
        private EntityReferenceCollection getMarqueContactAvantUpdate(IOrganizationService p_service, Entity p_CRavantMAJ)
        {
            EntityReferenceCollection v_MarqueContactAvantUpdate = new EntityReferenceCollection();
            EntityReferenceCollection v_Contactold = getParticipant("requiredattendees", p_CRavantMAJ);

            QueryExpression queryMarqueDeContact = new QueryExpression
            {
                EntityName = "bd_marque",
                ColumnSet = new ColumnSet { AllColumns = true }
            };


            LinkEntity linkEntity1 = new LinkEntity("bd_marque", "bd_marquesducontactid", "bd_marqueid", "{Entity 1 Primary field}", JoinOperator.Inner);

            LinkEntity linkEntity2 = new LinkEntity("bd_marquesducontactid", "contacct", "contactid", "{Entity 2 Primary field}", JoinOperator.Inner);
            linkEntity1.LinkEntities.Add(linkEntity2);
            queryMarqueDeContact.LinkEntities.Add(linkEntity1);
            linkEntity2.LinkCriteria.AddCondition(new ConditionExpression("contactid", ConditionOperator.Equal, v_Contactold[0].Id));

            EntityCollection tmp = p_service.RetrieveMultiple(queryMarqueDeContact);


            return v_MarqueContactAvantUpdate;
        }

        /// <summary>
        /// Récupére les marques du Comte rendu
        /// </summary>
        /// <param name="p_service">service du crm plugin</param>
        /// <param name="p_CRid">Id du compte rendu sur lequel récupérer les marques</param>
        /// <param name="p_RelationName">Nom de la relation entre le comte rendu et les marques</param>
        /// <returns></returns>
        private EntityReferenceCollection getMarqueCR(IOrganizationService p_service, Guid p_CRid, string p_RelationName)
        {
            try
            {
                EntityReferenceCollection oRefMarqueCR = new EntityReferenceCollection();

                QueryExpression query = new QueryExpression
                {
                    EntityName = p_RelationName,
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_compterenduid",
                                Operator = ConditionOperator.Equal,
                                Values = {p_CRid}
                            }
                        }
                    }
                };

                DataCollection<Entity> oListContactCR = p_service.RetrieveMultiple(query).Entities;

                foreach (Entity item in oListContactCR.ToArray())
                {
                    oRefMarqueCR.Add(new EntityReference("bd_marque", (Guid)item.Attributes["bd_marqueid"]));
                }

                return oRefMarqueCR;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getContactCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getContactCR : " + e.Message);
            }
        }

        /// <summary>
        /// Récupére toutes les marques associées a un contact
        /// </summary>
        /// <param name="p_service">service du crm plugin</param>
        /// <param name="p_contact">entityReference du contact</param>
        /// <returns>collection des marques</returns>
        private EntityReferenceCollection getMarqueFromContact(IOrganizationService service, EntityReference p_contact)
        {
            try
            {
                EntityReferenceCollection p_refContactMarqueNew = new EntityReferenceCollection();

                QueryExpression query = new QueryExpression
                {
                    EntityName = "bd_marque",
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_marquesducontactid",
                                Operator = ConditionOperator.Equal,
                                Values = {p_contact.Id}
                            }
                        }
                    }
                };

                DataCollection<Entity> oListMarqueContact = service.RetrieveMultiple(query).Entities;

                foreach (Entity item in oListMarqueContact.ToArray())
                {
                    p_refContactMarqueNew.Add(new EntityReference("bd_marque", (Guid)item.Attributes["bd_marqueid"]));
                }

                return p_refContactMarqueNew;

            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getMarqueFromContact : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getMarqueFromContact : " + e.Message);
            }
        }


        /// <summary>
        /// Permet de retrouver les secteurs d'activité d'un annonceur
        /// </summary>
        /// <param name="service"></param>
        /// <param name="p_AccountId"></param>
        /// <returns>La liste des secteurs d'activité</returns>
        private EntityReferenceCollection getSecteurAnnonceurAgence(IOrganizationService service, Guid p_AccountId)
        {

            QueryExpression query = new QueryExpression();
            EntityReferenceCollection p_refSecteurAnnonceurAgenceNew = new EntityReferenceCollection();
            DataCollection<Entity> oListSecteur;

            try
            {
                //On détermine s'il s'agit d'un annonceur d'une agence
                Entity oAccount = service.Retrieve("account", p_AccountId, new ColumnSet("customertypecode"));
                if (oAccount != null && oAccount.Contains("customertypecode"))
                {
                    switch (((OptionSetValue)oAccount.Attributes["customertypecode"]).Value)
                    {
                        case 899240000:
                            #region Annonceur
                            query.EntityName = "bd_account_bd_secteurdactivite";
                            query.ColumnSet = new ColumnSet("bd_secteurdactiviteid");
                            query.Criteria = new FilterExpression();
                            ConditionExpression conditionAccount = new ConditionExpression("accountid", ConditionOperator.Equal, p_AccountId);
                            query.Criteria.AddCondition(conditionAccount);

                            oListSecteur = service.RetrieveMultiple(query).Entities;

                            foreach (Entity item in oListSecteur.ToArray())
                            {
                                p_refSecteurAnnonceurAgenceNew.Add(new EntityReference("bd_secteurdactivite", (Guid)item.Attributes["bd_secteurdactiviteid"]));
                            }
                            #endregion
                            break;

                        default:
                            /*select distinct m.bd_secteurActivite,ma.accountid from bd__marque_agence ma
                            inner join bd_marque m on m.bd_marqueId = ma.bd_marqueid
                            where ma.accountid = '73701065-D74E-E411-8090-18A905457570'*/
                            query.EntityName = "bd__marque_agence";
                            //query.ColumnSet = new ColumnSet(true);
                            query.Distinct = true;
                            LinkEntity le = new LinkEntity("bd__marque_agence", "bd_marque", "bd_marqueid", "bd_marqueid", JoinOperator.Inner);
                            le.Columns = new ColumnSet("bd_secteuractivite");
                            le.EntityAlias = "SecteurAgence";
                            query.LinkEntities.Add(le);
                            query.Criteria = new FilterExpression();
                            ConditionExpression condition = new ConditionExpression("accountid", ConditionOperator.Equal, p_AccountId);
                            query.Criteria.AddCondition(condition);

                            oListSecteur = service.RetrieveMultiple(query).Entities;

                            foreach (Entity item in oListSecteur.ToArray())
                            {
                                if (item.Contains("SecteurAgence.bd_secteuractivite")  && !p_refSecteurAnnonceurAgenceNew.Contains((EntityReference)((AliasedValue)item.Attributes["SecteurAgence.bd_secteuractivite"]).Value))
                                    p_refSecteurAnnonceurAgenceNew.Add(new EntityReference("bd_secteurdactivite", ((EntityReference)((AliasedValue)item.Attributes["SecteurAgence.bd_secteuractivite"]).Value).Id));
                            }
                            break;
                    }
                }

                return p_refSecteurAnnonceurAgenceNew;

            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getSecteurAnnonceurAgence : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getSecteurAnnonceurAgence : " + e.Message);
            }
        }



        /// <summary>
        /// Permet de récuperer les participants présents dans le champs passé en paramètre "field".
        /// </summary>
        /// <param name="field">Nom du champ</param>
        /// <param name="entity">Entité visé</param>
        /// <param name="context">Context</param>
        /// <returns>Collection d'entité reférentes</returns>
        private EntityReferenceCollection getParticipant(string field, Entity entity)
        {
            EntityCollection participants = null;
            EntityReferenceCollection refParticipants = new EntityReferenceCollection();
            if (entity.Attributes.Contains(field))
            {
                participants = (EntityCollection)entity[field];
                foreach (Entity item in participants.Entities)
                {
                    refParticipants.Add(((EntityReference)(item.Attributes["partyid"])));
                }
            }
            return refParticipants;
        }

        /// <summary>
        /// Permet de récupérer le CR associé à un RDV
        /// </summary>
        /// <param name="service">Service CRM</param>
        /// <param name="idRDV">Id du RDV</param>
        /// <returns>Entité compte-rendu (Id du CR + RDV)</returns>
        private Entity getCompteRendu(IOrganizationService service, Guid idRDV)
        {
            try
            {
                QueryExpression query = new QueryExpression
                {
                    EntityName = "bd_compterendu",
                    //ColumnSet = new ColumnSet("bd_compterenduid", "bd_rendezvous"),
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_rendezvous",
                                Operator = ConditionOperator.Equal,
                                Values = {idRDV}
                            }
                        }
                    }
                };

                EntityCollection e = service.RetrieveMultiple(query);

                if (e.Entities.Count > 0) { return e.Entities[0]; }
                else { return null; }

            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getCompteRendu : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getCompteRendu : " + e.Message);
            }
        }

        /// <summary>
        /// Permet de récupérer le CR associé à un RDV
        /// </summary>
        /// <param name="service">Service CRM</param>
        /// <param name="idRDV">Id du RDV</param>
        /// <returns>Entité compte-rendu (Id du CR + RDV)</returns>
        private EntityReferenceCollection getContactCR(IOrganizationService service, Guid idCR, string p_relationName, string logicalName)
        {
            try
            {
                EntityReferenceCollection oRefContactCR = new EntityReferenceCollection();

                QueryExpression query = new QueryExpression
                {
                    EntityName = p_relationName,
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_compterenduid",
                                Operator = ConditionOperator.Equal,
                                Values = {idCR}
                            }
                        }
                    }
                };

                DataCollection<Entity> oListContactCR = service.RetrieveMultiple(query).Entities;

                foreach (Entity item in oListContactCR.ToArray())
                {
                    oRefContactCR.Add(new EntityReference(logicalName, (Guid)item.Attributes[logicalName + "id"]));
                }

                return oRefContactCR;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getContactCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getContactCR : " + e.Message);
            }
        }

        private void setBuffer(string p_bufer)
        {
            g_BUFFER += p_bufer;
        }
        /*
        /// <summary>
        /// Récupére les marques du Comte rendu
        /// </summary>
        /// <param name="p_service">service du crm plugin</param>
        /// <param name="p_CRid">Id du compte rendu sur lequel récupérer les marques</param>
        /// <param name="p_RelationName">Nom de la relation entre le comte rendu et les marques</param>
        /// <returns></returns>
        private EntityReferenceCollection getMarqueCR(IOrganizationService p_service, Guid p_CRid, string p_RelationName)
        {
            try
            {
                EntityReferenceCollection oRefMarqueCR = new EntityReferenceCollection();

                QueryExpression query = new QueryExpression
                {
                    EntityName = p_RelationName,
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_compterenduid",
                                Operator = ConditionOperator.Equal,
                                Values = {p_CRid}
                            }
                        }
                    }
                };

                DataCollection<Entity> oListContactCR = p_service.RetrieveMultiple(query).Entities;

                foreach (Entity item in oListContactCR.ToArray())
                {
                    oRefMarqueCR.Add(new EntityReference("bd_marque", (Guid)item.Attributes["bd_marqueid"]));
                }

                return oRefMarqueCR;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getContactCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getContactCR : " + e.Message);
            }
        }*/
    }
    #endregion
}
