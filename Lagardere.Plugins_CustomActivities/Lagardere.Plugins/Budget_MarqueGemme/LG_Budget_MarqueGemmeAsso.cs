﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Lagardere.CRM.Plugin.Budget_MarqueGemme
{
    class LG_Budget_MarqueGemmeAsso : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {
                // Obtain the execution context from the service provider.
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                Entity entity = null;

                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    entity = (Entity)context.InputParameters["Target"];
                    if (entity.LogicalName != "bd_compterendu") { return; }
                }
                else
                    return;
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException("An error occurred in the LG_Budget_MarqueGemmeAsso plug-in.", ex);
            }

            catch (Exception ex)
            {
                tracingService.Trace("FollowupPlugin | LG_Budget_MarqueGemmeAsso: {0}", ex.ToString());
                throw;
            }
        }
    }
}
