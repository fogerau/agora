﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Lagardere.CRM.Plugin.LG_Syncrho_MarqueGemme_Mandat_Budget
{
    public class LG_Syncrho_MarqueGemme_Mandat_Budget : IPlugin
    {

        /// <summary>
        /// Ce plugin permet de synchroniser (Ciprès, Gemme, MediaPilot) la société. Pour déclencher nous mettons un champ de la société afin de déclencher la synchro RABBITMQ
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            Log.Write("SociétéRecherche","Début");

            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            try
            {

                // Obtain the execution context from the service provider.
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);

                Entity entity = null;

                Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", "avant img");
                if (context.PostEntityImages.Contains("Target") && context.PostEntityImages["Target"] is Entity)
                {
                    Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", "post");
                    entity = (Entity)context.PostEntityImages["Target"];
                }
                else if (context.PreEntityImages.Contains("Target") && context.PreEntityImages["Target"] is Entity)
                {
                    Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", "pre");
                    entity = (Entity)context.PreEntityImages["Target"];
                }
                else
                    return;

                Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", context.MessageName.ToUpper());
                Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", entity.LogicalName);

                if (context.MessageName.ToUpper() == "CREATE" || context.MessageName.ToUpper() == "UPDATE" || context.MessageName.ToUpper() == "DELETE")
                {
                    Entity Societe = new Entity("account");

                    switch (entity.LogicalName)
                    {
                        case "bd_marquesgemmes":
                            Societe.Id = ((EntityReference)entity.Attributes["bd_annonceur"]).Id;
                            Societe.Attributes.Add("bd_datesynchromarquegemme", DateTime.Now);
                            break;
                        case "bd_mandat":
                            Societe.Id = ((EntityReference)entity.Attributes["bd_annonceurafacturer"]).Id;  // On synchronise uniquement l'annonceur à facturer.
                            Societe.Attributes.Add("bd_datesynchromandat", DateTime.Now);
                            break;
                        case "bd_budget":
                            Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", "1");
                            if (entity.Attributes.Contains("bd_marquegemme"))
                            {
                                Entity MarqueGemme = service.Retrieve("bd_marquesgemmes", ((EntityReference)entity.Attributes["bd_marquegemme"]).Id, new ColumnSet("bd_annonceur"));
                                Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", "1");
                                Societe.Id = ((EntityReference)MarqueGemme.Attributes["bd_annonceur"]).Id;
                                Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", "3");
                                Societe.Attributes.Add("bd_datesynchrobudget", DateTime.Now);
                                Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", "4");
                            }
                            break;
                        case "bd_accountperimetre":
                            Societe.Id = ((EntityReference)entity.Attributes["bd_accountid"]).Id;
                            Societe.Attributes.Add("bd_datesynchroprimetre", DateTime.Now);
                            break;
                        default:
                            break;
                    }

                    string res = string.IsNullOrEmpty(entity.GetAttributeValue<String>("bd_codecipres")) ? "" : " - " + entity.GetAttributeValue<String>("bd_codecipres");
                    res = entity.GetAttributeValue<Boolean>("bd_tiersdefacturation") ? "Client ADV" + res : "Fiche commerciale" + res;
                    entity.Attributes.Add("la_typeetcipres", res);

                    if (Societe.Id != Guid.Empty)
                        service.Update(Societe);
                }
                Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", "fin");

            }
            catch (Exception ex)
            {
                Log.Write("LG_Syncrho_MarqueGemme_Mandat_Budget", "fin");
                tracingService.Trace("Une erreur est survenue dans le Plugin LG_Syncrho_MarqueGemme_Mandat_Budget: {0}", ex.ToString());
                throw;
            }
        }
    }
}
