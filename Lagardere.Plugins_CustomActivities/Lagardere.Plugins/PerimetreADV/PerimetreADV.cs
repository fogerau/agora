﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Lagardere.CRM.Plugin.PerimetreADV
{
    public class PerimetreADV : IPlugin
    {
        /// <summary>
        /// Reporte les modifications sur account
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            tracingService.Trace("Start");

            try
            {
                tracingService.Trace("init");
                Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
                            serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = factory.CreateOrganizationService(context.UserId);
                
                try
                {
                 switch (context.MessageName.ToUpper()){
                     case "CREATE" :
                         tracingService.Trace("Create");
                         {
                            Entity entity = (Entity)context.PostEntityImages["Image"];
                            EntityReference v_perimetreADV = entity.GetAttributeValue<EntityReference>("la_perimetreadv");
                            EntityReference v_account = entity.GetAttributeValue<EntityReference>("la_account");

                            //Recupérer le perimletre ADV
                            Entity perimetreADV = service.Retrieve("la_perimetreadv", v_perimetreADV.Id, new ColumnSet(true));
                            string codesociete = perimetreADV.GetAttributeValue<String>("la_codesociete");
                            if (string.IsNullOrEmpty(codesociete))
                            {
                                codesociete = "-1";
                            }
                            QueryExpression query = new QueryExpression("account");
                            Entity account = new Entity("account");
                            account.Id = v_account.Id;
                            bool doNothing = false;
                              switch (codesociete)
                              {
                                  case ("360")://Metropole
                                      account.Attributes.Add("bd_perimetre_metropole", true);
                                      account.Attributes.Add("bd_modedereglementmetropoleid", entity.GetAttributeValue<EntityReference>("la_modedereglement"));
                                      break;
                                  case ("400"): // LGA
                                      account.Attributes.Add("bd_perimetre_lga", true);
                                      account.Attributes.Add("bd_modedereglementinternationalid", entity.GetAttributeValue<EntityReference>("la_modedereglement"));
                                      break;
                                  case ("-1"): //National
                                      account.Attributes.Add("bd_perimetre_national", true);
                                      account.Attributes.Add("bd_modederglementnationalid", entity.GetAttributeValue<EntityReference>("la_modedereglement"));
                                      break;
                                  default:
                                      doNothing = true;
                                      break;
                              }
                              if (!doNothing)
                              {
                                  service.Update(account);
                              }
                         }
                        break;
                     case "DELETE" :
                         {
                             tracingService.Trace("DELETE");
  
                             Entity entity = (Entity)context.PreEntityImages["Image"];
                                EntityReference v_perimetreADV = entity.GetAttributeValue<EntityReference>("la_perimetreadv");
                                EntityReference v_account = entity.GetAttributeValue<EntityReference>("la_account");
                             
                                //Recupérer le perimletre ADV
                                Entity perimetreADV = service.Retrieve("la_perimetreadv", v_perimetreADV.Id, new ColumnSet(true));
                              string codesociete = perimetreADV.GetAttributeValue<String>("la_codesociete");
                              QueryExpression query = new QueryExpression("account");
                              Entity account = new Entity("account");
                              account.Id = v_account.Id;
                              bool doNothing = false;
                              if (string.IsNullOrEmpty(codesociete))
                              {
                                  codesociete = "-1";
                              }

                           switch (codesociete)
                           {
                               case ("360")://Metropole
                                   account.Attributes.Add("bd_perimetre_metropole", false);
                                   account.Attributes.Add("bd_modedereglementmetropoleid", null);
                                   break;
                               case ("400"): // LGA
                                   account.Attributes.Add("bd_perimetre_lga", false);
                                   account.Attributes.Add("bd_modedereglementinternationalid", null);
                                   break;
                               case ("-1"): //National
                                   account.Attributes.Add("bd_perimetre_national", false);
                                   account.Attributes.Add("bd_modederglementnationalid", null);
                                   break;
                               default:
                                   doNothing = true;
                                   break;
                           }
                           if (!doNothing)
                           {
                               service.Update(account);
                           }
                         }
                         break;
                     case "UPDATE":
                         {
                             tracingService.Trace("UPDATE");

                             Entity preEntity = (Entity)context.PreEntityImages["Image"];
                             Entity postEntity = (Entity)context.PostEntityImages["Image"];
                             EntityReference pre_perimetreADVRef = preEntity.GetAttributeValue<EntityReference>("la_perimetreadv");
                             EntityReference post_perimetreADVRef = postEntity.GetAttributeValue<EntityReference>("la_perimetreadv");
                             EntityReference v_account = postEntity.GetAttributeValue<EntityReference>("la_account");
                             Entity account = new Entity("account");
                             account.Id = v_account.Id;
                             bool doNothing = false;

                             if (pre_perimetreADVRef.Id != post_perimetreADVRef.Id)
                             {
                                 //Changement de perimetre
                                 Entity pre_perimetreADV = service.Retrieve("la_perimetreadv", pre_perimetreADVRef.Id, new ColumnSet(true));
                                 string codesociete = pre_perimetreADV.GetAttributeValue<String>("la_codesociete");
                                 if (string.IsNullOrEmpty(codesociete))
                                 {
                                     codesociete = "-1";
                                 }

                                 switch (codesociete)
                                 {
                                     case ("360")://Metropole
                                         account.Attributes.Add("bd_perimetre_metropole", false);
                                         account.Attributes.Add("bd_modedereglementmetropoleid", null);
                                         break;
                                     case ("400"): // LGA
                                         account.Attributes.Add("bd_perimetre_lga", false);
                                         account.Attributes.Add("bd_modedereglementinternationalid", null);
                                         break;
                                     case ("-1"): //National
                                         account.Attributes.Add("bd_perimetre_national", false);
                                         account.Attributes.Add("bd_modederglementnationalid", null);
                                         break;

                                     default:
                                         doNothing = true;
                                         break;
                                 }
                             }
                            Entity post_perimetreADV = service.Retrieve("la_perimetreadv", post_perimetreADVRef.Id, new ColumnSet(true));
                            string codesociete2 = post_perimetreADV.GetAttributeValue<String>("la_codesociete");
                            if (string.IsNullOrEmpty(codesociete2))
                            {
                                codesociete2 = "-1";
                            }
                             
                             switch (codesociete2)
                             {
                                 case ("360")://Metropole
                                     account.Attributes.Add("bd_perimetre_metropole", true);
                                     account.Attributes.Add("bd_modedereglementmetropoleid", postEntity.GetAttributeValue<EntityReference>("la_modedereglement"));
                                     break;
                                 case ("400"): // LGA
                                     account.Attributes.Add("bd_perimetre_lga", true);
                                     account.Attributes.Add("bd_modedereglementinternationalid", postEntity.GetAttributeValue<EntityReference>("la_modedereglement"));
                                     break;
                                 case ("-1"): //National
                                     account.Attributes.Add("bd_perimetre_national", true);
                                     account.Attributes.Add("bd_modederglementnationalid", postEntity.GetAttributeValue<EntityReference>("la_modedereglement"));
                                     break;
                                 default:
                                     doNothing = true;
                                     break;
                             }
                             if (!doNothing)
                             {
                                 service.Update(account);
                             }

                         }

                         break;

                 }
                 
                }
                catch (Exception ex)
                {
                    throw new InvalidPluginExecutionException("Une erreur s'est produite lors du report des données perimetreDeSociété vers account. ", ex);
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin perimetreDeSociété.", ex);
            }
            catch (Exception ex)
            {
                tracingService.Trace("perimetreADVDeSociété : {0}", ex.ToString());
                throw new InvalidPluginExecutionException("Une erreur s'est produite dans le plugin perimetreDeSociété2.", ex); ;
            }
        }
    }
}
