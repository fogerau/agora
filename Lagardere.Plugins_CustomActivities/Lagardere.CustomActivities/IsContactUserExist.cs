﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace Lagardere.CustomActivities
{
    /// <summary>
    /// Permet de savoir si un user existe en tant que contact Lagardère
    /// </summary>
    public class IsContactUserExist : CodeActivity
    {
        [Output("OutIsContactUserExist")]
        public OutArgument<Boolean> v_OutIsContactUserExist { get; set; }

        protected override void Execute(CodeActivityContext p_context)
        {
            v_OutIsContactUserExist.Set(p_context, false);
        }

    }
}
