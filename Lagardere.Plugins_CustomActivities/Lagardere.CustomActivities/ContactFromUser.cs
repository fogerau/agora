﻿using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;

namespace Lagardere.CustomActivities
{
    public class ContactFromUser : CodeActivity
    {
        [Output("OutContact")]
        [ReferenceTarget("contact")]
        public OutArgument<EntityReference> v_OutContact { get; set; }

        [Input("InUser")]
        [ReferenceTarget("systemuser")]
        public InArgument<EntityReference> v_InUser { get; set; }

        protected override void Execute(CodeActivityContext p_context)
        {
            v_OutContact.Set(p_context, null);

            try
            {
                //Create the context
			    IWorkflowContext context = p_context.GetExtension<IWorkflowContext>();
			    IOrganizationServiceFactory serviceFactory = p_context.GetExtension<IOrganizationServiceFactory>();
			    IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
                EntityReference ref_User = v_InUser.Get<EntityReference>(p_context);

                Entity v_Contact = getContactFromUser(service, ref_User.Id);
                v_OutContact.Set(p_context, new EntityReference(v_Contact.LogicalName,v_Contact.Id));

            }
            catch (Exception e)
            {

                throw new InvalidPluginExecutionException("Une erreur est survenue dans le plugin Lagardere.CustomActivity.ContactFromUser", e);
            }

        }
        
        private Entity getContactFromUser(IOrganizationService p_service, Guid v_InUserId)
        {
            try
            {
                EntityReferenceCollection oRefMarqueCR = new EntityReferenceCollection();

                QueryExpression query = new QueryExpression
                {
                    EntityName = "contact",
                    ColumnSet = new ColumnSet { AllColumns = true },
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "bd_utilisateurlagardere",
                                Operator = ConditionOperator.Equal,
                                Values = {v_InUserId}
                            }
                        }
                    }
                };

                EntityCollection oListContact = p_service.RetrieveMultiple(query);

                if (oListContact.Entities.Count > 0) 
                {
                    return oListContact.Entities[0];
                }

                return null;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getContactFromUser : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getContactFromUser : " + e.Message);
            }
        }
    }
}
