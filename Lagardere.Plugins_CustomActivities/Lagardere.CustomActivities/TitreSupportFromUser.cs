﻿using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;

namespace Lagardere.CustomActivities
{
    public class TitreSupportFromUser : CodeActivity
    {
        [Output("OutTitreSupport")]
        [ReferenceTarget("bd_titresupport")]
        public OutArgument<EntityReference> v_OutTitreSupport { get; set; }

        [Input("InUser")]
        [ReferenceTarget("systemuser")]
        public InArgument<EntityReference> v_InUser { get; set; }

        protected override void Execute(CodeActivityContext p_context)
        {
            v_OutTitreSupport.Set(p_context, null);

            try
            {
                //Create the tracing service
                ITracingService tracingService = p_context.GetExtension<ITracingService>();

                //Create the context
                IWorkflowContext context = p_context.GetExtension<IWorkflowContext>();
                tracingService.Trace("Context.UserId : " + context.UserId.ToString());

                IOrganizationServiceFactory serviceFactory = p_context.GetExtension<IOrganizationServiceFactory>();
                IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
                EntityReference ref_User = v_InUser.Get<EntityReference>(p_context);
                tracingService.Trace("ref_User : " + v_InUser.Get<EntityReference>(p_context).Id);
                tracingService.Trace("ref_User name : " + v_InUser.Get<EntityReference>(p_context).Name);

                Entity v_TitreSupport = getTitreSupportFromUser(service, ref_User.Id);
                v_OutTitreSupport.Set(p_context, new EntityReference(v_TitreSupport.LogicalName, v_TitreSupport.Id));

            }
            catch (Exception e)
            {

                throw new InvalidPluginExecutionException("Une erreur est survenue dans le plugin Lagardere.CustomActivity.ContactFromUser", e);
            }

        }

        private Entity getTitreSupportFromUser(IOrganizationService p_service, Guid v_InUserId)
        {
            try
            {
                EntityReferenceCollection oRefMarqueCR = new EntityReferenceCollection();

                QueryExpression query = new QueryExpression("bd_titresupport");

                query.ColumnSet = new ColumnSet(true);

                LinkEntity linkEntity1 = new LinkEntity("bd_titresupport", "bd_bd_titresupport_systemuser", "bd_titresupportid", "bd_titresupportid", JoinOperator.Inner);

                LinkEntity linkEntity2 = new LinkEntity("bd_bd_titresupport_systemuser", "systemuser", "systemuserid", "systemuserid", JoinOperator.Inner);

                linkEntity1.LinkEntities.Add(linkEntity2);
                query.LinkEntities.Add(linkEntity1);

                //Add condition to match the Bike name with “Honda”
                linkEntity2.LinkCriteria = new FilterExpression();

                linkEntity2.LinkCriteria.AddCondition(new ConditionExpression("systemuserid", ConditionOperator.Equal, v_InUserId));
                                
                EntityCollection collRecords = p_service.RetrieveMultiple(query);


                if (collRecords.Entities.Count > 0)
                {
                    return collRecords.Entities[0];
                }

                return null;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getTitreSupportFromUser : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getTitreSupportFromUser : " + e.Message);
            }
        }
    }
}
