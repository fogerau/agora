﻿using Microsoft.Xrm.Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk;
using System.Net.Mail;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using System.ServiceModel;
using System.Diagnostics;

namespace MailGrandCompte
{
    class MailGrandCompte
    {
        const string EventSource = "MailGrandCompte";
        const string EventLogName = "Application";
        static void Main(string[] args)
        {

            try
            {
                if (!EventLog.SourceExists(EventSource))
                    EventLog.CreateEventSource(EventSource, EventLogName);
                EventLog.WriteEntry(EventSource, "Démarage de l'application MailGrandCompte");
                using (var service = new OrganizationService("Xrm"))
                {

                    QueryExpression queryCR = new QueryExpression("bd_compterendu");
                    queryCR.ColumnSet = new ColumnSet(true);
                    queryCR.Criteria.AddCondition(new ConditionExpression("modifiedon", ConditionOperator.Today));
                    EntityCollection compteRendus = service.RetrieveMultiple(queryCR);
                    foreach (Entity EntityCR in compteRendus.Entities)
                    {
                        EventLog.WriteEntry(EventSource, "Traitement de " + EntityCR.GetAttributeValue<string>("bd_name"));
                        String sConcat_marques = String.Empty;
                        String Guid = String.Empty;
                        String[] ListeGuid = new String[] { };

                        EntityReferenceCollection oCollRefMarqueDisassociate = new EntityReferenceCollection();
                        EntityReferenceCollection oCollRefMarqueAssociate = new EntityReferenceCollection();
                        EntityReferenceCollection v_refSecteurdactiviteDisassociate = new EntityReferenceCollection();
                        EntityReferenceCollection v_refSecteurdactiviteAssociate = new EntityReferenceCollection();
                        EntityReference oRefSecteur = new EntityReference();

                        // AGORA-1264 Envoi de mail pour grand compte
                        // Envoi de mail si 
                        //       - Le compte rendu est posterieur au RDV 
                        //       - Le compte rendu n'est pas annulé
                        //       - La société est Grand Compte
                        //       - Le rédacteur n'est pas le responsable grand compte
                        //       - Le cr est postérieur ou egal au rdv
                        // on recupere la société

                        EntityReference societeRef = (EntityReference)EntityCR.Attributes["bd_compte"];
                        ColumnSet cs = new ColumnSet("la_grandcompteregie", "la_grandcompteregie2", "name");
                        Entity societe = service.Retrieve("account", societeRef.Id, cs);
                        EntityReference gestionnaireRef = null;
                        if (societe.Contains("la_grandcompteregie"))
                        {
                            gestionnaireRef = (EntityReference)societe.Attributes["la_grandcompteregie"];
                            if (gestionnaireRef != null)
                            {
                                SendNotification(service, EntityCR, gestionnaireRef, societe);
                            }
                        }

                        if (societe.Contains("la_grandcompteregie2"))
                        {
                            gestionnaireRef = (EntityReference)societe.Attributes["la_grandcompteregie2"];
                            if (gestionnaireRef != null)
                            {
                                SendNotification(service, EntityCR, gestionnaireRef, societe);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(EventSource, ex.Message + "\r\n" + ex.StackTrace, EventLogEntryType.Error);
            }
            EventLog.WriteEntry(EventSource, "Fin de l'application MailGrandCompte");
        }

        private static void SendNotification(OrganizationService service, Entity EntityCR, EntityReference gestionnaireRef, Entity societe)
        {

            //Récupération du rendez-vous parent
            EntityReference rdvRef = (EntityReference)EntityCR.Attributes["bd_rendezvous"];
            Entity rdv = service.Retrieve("appointment", rdvRef.Id, new ColumnSet(true));
            DateTime dtRDV = (DateTime)rdv.Attributes["scheduledstart"];
            DateTime dtCR = DateTime.Now;
            if (EntityCR.Contains("modifiedon"))
            {
                dtCR = (DateTime)EntityCR.Attributes["modifiedon"];
            }
            EntityReference redacteurRef = (EntityReference)EntityCR.Attributes["ownerid"];
            if (DateTime.Compare(dtRDV, dtCR) <= 0 && redacteurRef.Id != gestionnaireRef.Id)
            {
                // Récupération des données du rédacteur
                Entity redacteur = service.Retrieve("systemuser", redacteurRef.Id, new ColumnSet(true));
                // Récupération des données du gestionnaire
                Entity gestionnaire = service.Retrieve("systemuser", gestionnaireRef.Id, new ColumnSet(true));
                EntityReference gestDivision = gestionnaire.GetAttributeValue<EntityReference>("businessunitid");
                EntityReference redDivision = redacteur.GetAttributeValue<EntityReference>("businessunitid");
                //  <EntityReference>
                if (gestDivision.Id == redDivision.Id)
                {
                    // Récupération des contacts du CR
                    EntityCollection contacts = getContactsFromCR2(service, EntityCR.Id);
                    // envoi du mail
                    SmtpClient smtpClient = new SmtpClient();
                    MailMessage message = new MailMessage();
                    MailAddress fromAddress = new MailAddress("noreply-agora@lagardere-active.com");
                    // setup up the host, increase the timeout to 5 minutes
                    smtpClient.Host = Properties.Settings.Default.smptHost;
                    smtpClient.UseDefaultCredentials = false;
                    //smtpClient.Credentials = basicCredential;
                    smtpClient.Timeout = (60 * 5 * 1000);
                    message.From = fromAddress;
                    message.Subject = "Grand Compte Agora - Création/Modification de compte rendu";
                    message.IsBodyHtml = true;
                    // Récupérer le titre /support
                    QueryExpression query = new QueryExpression("bd_titresupporthistorique");
                    query.ColumnSet = new ColumnSet(true);
                    query.Criteria.AddCondition(new ConditionExpression("bd_utilisateur", ConditionOperator.Equal, redacteurRef.Id));
                    query.Criteria.AddCondition(new ConditionExpression("bd_datedefin", ConditionOperator.Null));
                    query.AddOrder("bd_datededbut", OrderType.Descending);
                    EntityCollection titresupportC = service.RetrieveMultiple(query);
                    string ts = "";
                    if (titresupportC.Entities.Count > 0)
                    {
                        Entity t = service.Retrieve("bd_titresupport", titresupportC.Entities[0].GetAttributeValue<EntityReference>("bd_titresupport").Id, new ColumnSet(true));
                        ts = " (" + t.GetAttributeValue<string>("bd_name") + ")";
                    }

                    message.Body = @"<html>
<body>
<span>Bonjour " + gestionnaire.Attributes["fullname"] + @"</span><br>
<span>" + redacteur.Attributes["fullname"] + ts + @" a rédigé un compte-rendu suite au rendez-vous du " + dtRDV.ToShortDateString() + @" avec la société <b>" + societe.Attributes["name"] + @"</b>.<br><br></span>";
                    message.Body += @"<span><b>Compte rendu</b> : </span>";
                    message.Body += @"<span>" + EntityCR.Attributes["bd_compterendu"] + "<br></span>";

                    if (contacts.Entities.Count > 0)
                    {
                        if (contacts.Entities.Count == 1)
                        {
                            message.Body += @"<span>Le contact est : </span>";
                        }
                        else
                        {
                            message.Body += @"<span>Les contacts sont : </span>";
                        }
                        foreach (Entity contact in contacts.Entities)
                        {
                            string civilite = GetOptionsSetTextForValue(service, "contact", "bd_civilite", ((OptionSetValue)contact.Attributes["bd_civilite"]).Value);
                            string fonction = "";
                            if (contact.Contains("bd_fonctionlp"))
                            {
                                fonction = " (" + GetOptionsSetTextForValue(service, "contact", "bd_fonctionlp", ((OptionSetValue)contact.Attributes["bd_fonctionlp"]).Value) + ")";
                            }

                            message.Body += "<li>" + civilite + " " + contact.Attributes["fullname"] + fonction.ToString() + "</li>";
                        }
                    }
                    message.Body += @"</body></html>";
                    if (gestionnaire.Contains("internalemailaddress"))
                    {
                        //SmtpFailedRecipientException =internalemailaddress
                        message.To.Add(new MailAddress((string)gestionnaire.Attributes["internalemailaddress"], (string)gestionnaire.Attributes["fullname"]));
                        //  message.To.Add(new MailAddress("OMNILOG-F.OGERAU@Lagardere-Active.com", "Frédéric Ogerau"));
                        smtpClient.Send(message);
                        EventLog.WriteEntry(EventSource, "Mail envoyé à " + gestionnaire.GetAttributeValue<string>("fullname"));
                    }
                }

            }
        }
    

        private static string GetOptionsSetTextForValue(IOrganizationService service, string entityName, string attributeName, int selectedValue)
        {

            RetrieveAttributeRequest retrieveAttributeRequest = new
            RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = attributeName,
                RetrieveAsIfPublished = true
            };
            // Execute the request.
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            // Access the retrieved attribute.
            Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata)
            retrieveAttributeResponse.AttributeMetadata;// Get the current options list for the retrieved attribute.
            OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string selectedOptionLabel = null;
            foreach (OptionMetadata oMD in optionList)
            {
                if (oMD.Value == selectedValue)
                {
                    selectedOptionLabel = oMD.Label.LocalizedLabels[0].Label.ToString();
                    break;
                }
            }
            return selectedOptionLabel;
        }
        /// <summary>
        /// Permet de retrouver la liste des contacts d'un compte-rendu
        /// </summary>
        /// <param name="p_service"></param>
        /// <param name="v_InUserId"></param>
        /// <returns></returns>
        private static EntityCollection getContactsFromCR2(IOrganizationService p_service, Guid v_CompteRenduId)
        {
            try
            {
                QueryExpression query = new QueryExpression("contact");
                query.ColumnSet = new ColumnSet(true);
                LinkEntity linkEntity1 = new LinkEntity("contact", "bd_contact_bd_compterendu", "contactid", "contactid", JoinOperator.Inner);
                LinkEntity linkEntity2 = new LinkEntity("bd_contact_bd_compterendu", "bd_compterendu", "bd_compterenduid", "bd_compterenduid", JoinOperator.Inner);
                linkEntity1.LinkEntities.Add(linkEntity2);
                query.LinkEntities.Add(linkEntity1);
                linkEntity2.LinkCriteria = new FilterExpression();
                linkEntity2.LinkCriteria.AddCondition(new ConditionExpression("bd_compterenduid", ConditionOperator.Equal, v_CompteRenduId));
                EntityCollection collRecords = p_service.RetrieveMultiple(query);

                return collRecords;
            }
            catch (FaultException e)
            {
                throw new InvalidPluginExecutionException("getContactsFormCR : Une erreur s'est produite dans le plugin LG_CompteRendu_Mail getContactFormCR : " + e.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("getContactsFormCR : Une erreur s'est produite dans le plugin LG_CompteRendu_Mail getContactFormCR : " + e.Message);
            }
        }
    }
}
